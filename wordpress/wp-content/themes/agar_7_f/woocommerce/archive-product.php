<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); 
global $wp_query; 
$term = get_term_by( 'slug', get_query_var($wp_query->query_vars['taxonomy']), $wp_query->query_vars['taxonomy']);
$subcats = get_terms( 'product_cat', array(
	'parent'    => $term->term_id,
	'hide_empty' => false
));
$category = $term->term_id;
if($subcats){
	$has_subCat = 'true';
}else{
	$has_subCat = 'false';
}
?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; 
		//echo @do_shortcode('[product_categories_dropdown orderby="title" count="0" hierarchical="0"]');
			/*
			$args = array(
				'type'                     => 'product',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 1,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'product_cat',
				'pad_counts'               => false,
				'hide_if_empty'      	=> false

			); 
			echo '<div class="select_category">';
			$categories = get_categories( $args );
			$current_category = get_query_var( 'term' );
			echo '<select id="product_category">';
			echo '<option value="0">All Categories</option>';
			foreach($categories as $category){
				if($current_category == $category->slug){
					$selected = 'selected=selected';
				}else{
					$selected = '';
				}
				echo '<option value="'.$category->term_id.'" data-slug="'.$category->slug.'" '.$selected.'>'.$category->name.'</option>';
			}
			echo '</select>';
			echo '</div>';
			* */
		?>

		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				//do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php //woocommerce_product_subcategories(); ?>
				<?php global $wp_query;
				$cat = $wp_query->get_queried_object(); 
				$current_category = $cat->term_id;
				$children = get_terms( 'product_cat', array(
					'parent'    => $cat->term_id,
					'hide_empty' => false
				));
				$args1 = array(
					'type'                     => 'product',
					'child_of'                 => '',
					'parent'                   => $cat->term_id,
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'taxonomy'                 => 'product_cat',
					'pad_counts'               => false,
					'hide_if_empty'      	   => false

				); 
				$args2 = array(
					'post_type'                => 'product',
					'post_status'              => 'publish',
					'orderby'                  => 'title',
					'order'                    => 'ASC',
					'posts_per_page'           => -1,
					'tax_query' 			   => array(
						 array(
							'taxonomy' => 'product_cat',
							'field' => 'id',
							'terms' => array( $current_category ),
							'include_children' => false,       
						), 
					), 
				); 
				$categories = get_categories( $args1 );
				$datas = get_posts( $args2 );
				$products = array();
				foreach($categories as $category){
					$products[] = array(
						'id'	=>	$category->term_id,
						'name'	=>	$category->name,
						'type'	=>	'category'
					);
				}
				$a = false;
				foreach($datas as $data){
					$a = false;
					$terms = wp_get_object_terms( $data->ID, 'product_cat');
					if(count($terms) > 1){
						foreach($terms as $term){
							if($term->term_id != $current_category){
								$childrens = get_terms( 'product_cat', array(
									'parent'    => $current_category,
									'hide_empty' => false
								));
								if($childrens){
									foreach($childrens as $children){
										if($children->term_id == $term->term_id) 
											$a = true;
									}
								}
							}
						}
					}
					if($a) 
						continue;
					$products[] = array(
						'id'	=>	$data->ID,
						'name'	=>	$data->post_title,
						'type'	=>	'product'
					);
				}
				function compareByName($a, $b) {
					return strcmp($a['name'], $b['name']);
				}
				usort($products, 'compareByName');
				foreach($products as $product){
					if($product['type'] == 'category'){
						$category = get_term($product['id'],'product_cat');
						$url = get_term_link( $category, 'product_cat' );
						$thumbnail_id = get_woocommerce_term_meta( $product['id'], 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );
						$name = $product['name'].' <mark class="count">('.$category->count.')</mark>';
					}else{
						$url = get_permalink($product['id']);
						$thumbSrc = wp_get_attachment_image_src( get_post_thumbnail_id($product['id']), 'shop_catalog' );
						if(!empty($thumbSrc))
							$image = $thumbSrc[0];
						$name = $product['name'];
					}
				?>
				<li class="product">
					<a href="<?php echo $url; ?>">
						<?php 
						if($image){ ?>
							<img class="woocommerce-placeholder wp-post-image" width="150" height="150" alt="Placeholder" src="<?php echo $image; ?>">
						<?php }else{ ?>
							<img class="woocommerce-placeholder wp-post-image" width="150" height="150" alt="Placeholder" src="<?php bloginfo('url'); ?>/wp-content/plugins/woocommerce/assets/images/placeholder.png">
						<?php } ?> 
						<h3><?php echo $name; ?></h3>
					</a>
				</li>
				<?php } ?>
				<?php while ( have_posts() ) : the_post(); 
					$terms = wp_get_post_terms(get_the_ID(), $wp_query->query_vars['taxonomy']);
					if(isset($_GET['s']) && $_GET['s'] != '' ){
						wc_get_template_part( 'content', 'product' );
					}else{
						if($has_subCat == 'true'){
							
							if (count($terms) == 1 && $terms[0]->term_id == $term->term_id):
							
								//wc_get_template_part( 'content', 'product' );

							endif;  // end of the loop. 
						
						}else{
						
							//wc_get_template_part( 'content', 'product' );
						}
					}
					endwhile;
				?>
				

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				//do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
