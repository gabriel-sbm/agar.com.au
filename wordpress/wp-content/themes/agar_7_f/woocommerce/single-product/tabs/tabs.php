<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs">
		<?php 
		$data_sds = get_post_meta(get_the_ID(), 'sds_product_pdfs', true);
		$data_pds = get_post_meta(get_the_ID(), 'pds_product_pdfs', true);
		$sds_pdfs = unserialize($data_sds);
		$pds_pdfs = unserialize($data_pds);
		if($sds_pdfs || $pds_pdfs ){ ?>
			<div class="product_pdf_dn"> 
			<h2>Download</h2>

			<table width="100%" cellspacing="0" cellpadding="10" border="0" style="width:30%">
				<tbody>
					<?php  if($sds_pdfs){
						if(!is_array($sds_pdfs)){
							$sds_pdfs = array($sds_pdfs);
						}
						foreach($sds_pdfs as $pdf){ 
							$name = explode('/', $pdf);
							$name = $name[count($name)-1];
							$name = explode('.', $name);
							$name = $name[0];
							$name = str_replace('_',' ',$name);
					  ?>
					<tr>
						<td align="left">
							<p style="text-align: left;"><?php echo $name ?></p>
						</td>
						<td align="center" style="text-align: center;">
							<a class="button" href="<?php echo $pdf; ?>" target="_blank">Download SDS File</a>
						</td>
					</tr>
					<?php } } ?>
					<?php  if($pds_pdfs){
						if(!is_array($pds_pdfs)){
							$pds_pdfs = array($pds_pdfs);
						}
						foreach($pds_pdfs as $pdf){ 
							$name = explode('/', $pdf);
							$name = $name[count($name)-1];
							$name = explode('.', $name);
							$name = $name[0];
							$name = str_replace('_',' ',$name);
							if ( !is_user_logged_in() ) {
								$pdf = get_bloginfo('url').'/my-account/';
								//$class = "user_not_logged_dwn";
								$target = '';
							}else{
								//$class = "";
								$target = 'target="_blank"';
							}
					  ?>
					<tr>
						<td align="left">
							<p style="text-align: left;"><?php echo $name ?></p>
						</td>
						<td align="center" style="text-align: center;">
							<a class="button" href="<?php echo $pdf; ?>" <?php echo $target; ?>>Download SDS File</a>
						</td>
					</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
		<ul class="tabs">
			<?php foreach ( $tabs as $key => $tab ) : ?>

				<li class="<?php echo $key ?>_tab">
					<a href="#tab-<?php echo $key ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?></a>
				</li>

			<?php endforeach; ?>
		</ul>
		<?php /* foreach ( $tabs as $key => $tab ) : ?>
			<?php if($key != 'description'){ ?>
			<div class="panel entry-content" id="tab-<?php echo $key ?>">
				<?php call_user_func( $tab['callback'], $key, $tab ) ?>
			</div>
			<?php } ?>
		<?php endforeach; ?>
		<?php */foreach ( $tabs as $key => $tab ) : ?>
			<?php if($key == 'reviews'){ ?>
			<div class="panel entry-content" id="tab-<?php echo $key ?>">
				<?php call_user_func( $tab['callback'], $key, $tab ) ?>
			</div>
			<?php } ?>
		<?php endforeach;  ?>
	</div>

<?php endif; ?>
