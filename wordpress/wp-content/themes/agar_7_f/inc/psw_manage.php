<?php 
add_action( 'admin_menu', 'register_psw_menu_page' );

function register_psw_menu_page() {

	add_menu_page( 'Manage PSW', 'Manage PSW', 'manage_options', 'manage_psw', 'callback_manage_psw', '', 50 );

}

function callback_manage_psw(){ ?>
	<script>
		jQuery(document).ready(function($){
			jQuery('#product_linkS').validate();
			jQuery('#product_linkA').validate();
			jQuery("#selectallSoilage").click(function () { 
				jQuery('.selectsingleSoilage').prop('checked', this.checked);
			});
			jQuery(".selectsingleSoilage").click(function(){
				if(jQuery(".selectsingleSoilage").length == jQuery(".selectsingleSoilage:checked").length) {
					jQuery("#selectallSoilage").prop("checked", "checked");
				} else {
					jQuery("#selectallSoilage").removeAttr("checked");
				}
			});
			jQuery("#selectallArea").click(function () { 
				jQuery('.selectsingleArea').prop('checked', this.checked);
			});
			jQuery(".selectsingleArea").click(function(){
				if(jQuery(".selectsingleArea").length == jQuery(".selectsingleArea:checked").length) {
					jQuery("#selectallArea").prop("checked", "checked");
				} else {
					jQuery("#selectallArea").removeAttr("checked");
				}
			});
			jQuery("select.flexselect").flexselect({
				allowMismatch: true,
				inputNameTransform:  function(name) { return "flexselect_" + name; }
			});
			//Soilage Ajax
			$(".product_linkS select[name='soilage']").change(function() { 
				$('.loaderSmall').show();
				var soilage_id = $(this).val();
				if(soilage_id != 0){
					var data = {
						'action': 'select_soilage',
						'soilage_id': soilage_id
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkS select[name='products[]'] option").removeAttr('selected');
							$(".product_linkS select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Surface Ajax
			$(".product_linkS select[name='surface']").change(function() { 
				$('.loaderSmall').show();
				var surface_id = $(this).val();
				var soilage_id = $(".product_linkS select[name='soilage']").val();
				if(surface_id != 0){
					var data = {
						'action': 'select_surface',
						'soilage_id': (soilage_id != 0 ? soilage_id : ''),
						'surface_id': surface_id
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkS select[name='products[]'] option").removeAttr('selected');
							$(".product_linkS select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Soil Sub-Category Ajax
			$(".product_linkS select[name='sub_category']").change(function() {
				$('.loaderSmall').show(); 
				var category_id = $(this).val();
				var category_name = $(this).find('option:selected').text();
				var soilage_id = $(".product_linkS select[name='soilage']").val();
				var surface_id = $(".product_linkS select[name='surface']").val();
				if(surface_id != 0){
					var data = {
						'action': 'select_soil_category',
						'soilage_id': (soilage_id != 0 ? soilage_id : ''),
						'surface_id': (surface_id != 0 ? surface_id : ''),
						'category_id': category_id,
						'category_name':  category_name
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkS select[name='products[]'] option").removeAttr('selected');
							$(".product_linkS select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Area Ajax
			$(".product_linkA select[name='area']").change(function() { 
				$('.loaderSmall').show();
				var area_id = $(this).val();
				if(area_id != 0){
					var data = {
						'action': 'select_area_admin',
						'area_id': area_id
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkA select[name='products[]'] option").removeAttr('selected');
							$(".product_linkA select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Sub-Category 1 Ajax
			$(".product_linkA select[name='sub_category1']").change(function() { 
				$('.loaderSmall').show();
				var subcat1_id = $(this).val();
				var area_id = $(".product_linkA select[name='area']").val();
				if(subcat1_id != 0){
					var data = {
						'action': 'select_subCat1',
						'area_id': (area_id != 0 ? area_id : ''),
						'subcat1_id': subcat1_id
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkA select[name='products[]'] option").removeAttr('selected');
							$(".product_linkA select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Sub-Category 2 Ajax
			$(".product_linkA select[name='sub_category2']").change(function() { 
				$('.loaderSmall').show();
				var subcat3_id = $(this).val();
				var subcat3_name = $(this).find('option:selected').text();
				var area_id = $(".product_linkA select[name='area']").val();
				var subcat2_id = $(".product_linkA select[name='sub_category1']").val();
				if(subcat3_id != 0){
					var data = {
						'action': 'select_subCat2',
						'area_id': (area_id != 0 ? area_id : ''),
						'subcat2_id': (subcat2_id != 0 ? subcat2_id : ''),
						'subcat3_id': subcat3_id,
						'subcat3_name':  subcat3_name
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkA select[name='products[]'] option").removeAttr('selected');
							$(".product_linkA select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Sub-Category 3 Ajax
			$(".product_linkA select[name='sub_category3']").change(function() {
				$('.loaderSmall').show(); 
				var subcat4_id = $(this).val();
				var subcat3_id = $(".product_linkA select[name='sub_category2']").val();
				if(subcat4_id != 0){
					var data = {
						'action': 'select_subCat3',
						'subcat3_id': (subcat3_id != 0 ? subcat3_id : ''),
						'subcat4_id': subcat4_id
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkA select[name='products[]'] option").removeAttr('selected');
							$(".product_linkA select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			
			//Sub-Category 4 Ajax
			$(".product_linkA select[name='sub_category4']").change(function() { 
				$('.loaderSmall').show();
				var subcat5_id = $(this).val();
				var subcat5_name = $(this).find('option:selected').text();
				var subcat3_id = $(".product_linkA select[name='sub_category2']").val();
				var subcat4_id = $(".product_linkA select[name='sub_category3']").val();
				if(subcat5_id != 0){
					var data = {
						'action': 'select_subCat4',
						'subcat3_id': (subcat3_id != 0 ? subcat3_id : ''),
						'subcat4_id': (subcat4_id != 0 ? subcat4_id : ''),
						'subcat3_id': subcat5_id,
						'subcat5_name':  subcat5_name
					};
					$.post(ajaxurl, data, function(response) {
						if(response != 'none'){
							$(".product_linkA select[name='products[]'] option").removeAttr('selected');
							$(".product_linkA select[name='products[]'] option").each(function() {
								var obj = $(this);
								var prod = obj.val();
								var result = $.grep(response, function(e){ return e.toLowerCase() == prod.toLowerCase(); });
								if (result.length == 1) {
									obj.attr('selected','selected');
								}
							});
						}
						$('.loaderSmall').hide();
					},'json');
				}
			});
			edit_categories('soilage_to_edit');
			edit_categories('surface_to_edit');
			edit_categories('category_to_edit');
			edit_categories('area_to_edit');
			edit_categories('category1_to_edit');
			edit_categories('category2_to_edit');
			edit_categories('category3_to_edit');
			edit_categories('category4_to_edit');
			
			
		});
		function edit_categories(select_box){
			jQuery("form select[name='"+select_box+"']").change(function() { 
				var text = jQuery(this).find('option:selected').text();
				jQuery(this).parents('form').find('input[type="text"]').val(text);
			});
		}
		function redirect_to(url,rpp){
			window.location.href=url+"&rpp="+rpp;
		}
	</script>
	
	<?php global $wpdb;
	$error = array();
	$tabs = array( 
		'soilage' => 'Soil to Remove',
		'area' => 'Area to Clean', 
		'addProductsSoilage' => 'Add Products to Soilage', 
		'addProductsArea' => 'Add Products to Area' 
	);
	if($_GET['action'] == 'edit' && $_GET['type'] == 'soilage'){
		$tabs['addProductsSoilage'] = 'Edit Products to Soilage';
	}
	if($_GET['action'] == 'edit' && $_GET['type'] == 'area'){
		$tabs['addProductsArea'] = 'Edit Products to Area';
	}
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    $current = ($_GET['tab'] != '') ? $_GET['tab'] : '';
    if($_GET['tab'] == 'addProductsSoilage'){
		$subtab = 'addCategoriesS';
	}else if($_GET['tab'] == 'addProductsArea'){
		$subtab = 'addCategoriesA';
	}else{
		$subtab = '';
	}
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        if($_GET['tab'] == '' && $tab == 'soilage')
			$class = ' nav-tab-active';
        if($tab == 'addProductsSoilage'){
			echo "<a class='nav-tab$class' href='?page=manage_psw&tab=$tab&subtab=addCategoriesS'>$name</a>";
		}else if($tab == 'addProductsArea'){
			echo "<a class='nav-tab$class' href='?page=manage_psw&tab=$tab&subtab=addCategoriesA'>$name</a>";
		}else{
			echo "<a class='nav-tab$class' href='?page=manage_psw&tab=$tab'>$name</a>";
		}
    }
    echo '</h2>';
    if ( $_GET['tab'] =='' || $_GET['tab'] == 'soilage' && ($_GET['action'] == '' || !isset($_GET['action']) || $_GET['action'] == 'delete' ) ){ 
		if(isset($_REQUEST["rpp"])){
			$perpage = ($_REQUEST["rpp"]); 
		}else{ 
			$perpage = 100;	 
		}
		if(isset($_REQUEST["p"])){
			$page = ($_REQUEST["p"]); 
		}else{ 
			$page = 1;	 
		}
		if(isset($_REQUEST["action"]) && $_REQUEST["action"] == 'delete'){
			$wpdb->delete( 
				'soil_search_4',
				array( 
					'1_id' => $_GET['soilageid'],
					'2_id' => $_GET['surfaceid'],
					'3_id' => $_GET['categoryid'],
					'name' => $_GET['name'],
				), 
				array( 
					'%d',
					'%d',
					'%s'
				)
			);
			
		}
		if($_REQUEST['deleteSoilage']){
			if(!empty($_POST['selectsingleSoilage'])) { 
				$checked_value = $_POST['selectsingleSoilage'];
				foreach($checked_value as $val) {
					$wpdb->delete(
						'soil_search_4',
						array( 'id' => $val ),
						array( '%d' )
					);
				}
			}
		}
		if($_REQUEST['search_category']){
			$url = admin_url().'admin.php?page=manage_psw&tab='.($_GET['tab'] != '' ? $_GET['tab'] : 'soilage').'&search='.$_REQUEST['search_keyword']; 
			echo "<script>window.location.href='".$url."'</script>";
		}
		$url = admin_url().'admin.php?page=manage_psw&tab='.($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); 
		$current_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
		if($_GET['order'] != '' && $_GET['orderby'] != ''){
			$parse_url = parse_url($current_url);
			$query = explode('&',$parse_url['query']);
			unset($query[count($query)-1]);
			unset($query[count($query)-1]);
			if(count($query) >1){
				$query = implode('&',$query);
			}else{
				$query = $query[0];
			}
			$current_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
		}
		$start=($page-1)*$perpage;
		if($_GET['order'] != '' && $_GET['orderby'] != ''){
			$order = 'order by '.$_GET['orderby'].' '.$_GET['order'];
		}else{
			$order = 'order by productid desc';
		}
		if($_GET['search'] != ''){
			$allsoilages = $wpdb->get_results("SELECT * from (SELECT ss1.id as soilageid,ss1.name as soilage,ss2.id as surfaceid,ss2.name as surface,ss3.name as category,ss3.id as categoryid,ss4.id as productid,ss4.name as product, ss4.date_created as date_created, ss4.date_modified as date_modified FROM `soil_search_4` ss4 left join `soil_search_3` ss3 on (ss4.3_id = ss3.id) left join `soil_search_2` ss2 on (ss4.2_id = ss2.id) left join `soil_search_1` ss1 on (ss4.1_id = ss1.id) where (ss4.name REGEXP '".$_GET['search']."' || ss1.name REGEXP '".$_GET['search']."' || ss2.name REGEXP '".$_GET['search']."' || ss3.name REGEXP '".$_GET['search']."')) temp $order Limit $start, $perpage"); 
		}else{
			$allsoilages = $wpdb->get_results("SELECT * from (SELECT ss1.id as soilageid,ss1.name as soilage,ss2.id as surfaceid,ss2.name as surface,ss3.name as category,ss3.id as categoryid,ss4.id as productid,ss4.name as product, ss4.date_created as date_created, ss4.date_modified as date_modified FROM `soil_search_4` ss4 left join  `soil_search_3` ss3 on (ss4.3_id = ss3.id) left join `soil_search_2` ss2 on (ss4.2_id = ss2.id) left join `soil_search_1` ss1 on (ss4.1_id = ss1.id)) temp $order Limit $start, $perpage"); 
		} ?>
		<form method="post" action="" id="soilageProducts">
			<input id="selectallSoilage" name="selectallSoilage" type="checkbox" value="" style="margin:0 28px 0 10px">
			<input type="submit" value="Delete" name="deleteSoilage" style="background-color: #bcbcbc; cursor: pointer; border:1px solid #202020; color: #202020; font-size: 15px; font-weight: 600; padding: 3px 20px;" />
			<span style="display: inline-block; float: none; text-align: center; width: 66%;">
				<?php /* ?>
				<label>Search Products:</label>
				<select id="soilage" name="products" class="search_product flexselect">
					<option value=""></option>
					<?php $args = array(
							'post_type'			=> 'product',
							'post_status'		=> 'publish',
							'posts_per_page'	=> -1,
							'orderby' 			=> 'title',
							'order' 			=> 'ASC' 
						); 
						$products = get_posts($args);
						foreach($products as $product){
							echo '<option value="'.$product->post_title.'" '.$selected.'>'.$product->post_title.'</option>';
						}
					?>
				</select>
				<input type="submit" name="search_product" value="Go" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer;" />
				* <?php */ ?>
				<label>Search:</label>
				<input type="text" name="search_keyword" value="">
				<input type="submit" name="search_category" value="Go" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer;" />
			</span>
			<?php $rpp_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
			if($_GET['rpp'] != ''){
				$parse_url = parse_url($rpp_url);
				$query = explode('&',$parse_url['query']);
				unset($query[count($query)-1]);
				if(count($query) >1){
					$query = implode('&',$query);
				}else{
					$query = $query[0];
				}
				$rpp_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
			} ?>
			<span style="float:right;margin-right:40px;">Record per Page  
				<select name="Records" onchange="redirect_to('<?php echo $rpp_url; ?>',this.value)">
					<?php for($i=1; $i<=6; $i++) {  ?>
						<option value="<?php echo $i*100?>" <?php if($perpage==$i*100){echo 'selected';} ?>><?php echo $i*100 ?></option>
					<?php } ?>
				</select>
			</span>
			<?php if(!empty($allsoilages)){
				if($_GET['order'] == '' || $_GET['order'] == 'desc'){
					$class = 'desc';
				}elseif($_GET['order'] == 'asc'){
					$class = 'asc';
				}
				if($_GET['order'] == '' || $_GET['order'] == 'asc'){
					$Pclass = 'asc';
				}elseif($_GET['order'] == 'desc'){
					$Pclass = 'desc';
				} ?>
				<table style="width:100%; margin-top:20px; background-color:#fffoverflow-x: hidden; overflow-y: auto; display: inline-block; height: 600px;" class="main-table-bottom" cellspacing="1" cellpadding="10" bgcolor="#fff" >
					<tr>
						<th width="5%" style="border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"></th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=soilage&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=soilage&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Soilage</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=surface&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=surface&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Surface</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=category&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=category&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Sub Category</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=product&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=product&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Products</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_created&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_created&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Date Created</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_modified&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_modified&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Date Modified</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="10%" style="border-bottom:1px solid #f0f0f0;">Action</th>
					</tr>
					<?php $k = 0; foreach($allsoilages as $allsoilage) {  
							$surfaces = $wpdb->get_results("select * from soil_search_2 where FIND_IN_SET('".$allsoilage->id."')");
						?>
						<tr>
							<td width="5%" style="border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><input type="checkbox" class="selectsingleSoilage" name="selectsingleSoilage[<?php echo $k; ?>]" type="checkbox" value="<?php echo $allsoilage->productid; ?>" /></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allsoilage->soilage; ?></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allsoilage->surface; ?></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allsoilage->category; ?></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allsoilage->product; ?></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo ($allsoilage->date_created != 0 ? date('d/m/Y',$allsoilage->date_created) : ''); ?></td>
							<td width="15%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo ($allsoilage->date_modified != 0 ? date('d/m/Y',$allsoilage->date_modified) : ''); ?></td> 
							<td width="10%" style="border-bottom:1px solid #f0f0f0;">
								<a class="edit-link" style="font-weight:bold; margin-right:30px;" href="?page=manage_psw&type=<?php echo ($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); ?>&tab=addProductsSoilage&subtab=<?php echo ($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesS'); ?>&action=edit&soilageid=<?php echo ($allsoilage->soilageid != '' ? $allsoilage->soilageid : 0); ?>&surfaceid=<?php echo ($allsoilage->surfaceid != '' ? $allsoilage->surfaceid : 0); ?>&categoryid=<?php echo ($allsoilage->categoryid != '' ? $allsoilage->categoryid : 0); ?>&name=<?php echo urlencode($allsoilage->product); ?>">Edit</a>
								<?php /* ?>
								<a class="edit-link" style="font-weight:bold" href="?page=manage_psw&tab=<?php echo ($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); ?>&subtab=<?php echo ($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesS'); ?>&p=<?php echo $_GET['p']; ?>&rpp=<?php echo $_GET['rpp']; ?>&action=delete&soilageid=<?php echo $allsoilage->soilageid; ?>&surfaceid=<?php echo $allsoilage->surfaceid; ?>&name=<?php echo $allsoilage->product; ?>">Delete</a>
								<?php */ ?>
							</td>
						</tr>
					<?php $k++; } ?>
				</table>
			<?php }else{ 
				echo '<h4>Data related to '.$_GET['search'].' not found</h4>';
			} ?>
		</form>
		<?php 
		if($_GET['search'] != ''){
			$allsoilages = $wpdb->get_results("SELECT ss1.id as soilageid,ss1.name as soilage,ss2.id as surfaceid,ss2.name as surface,ss3.name as category,ss3.id as categoryid,ss4.id as productid,ss4.name as product FROM `soil_search_4` ss4 left join `soil_search_3` ss3 on (ss4.3_id = ss3.id) left join `soil_search_2` ss2 on (ss4.2_id = ss2.id) left join `soil_search_1` ss1 on (ss4.1_id = ss1.id) where ss4.name REGEXP '".$_GET['search']."'");
		}else{
			$allsoilages = $wpdb->get_results("SELECT ss1.id as soilageid,ss1.name as soilage,ss2.id as surfaceid,ss2.name as surface,ss3.name as category,ss3.id as categoryid,ss4.id as productid,ss4.name as product FROM `soil_search_4` ss4 left join  `soil_search_3` ss3 on (ss4.3_id = ss3.id) left join `soil_search_2` ss2 on (ss4.2_id = ss2.id) left join `soil_search_1` ss1 on (ss4.1_id = ss1.id)");
		}
		$total = count($allsoilages);
		if($total > 0){
			$totalPages = ceil($total / $perpage);
			if($totalPages > 1){
				if($page <=1 ){
					echo "<span id='page_prev' style='font-weight:bold;'>Prev</span>";
				}else{
					$j = $page - 1;
					echo "<span><a id='page_prev' href='".$rpp_url."&p=$j&rpp=".$perpage."'> Prev</a>&nbsp</span>";
				}
				for($i=1; $i <= $totalPages; $i++){
					if($i<>$page) {
						echo "<span><a href='".$rpp_url."&p=$i&rpp=".$perpage."' id='page_a_link'> $i</a>&nbsp</span>";
					}else{
						echo "<span id='page_links' style='font-weight:bold;'>&nbsp$i</span>";
					}
				}
				if($page == $totalPages ){
					echo "<span id='page_next' style='font-weight:bold;'>&nbsp Next ></span>";
				}else{
					$j = $page + 1;
					echo "<span><a href='".$rpp_url."&p=$j&rpp=".$perpage."' id='page_next'> Next</a>&nbsp</span>";
				}
			}
		}
	}
	if ( $_GET['tab'] == 'area' && ($_GET['action'] == '' || !isset($_GET['action']) || $_GET['action'] == 'delete' ) ){
		if(isset($_REQUEST["rpp"])){
			$perpage = ($_REQUEST["rpp"]); 
		}else{ 
			$perpage = 50;	 
		}
		if(isset($_REQUEST["p"])){
			$page = ($_REQUEST["p"]); 
		}else{ 
			$page = 1;	 
		}
		if(isset($_REQUEST["action"]) && $_REQUEST["action"] == 'delete'){
			$wpdb->delete( 
				'area_search_6',
				array( 
					'1_id' => $_GET['areaid'],
					'2_id' => $_GET['subcategory1_id'],
					'3_id' => $_GET['subcategory2_id'],
					'4_id' => $_GET['subcategory3_id'],
					'5_id' => $_GET['subcategory4_id'],
					'name' => $_GET['name'],
				), 
				array( 
					'%d',
					'%d',
					'%d',
					'%d',
					'%d',
					'%s'
				)
			);
		}
		if($_REQUEST['deleteArea']){
			if(!empty($_POST['selectsingleArea'])) { 
				$checked_value = $_POST['selectsingleArea'];
				foreach($checked_value as $val) {
					$wpdb->delete(
						'area_search_6',
						array( 'id' => $val ),
						array( '%d' )
					);
				}
			}
		}
		if($_REQUEST['search_category']){
			$url = admin_url().'admin.php?page=manage_psw&tab='.($_GET['tab'] != '' ? $_GET['tab'] : 'soilage').'&search='.$_REQUEST['search_keyword']; 
			echo "<script>window.location.href='".$url."'</script>";
		}
		$url = admin_url().'admin.php?page=manage_psw&tab='.($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); 
		$start=($page-1)*$perpage;
		$current_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
		if($_GET['order'] != '' && $_GET['orderby'] != ''){
			$parse_url = parse_url($current_url);
			$query = explode('&',$parse_url['query']);
			unset($query[count($query)-1]);
			unset($query[count($query)-1]);
			if(count($query) >1){
				$query = implode('&',$query);
			}else{
				$query = $query[0];
			}
			$current_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
		}
		if($_GET['order'] != '' && $_GET['orderby'] != ''){
			$order = 'order by '.$_GET['orderby'].' '.$_GET['order'];
		}else{
			$order = 'order by productid desc';
		}
		if($_GET['search'] != ''){
			$allareas = $wpdb->get_results("SELECT * from (SELECT as1.id as areaid,as1.name as area,as2.id as subcategory1_id,as2.name as subcategory1,as3.id as subcategory2_id,as3.name as subcategory2,as4.id as subcategory3_id,as4.name as subcategory3,as5.id as subcategory4_id,as5.name as subcategory4,as6.id as productid,as6.name as product, as6.date_created as date_created, as6.date_modified FROM `area_search_6` as6 left join `area_search_5` as5 on (as6.5_id = as5.id) left join `area_search_4` as4 on (as6.4_id = as4.id) left join `area_search_3` as3 on (as6.3_id = as3.id) left join `area_search_2` as2 on (as6.2_id = as2.id) left join `area_search_1` as1 on (as6.1_id = as1.id) where (as6.name REGEXP '".$_GET['search']."' || as1.name REGEXP '".$_GET['search']."' || as2.name REGEXP '".$_GET['search']."' || as3.name REGEXP '".$_GET['search']."' || as4.name REGEXP '".$_GET['search']."' || as5.name REGEXP '".$_GET['search']."')) temp $order Limit $start, $perpage");
		}else{
			$allareas = $wpdb->get_results("SELECT * from (SELECT as1.id as areaid,as1.name as area,as2.id as subcategory1_id,as2.name as subcategory1,as3.id as subcategory2_id,as3.name as subcategory2,as4.id as subcategory3_id,as4.name as subcategory3,as5.id as subcategory4_id,as5.name as subcategory4,as6.id as productid,as6.name as product, as6.date_created as date_created, as6.date_modified as date_modified FROM `area_search_6` as6 left join `area_search_5` as5 on (as6.5_id = as5.id) left join `area_search_4` as4 on (as6.4_id = as4.id) left join `area_search_3` as3 on (as6.3_id = as3.id) left join `area_search_2` as2 on (as6.2_id = as2.id) left join `area_search_1` as1 on (as6.1_id = as1.id)) temp $order Limit $start, $perpage");
		} ?>
		
		  
		<form method="post" action="" id="areaProducts">
			<input id="selectallArea" name="selectallarea" type="checkbox" value="" style="margin:0 28px 0 10px">
			<input type="submit" value="Delete" name="deleteArea" style="background-color: #bcbcbc; cursor: pointer; border:1px solid #202020; color: #202020; font-size: 15px; font-weight: 600; padding: 3px 20px;" />
			<span style="display: inline-block; float: none; text-align: center; width: 66%;">
				<?php /* ?>
				<label>Search Products:</label>
				<select id="area" name="products" class="search_product flexselect">
					<option value=""></option>
					<?php $args = array(
							'post_type'			=> 'product',
							'post_status'		=> 'publish',
							'posts_per_page'	=> -1,
							'orderby' 			=> 'title',
							'order' 			=> 'ASC' 
						); 
						$products = get_posts($args);
						foreach($products as $product){
							echo '<option value="'.$product->post_title.'" '.$selected.'>'.$product->post_title.'</option>';
						}
					?>
				</select>
				<input type="submit" name="search_product" value="Go" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer;" />
				<?php */ ?>
				<label>Search:</label>
				<input type="text" name="search_keyword" value="">
				<input type="submit" name="search_category" value="Go" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer;" />
			</span>
			<?php $rpp_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
			if($_GET['rpp'] != ''){
				$parse_url = parse_url($rpp_url);
				$query = explode('&',$parse_url['query']);
				unset($query[count($query)-1]);
				if(count($query) >1){
					$query = implode('&',$query);
				}else{
					$query = $query[0];
				}
				$rpp_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
			} ?>
			<span style="float:right;margin-right:40px;">Record per Page  
				<select name="Records" onchange="redirect_to('<?php echo $rpp_url; ?>',this.value)">
					<?php for($i=1; $i<=6; $i++) {  ?>
						<option value="<?php echo $i*50?>" <?php if($perpage==$i*50){echo 'selected';} ?>><?php echo $i*50 ?></option>
					<?php } ?>
				</select>
			</span>
			<?php if(!empty($allareas)){
				if($_GET['order'] == '' || $_GET['order'] == 'desc'){
					$class = 'desc';
				}elseif($_GET['order'] == 'asc'){
					$class = 'asc';
				} 
				if($_GET['order'] == '' || $_GET['order'] == 'asc'){
					$Pclass = 'asc';
				}elseif($_GET['order'] == 'desc'){
					$Pclass = 'desc';
				} ?>
				<table style="width:100%; margin-top:20px; background-color:#fffoverflow-x: hidden; overflow-y: auto; display: inline-block; height: 600px;" class="main-table-bottom" cellspacing="1" cellpadding="10" bgcolor="#fff" >
					<tr>
						<th width="5%" style="border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"></th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=area&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=area&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Area</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory1&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory1&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Sub-Category 1</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory2&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory2&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Sub-Category 2</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory3&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory3&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Sub-Category 3</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory4&order=asc' ?>">
							<?php }elseif($_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=subcategory4&order=desc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Sub-Category 4</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=product&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=product&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Products</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_created&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_created&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Date Created</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Pclass; ?>">
							<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_modified&order=desc' ?>">
							<?php }elseif($_GET['order'] == 'desc'){ ?>
								<a href="<?php echo $current_url.'&orderby=date_modified&order=asc' ?>">
							<?php } ?>
								<span style="display:inline-block; float:left;">Date Modified</span>
								<span class="sorting-indicator"></span>
							</a>
						</th>
						<th width="10%" style="border-bottom:1px solid #f0f0f0;">Action</th>
					</tr>
					<?php $k = 0; foreach($allareas as $allarea) {  
							$surfaces = $wpdb->get_results("select * from soil_search_2 where FIND_IN_SET('".$allsoilage->id."')");
							
						?>
						<tr>
							<td width="5%" style="border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><input type="checkbox" class="selectsingleArea" name="selectsingleArea[<?php echo $k; ?>]" type="checkbox" value="<?php echo $allarea->productid; ?>" /></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->area; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->subcategory1; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->subcategory2; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->subcategory3; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->subcategory4; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $allarea->product; ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo ($allarea->date_created != 0 ? date('d/m/Y',$allarea->date_created) : ''); ?></td>
							<td width="14%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo ($allarea->date_modified != 0 ? date('d/m/Y',$allarea->date_modified) : ''); ?></td>
							<td width="10%" style="border-bottom:1px solid #f0f0f0;">
								<a class="edit-link" style="font-weight:bold; margin-right:10px;" href="?page=manage_psw&type=<?php echo ($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); ?>&tab=addProductsArea&subtab=<?php echo ($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesA'); ?>&action=edit&areaid=<?php echo ($allarea->areaid != '' ? $allarea->areaid : 0); ?>&subcategory1=<?php echo ($allarea->subcategory1_id != '' ? $allarea->subcategory1_id : 0); ?>&subcategory2=<?php echo ($allarea->subcategory2_id != '' ? $allarea->subcategory2_id : 0); ?>&subcategory3=<?php echo ($allarea->subcategory3_id != '' ? $allarea->subcategory3_id : 0); ?>&subcategory4=<?php echo ($allarea->subcategory4_id != '' ? $allarea->subcategory4_id : 0); ?>&name=<?php echo urlencode($allarea->product); ?>">Edit</a>
								<?php /* ?>
								<a class="edit-link" style="font-weight:bold" href="?page=manage_psw&tab=<?php echo ($_GET['tab'] != '' ? $_GET['tab'] : 'soilage'); ?>&subtab=<?php echo ($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesA'); ?>&action=delete&areaid=<?php echo $allarea->areaid; ?>&subcategory1=<?php echo $allarea->subcategory1_id; ?>&subcategory2=<?php echo $allarea->subcategory2_id; ?>&subcategory3=<?php echo $allarea->subcategory3_id; ?>&subcategory4=<?php echo $allarea->subcategory4_id; ?>&name=<?php echo $allarea->product; ?>">Delete</a>
								<?php */ ?>
							</td>
						</tr>
					<?php $k++; } ?>
				</table>
			<?php }else{
				echo '<h4>Data related to '.$_GET['search'].' not found</h4>';
			} ?>
		</form>
		<?php 
		if($_GET['search'] != ''){
			$allareas = $wpdb->get_results("SELECT as1.id as areaid,as1.name as area,as2.id as subcategory1_id,as2.name as subcategory1,as3.id as subcategory2_id,as3.name as subcategory2,as4.id as subcategory3_id,as4.name as subcategory3,as5.id as subcategory4_id,as5.name as subcategory4,as6.id as productid,as6.name as product FROM `area_search_6` as6 left join `area_search_5` as5 on (as6.5_id = as5.id) left join `area_search_4` as4 on (as6.4_id = as4.id) left join `area_search_3` as3 on (as6.3_id = as3.id) left join `area_search_2` as2 on (as6.2_id = as2.id) left join `area_search_1` as1 on (as6.1_id = as1.id) where as6.name REGEXP '".$_GET['search']."'");
		}else{
			$allareas = $wpdb->get_results("SELECT as1.id as areaid,as1.name as area,as2.id as subcategory1_id,as2.name as subcategory1,as3.id as subcategory2_id,as3.name as subcategory2,as4.id as subcategory3_id,as4.name as subcategory3,as5.id as subcategory4_id,as5.name as subcategory4,as6.id as productid,as6.name as product FROM `area_search_6` as6 left join `area_search_5` as5 on (as6.5_id = as5.id) left join `area_search_4` as4 on (as6.4_id = as4.id) left join `area_search_3` as3 on (as6.3_id = as3.id) left join `area_search_2` as2 on (as6.2_id = as2.id) left join `area_search_1` as1 on (as6.1_id = as1.id)");
		}
		
		$total = count($allareas);
		if($total > 0){
			$totalPages = ceil($total / $perpage);
			if($page <=1 ){
				echo "<span id='page_prev' style='font-weight:bold;'>Prev</span>";
			}else{
				$j = $page - 1;
				echo "<span><a id='page_prev' href='".$rpp_url."&p=$j&rpp=".$perpage."'> Prev</a>&nbsp</span>";
			}
			for($i=1; $i <= $totalPages; $i++){
				if($i<>$page) {
					echo "<span><a href='".$rpp_url."&p=$i&rpp=".$perpage."' id='page_a_link'> $i</a>&nbsp</span>";
				}else{
					echo "<span id='page_links' style='font-weight:bold;'>&nbsp$i</span>";
				}
			}
			if($page == $totalPages ){
				echo "<span id='page_next' style='font-weight:bold;'>&nbsp Next ></span>";
			}else{
				$j = $page + 1;
				echo "<span><a href='".$rpp_url."&p=$j&rpp=".$perpage."' id='page_next'> Next</a>&nbsp</span>";
			}
		}
	}
	if($_GET['tab'] == 'addProductsSoilage'){
		if($_REQUEST['save_soilage']){
			if($_GET['action']=='edit'){
				$result = $wpdb->get_results("SELECT * from soil_search_4 where 1_id = ".$_GET['soilageid']." and 2_id = ".$_GET['surfaceid']." and 3_id = ".$_GET['categoryid']." and name REGEXP '".$_GET['name']."'");
				$rowID = $result[0]->id;
				$wpdb->update( 
					'soil_search_4', 
					array( 
						'1_id' 			=> $_POST['soilage'], 
						'2_id' 			=> $_POST['surface'],
						'3_id' 			=> $_POST['sub_category'],
						'name' 			=> $_POST['product'],
						'date_modified'	=> time()
					), 
					array('id'	=>	$rowID),
					array( 
						'%d', 
						'%d', 
						'%d', 
						'%s',
						'%d' 
					) 
				);
				$href = get_bloginfo('url')."/wp-admin/admin.php?page=manage_psw&type=".($_GET['tab'] != '' ? $_GET['tab'] : 'soilage')."&tab=addProductsSoilage&subtab=".($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesS')."&action=edit&soilageid=".$_POST['soilage']."&surfaceid=".$_POST['surface']."&categoryid=".$_POST['sub_category']."&name=".urlencode($_POST['product']); ?>
				<script>
					window.location.href = '<?php echo $href ?>';
				</script>
			<?php }else{
				$products = array();
				$products = $_REQUEST['products'];
				$soilageID = $_REQUEST['soilage'];
				$surfaceID = $_REQUEST['surface'];
				$sub_categoryID = $_REQUEST['sub_category'];
				//Surface
				$surface_name = $wpdb->get_results("SELECT * from soil_search_2 where id = ".$surfaceID);
				$result = $wpdb->get_results("select * from soil_search_2 where FIND_IN_SET('".$soilageID."',1_id) and name= '".$surface_name[0]->name."'");
				if(empty($result)){
					$result = $wpdb->get_results("SELECT 1_id from soil_search_2 where id = ".$surfaceID);
					$soils = get_object_vars($result[0]);
					if($soils['1_id'] != '')
						$Soil = $soils['1_id'].','.$soilageID;
					else
						$Soil = $soilageID;
					$arr = explode(',',$soils['1_id']);
					if (!in_array($soilageID, $arr)){
						$wpdb->update( 
							'soil_search_2', 
							array( 
								'1_id' => $Soil
							), 
							array('id'	=>	$surfaceID),
							array( 
								'%s'
							),
							array( '%d' ) 
						);
					}
				}
				
				//Sub Category
				$cat_name = $wpdb->get_results("SELECT * from soil_search_3 where id = ".$sub_categoryID."");
				$result = $wpdb->get_results("SELECT * from soil_search_3 where 1_id = ".$soilageID." and 2_id = ".$surfaceID." and name REGEXP '".$cat_name[0]->name."' ");
				if(empty($result) && $cat_name[0]->name != ''){
					$wpdb->insert( 
						'soil_search_3', 
						array( 
							'name'	=> $cat_name[0]->name,
							'1_id'	=> $soilageID,
							'2_id'	=> $surfaceID
						), 
						array( 
							'%s',
							'%d',
							'%d'
						) 
					);
					$sub_categoryID = $wpdb->insert_id;
				}else{
					$sub_categoryID = $result[0]->id;
				}
				
				//Products Links
				foreach($products as $product){
					$result = $wpdb->get_results("SELECT * from soil_search_4 where 1_id = ".$soilageID." and 2_id = ".$surfaceID." and 3_id = ".$sub_categoryID." and name REGEXP '".$product."'");
					if(empty($result)){
						$wpdb->insert( 
							'soil_search_4', 
							array( 
								'1_id' 			=> $soilageID, 
								'2_id' 			=> $surfaceID,
								'3_id' 			=> $sub_categoryID,
								'name' 			=> $product,
								'date_created'	=>	time()
							), 
							array( 
								'%d', 
								'%d', 
								'%d', 
								'%s' 
							) 
						);
					}
				}
				
			}
		} 
		if($_REQUEST['add_soilage']){
			$soilage = $_REQUEST['new_soilage'];
			if($soilage != ''){
				$wpdb->insert( 
						'soil_search_1', 
						array( 
							'name' => $soilage
						), 
						array( 
							'%s' 
						) 
					);
			}
		}
		if($_REQUEST['add_surface']){
			$soilages = array();
			$surface = $_REQUEST['new_surface'];
			$soilages = $_REQUEST['soilage'];
			$result = $wpdb->get_results("SELECT * from soil_search_2 where name REGEXP '".$surface."' ");
			if($surface != '' && empty($result)){
				$row_inserted = $wpdb->insert( 
						'soil_search_2', 
						array( 
							'name' => $surface
						), 
						array( 
							'%s' 
						) 
					);
				$insertedID = $wpdb->insert_id;
			}else{
				$insertedID = $result[0]->id;
			}
			if($insertedID != ''){
				foreach($soilages as $soilage){
					$result = $wpdb->get_results("SELECT 1_id from soil_search_2 where id = ".$insertedID." ");
					$soils = get_object_vars($result[0]);
					if($soils['1_id'] != '')
						$Soil = $soils['1_id'].','.$soilage;
					else
						$Soil = $soilage;
					$arr = explode(',',$soils['1_id']);
					if (!in_array($soilage, $arr)){
						$wpdb->update( 
							'soil_search_2', 
							array( 
								'1_id' => $Soil
							), 
							array('id'	=>	$insertedID),
							array( 
								'%s'
							),
							array( '%d' ) 
						);
					}
				}
			}
		}
		if($_REQUEST['add_sub_category']){
			$new_sub_category = $_REQUEST['new_sub_category'];
			$surface = $_REQUEST['surface'];
			$soilage = $_REQUEST['soilage'];
			$result = $wpdb->get_results("SELECT * from soil_search_3 where 1_id = ".$soilage." and 2_id = ".$surface." and name REGEXP '".$new_sub_category."' ");
			if(empty($result)){
				$wpdb->insert( 
					'soil_search_3', 
					array( 
						'name'	=> $new_sub_category,
						'1_id'	=> $soilage,
						'2_id'	=> $surface
					), 
					array( 
						'%s',
						'%d',
						'%d'
					) 
				);
			}
		} 
		echo '<h1>Add/edit products</h1>';
		$subtabs = array( 
			'addCategoriesS' => 'Add Products to Soilage',
			'editCategoriesS' => 'Edit Categories'
		);
		echo '<div id="icon-themes" class="icon32"><br></div>';
		echo '<h2 class="nav-tab-wrapper">';
		$current = ($_GET['subtab'] != '') ? $_GET['subtab'] : '';
		$tab = $_GET['tab'];
		foreach( $subtabs as $subtab => $name ){
			$class = ( $subtab == $current ) ? ' nav-tab-active' : '';
			echo "<a class='nav-tab$class' href='?page=manage_psw&tab=$tab&subtab=$subtab'>$name</a>";
		}
		echo '</h2>'; 
		if($_GET['subtab'] == 'addCategoriesS'){ ?>
		<div class="block_left" <?php if($_GET['action'] == 'edit') echo 'style="display:none"'; ?>>
			<div class="add_soilage">
				<h3>Add New Soilage</h3>
				<form action="" method="POST">
					<span>
						<label>Add new Soilage</label>
						<input type="text" name="new_soilage" value="">
					</span>
					<span>
						<input type="submit" name="add_soilage" class="button button-primary" value="Add">
					</span>
				</form>
			</div>
			<div class="add_surface">
				<h3>Add New Surface</h3>
				<form action="" method="POST">
					<span>
						<label>Add new Surface</label>
						<input type="text" name="new_surface" value="">
					</span>
					<span>
						<label>Link with Soilage</label>
						<select name="soilage[]" multiple="multiple" tabindex="1" size=6>
							<?php $soilages = $wpdb->get_results("SELECT * from soil_search_1"); 
								foreach($soilages as $soilage){
									echo '<option value="'.$soilage->id.'">'.$soilage->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<input type="submit" name="add_surface" class="button button-primary" value="Add">
					</span>
				</form>
			</div>
			<div class="add_surface">
					<h3>Add New Sub-Category</h3>
					<form action="" method="POST">
						<span>
							<label>Add Sub-Category</label>
							<input type="text" name="new_sub_category" value="">
						</span>
						<span>
							<label>Link with Soilage</label>
							<!--select name="sub_category2[]" multiple="multiple" tabindex="1" size=6-->
							<select name="soilage">
								<option value="0">Select Soilage</option>
								<?php $soilages = $wpdb->get_results("SELECT * from soil_search_1 order by name ASC"); 
									foreach($soilages as $soilage){
										echo '<option value="'.$soilage->id.'">'.$soilage->name.'</option>';
									} ?>
							</select>
						</span>
						<span>
							<label>Link with Surface</label>
							<!--select name="sub_category3[]" multiple="multiple" tabindex="1" size=6-->
							<select name="surface">
								<option value="0">Select Surface</option>
								<?php $surfaces = $wpdb->get_results("SELECT * from soil_search_2 order by name ASC"); 
									foreach($surfaces as $surface){
										echo '<option value="'.$surface->id.'">'.$surface->name.'</option>';
									} ?>
							</select>
						</span>
						<span>
							<input type="submit" name="add_sub_category" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
		</div>
		<div class="block_right">
			<div class="loaderSmall"></div>
			<?php if($_GET['action'] == 'edit'){
				echo '<h3>Edit product to Soilage Section</h3>';
			}else{
				echo '<h3>Add products to Soilage Section</h3>'; 
			}
			?>
			<form class="product_linkS" id="product_linkS" action="" method="POST">
				<span>
					<label>Select Soilage</label>
					<select name="soilage"  class="required">
						<option value="0">Select Soilage</option>
						<?php $soilages = $wpdb->get_results("SELECT * from soil_search_1 order by name ASC"); 
							foreach($soilages as $soilage){
								if($_GET['soilageid'] == $soilage->id){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$soilage->id.'" '.$selected.'>'.$soilage->name.'</option>';
							}
						?>
					</select>
				</span>
				<span>
					<label>Select Surface</label>
					<select name="surface">
						<option value="0">Select Surface</option>
						<?php if($_GET['action'] == 'edit'){
							$surfaces = $wpdb->get_results("SELECT * from soil_search_2 order by name ASC");
						}else{
							$surfaces = $wpdb->get_results("SELECT * from soil_search_2 group by name order by name ASC");
						} 
							foreach($surfaces as $surface){
								if($_GET['surfaceid'] == $surface->id){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$surface->id.'" '.$selected.'>'.$surface->name.'</option>';
							}
						?>
					</select>
				</span>
				<span>
					<label>Select Sub-Category</label>
					<select name="sub_category">
						<option value="0">Select Sub-Category</option>
						<?php if($_GET['action'] == 'edit'){
							$categories = $wpdb->get_results("SELECT * from soil_search_3 order by name ASC");
						}else{
							$categories = $wpdb->get_results("SELECT * from soil_search_3 group by name order by name ASC");
						}
							foreach($categories as $category){
								if($_GET['categoryid'] == $category->id){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$category->id.'" '.$selected.'>'.$category->name.'</option>';
							}
						?>
					</select>
				</span>
				<span>
					<label>Select Products</label>
					<?php if($_GET['action'] == 'edit'){ ?>
						<select name="product">
							<option value="0">Select Product</option>
					<?php }else{ ?>
						<select name="products[]" class="required" multiple="multiple" tabindex="1" size=10>
					<?php } ?>
						<?php $args = array(
								'post_type'			=> 'product',
								'post_status'		=> 'publish',
								'posts_per_page'	=> -1,
								'orderby' 			=> 'title',
								'order' 			=> 'ASC' 
							); 
							$products = get_posts($args);
							foreach($products as $product){
								if(trim(strtolower(urldecode($_GET['name']))) == trim(strtolower($product->post_title))){
									$selected = 'selected="selected"';
								}else{
									$selected = '';
								}
								echo '<option value="'.$product->post_title.'" '.$selected.'>'.$product->post_title.'</option>';
							}
						?>
					</select>
					<?php if($_GET['action'] != 'edit'){ ?>
						<p>Note: Press CTRL and select multiple products.</p>
					<?php } ?>
				</span>
				<span>
					<input type="submit" name="save_soilage" class="button button-primary" value="Save">
				</span>
			</form>
		</div>
		<?php }
		if($_GET['subtab']=='editCategoriesS'){ 
			if($_REQUEST['edit_soilage']){
				$soilage = $_REQUEST['soilage_to_edit'];
				$rename_soilage = $_REQUEST['rename_soilage'];
				if($soilage != '' && $rename_soilage != ''){
					$wpdb->update( 
						'soil_search_1', 
						array( 
							'name' => $rename_soilage
						), 
						array('id' => $soilage),
						array( 
							'%s' 
						) 
					);
				}
			}
			if($_REQUEST['delete_soilage']){
				$soilage = $_REQUEST['soilage_to_edit'];
				if($soilage != ''){
					$wpdb->delete(
						'soil_search_1',
						array( 'id' => $soilage ),
						array( '%d' )
					);
					$wpdb->delete(
						'soil_search_4',
						array( '1_id' => $soilage ),
						array( '%d' )
					);
				}
			}
			if($_REQUEST['edit_surface']){
				$surface = $_REQUEST['surface_to_edit'];
				$rename_surface = $_REQUEST['rename_surface'];
				if($surface != '' && $rename_surface != ''){
					$wpdb->update( 
						'soil_search_2', 
						array( 
							'name' => $rename_surface
						), 
						array('id' => $surface),
						array( 
							'%s' 
						) 
					);
				}
			}
			if($_REQUEST['delete_surface']){
				$surface = $_REQUEST['surface_to_edit'];
				if($surface != ''){
					$wpdb->delete(
						'soil_search_2',
						array( 'id' => $surface ),
						array( '%d' )
					);
					$wpdb->delete(
						'soil_search_4',
						array( '2_id' => $surface ),
						array( '%d' )
					);
				}
			}
			if($_REQUEST['edit_category']){
				$category = $_REQUEST['category_to_edit'];
				$rename_category = $_REQUEST['rename_category'];
				if($category != '' && $rename_category != ''){
					$cat_name = $wpdb->get_results("SELECT * from soil_search_3 where id = ".$category."");
					$wpdb->update( 
						'soil_search_3', 
						array( 
							'name' => $rename_category
						), 
						array('name' => $cat_name[0]->name),
						array( 
							'%s' 
						) 
					);
				}
			}
			if($_REQUEST['delete_category']){
				$category = $_REQUEST['category_to_edit'];
				$cat_name = $wpdb->get_results("SELECT * from soil_search_3 where id = ".$category."");
				if($category != ''){
					$wpdb->delete(
						'soil_search_3',
						array( 'name' => $cat_name[0]->name ),
						array( '%s' )
					);
					$wpdb->delete(
						'soil_search_4',
						array( '3_id' => $category ),
						array( '%d' )
					);
				}
			}
			
			?>
			<div class="block_left">
				<h3>Edit Soilage:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Soilage: </label>
						<select name="soilage_to_edit">
							<option value="0">Select Soilage</option>
							<?php $soilages = $wpdb->get_results("SELECT * from soil_search_1 order by name ASC"); 
								foreach($soilages as $soilage){
									echo '<option value="'.$soilage->id.'">'.$soilage->name.'</option>';
								} ?>
						</select>
					</span>
					<span>
						<label>Rename selected Soilage: </label>
						<input type="text" name="rename_soilage" value="">
					</span>
					<span>
						<input type="submit" name="edit_soilage" class="button button-primary" value="Save">
						<input type="submit" name="delete_soilage" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</div>
			<div class="block_right">
				<h3>Edit Surface:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Surface</label>
						<select name="surface_to_edit">
							<option value="0">Select Surface</option>
							<?php $surfaces = $wpdb->get_results("SELECT * from soil_search_2 order by name ASC"); 
								foreach($surfaces as $surface){
									echo '<option value="'.$surface->id.'">'.$surface->name.'</option>';
								} ?>
						</select>
					</span>
					<span>
						<label>Rename selected Surface: </label>
						<input type="text" name="rename_surface" value="">
					</span>
					<span>
						<input type="submit" name="edit_surface" class="button button-primary" value="Save">
						<input type="submit" name="delete_surface" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</div>
			<div class="block_left">
				<h3>Edit Sub Category:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Sub Category: </label>
						<select name="category_to_edit">
							<option value="0">Select Sub Category</option>
							<?php $categories = $wpdb->get_results("SELECT * from soil_search_3 group by name order by name ASC"); 
								foreach($categories as $category){
									echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								} ?>
						</select>
					</span>
					<span>
						<label>Rename selected Sub Category: </label>
						<input type="text" name="rename_category" value="">
					</span>
					<span>
						<input type="submit" name="edit_category" class="button button-primary" value="Save">
						<input type="submit" name="delete_category" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</div>
		<?php } 
		}
	if($_GET['tab'] == 'addProductsArea'){
		echo '<h1>Add/edit products</h1>';
		$subtabs = array( 
			'addCategoriesA' => 'Add Products to Area',
			'editCategoriesA' => 'Edit Categories'
		);
		echo '<div id="icon-themes" class="icon32"><br></div>';
		echo '<h2 class="nav-tab-wrapper">';
		$current = ($_GET['subtab'] != '') ? $_GET['subtab'] : '';
		$tab = $_GET['tab'];
		foreach( $subtabs as $subtab => $name ){
			$class = ( $subtab == $current ) ? ' nav-tab-active' : '';
			echo "<a class='nav-tab$class' href='?page=manage_psw&tab=$tab&subtab=$subtab'>$name</a>";
		}
		echo '</h2>';
		if($_GET['subtab']=='addCategoriesA'){ 
			if($_REQUEST['save_area']){
				if($_GET['action']=='edit'){
					$result = $wpdb->get_results("SELECT * from area_search_6 where 1_id = ".$_GET['areaid']." and 2_id = ".$_GET['subcategory1']." and 3_id = ".$_GET['subcategory2']." and 4_id = ".$_GET['subcategory3']." and 5_id = ".$_GET['subcategory4']." and name REGEXP '".$_GET['name']."'");
					$rowID = $result[0]->id;
					$wpdb->update( 
						'area_search_6', 
						array( 
							'1_id' 			=> $_REQUEST['areaid'], 
							'2_id' 			=> $_POST['sub_category1'],
							'3_id' 			=> $_POST['sub_category2'],
							'4_id' 			=> $_POST['sub_category3'],
							'5_id' 			=> $_POST['sub_category4'],
							'name' 			=> $_POST['product'],
							'date_modified'	=> time()
						), 
						array('id'	=>	$rowID)
					);
					$href = get_bloginfo('url')."/wp-admin/admin.php?page=manage_psw&type=".($_GET['tab'] != '' ? $_GET['tab'] : 'soilage')."&tab=addProductsArea&subtab=".($_GET['subtab'] != '' ? $_GET['subtab'] : 'addCategoriesA')."&action=edit&areaid=".($_REQUEST['areaid'] != '' ? $_REQUEST['areaid'] : 0)."&subcategory1=".($_POST['sub_category1'] != '' ? $_POST['sub_category1'] : 0)."&subcategory2=".($_POST['sub_category2'] != '' ? $_POST['sub_category2'] : 0)."&subcategory3=".($_POST['sub_category3'] != '' ? $_POST['sub_category3'] : 0)."&subcategory4=".($_POST['sub_category4'] != '' ? $_POST['sub_category4'] : 0)."&name=".urlencode($_POST['product']); ?>
					<script>
						window.location.href = '<?php echo $href ?>';
					</script>
				<?php }else{
					$products = array();
					$products = $_REQUEST['products'];
					$areaID = $_REQUEST['area'];
					$sub_category1ID = $_REQUEST['sub_category1'];
					$sub_category2ID = $_REQUEST['sub_category2'];
					$sub_category3ID = $_REQUEST['sub_category3'];
					$sub_category4ID = $_REQUEST['sub_category4'];
					//Sub-Category 1
					if($sub_category1ID != 0){
						$cat_name = $wpdb->get_results("SELECT * from area_search_2 where id = ".$sub_category1ID);
						$result = $wpdb->get_results("SELECT * from area_search_2 where 1_id = ".$areaID." and name REGEXP '".$cat_name[0]->name."' ");
						if(empty($result) && $cat_name[0]->name != ''){
							$wpdb->insert( 
								'area_search_2', 
								array( 
									'name'	=> $cat_name[0]->name,
									'1_id'	=> $areaID
								), 
								array( 
									'%s',
									'%d'
								) 
							);
							$sub_category1ID = $wpdb->insert_id;
						}else{
							$sub_category1ID = $result[0]->id;
						}
					}
					
					//Sub-Category 2
					if($sub_category2ID != 0){
						$cat_name = $wpdb->get_results("SELECT * from area_search_3 where id = ".$sub_category2ID);
						$result = $wpdb->get_results("SELECT * from area_search_3 where 1_id = ".$areaID." and 2_id = ".$sub_category1ID." and name REGEXP '".$cat_name[0]->name."' ");
						if(empty($result) && $cat_name[0]->name != ''){
							$wpdb->insert( 
								'area_search_3', 
								array( 
									'name'	=> $cat_name[0]->name,
									'1_id'	=> $areaID,
									'2_id'	=> $sub_category1ID,
								), 
								array( 
									'%s',
									'%d',
									'%d'
								) 
							);
							$sub_category2ID = $wpdb->insert_id;
						}else{
							$sub_category2ID = $result[0]->id;
						}
					}
					
					//Sub-Category 3
					if($sub_category3ID != 0){
						$cat_name = $wpdb->get_results("SELECT * from area_search_4 where id = ".$sub_category3ID);
						if(!empty($cat_name)){
							$result = $wpdb->get_results("SELECT * from area_search_4 where 3_id = ".$sub_category2ID." and name REGEXP '".$cat_name[0]->name."' ");
						}
						if(empty($result) && $cat_name[0]->name != ''){
							$wpdb->insert( 
								'area_search_4', 
								array( 
									'name'	=> $cat_name[0]->name,
									'3_id'	=> $sub_category2ID
								), 
								array( 
									'%s',
									'%d'
								) 
							);
							$sub_category3ID = $wpdb->insert_id;
						}else{
							$sub_category3ID = $result[0]->id;
						}
					}
					
					//Sub-Category 4
					if($sub_category4ID != 0){
						$cat_name = $wpdb->get_results("SELECT * from area_search_5 where id = ".$sub_category4ID);
						$result = $wpdb->get_results("SELECT * from area_search_5 where 3_id = ".$sub_category2ID." and 4_id = ".$sub_category3ID." and name REGEXP '".$cat_name[0]->name."' ");
						if(empty($result) && $cat_name[0]->name != ''){
							$wpdb->insert( 
								'area_search_5', 
								array( 
									'name'	=> $cat_name[0]->name,
									'3_id'	=> $sub_category2ID,
									'4_id'	=> $sub_category3ID
								), 
								array( 
									'%s',
									'%d',
									'%d'
								) 
							);
							$sub_category4ID = $wpdb->insert_id;
						}else{
							$sub_category4ID = $result[0]->id;
						}
					}
					
					//Product Link
					foreach($products as $product){
						$result = $wpdb->get_results("SELECT * from area_search_6 where 1_id = ".$areaID." and 2_id = ".$sub_category1ID." and 3_id = ".$sub_category2ID." and 4_id = ".$sub_category3ID." and 5_id = ".$sub_category4ID." and name REGEXP '".$product."'");
						if(empty($result)){
							$wpdb->insert( 
								'area_search_6', 
								array( 
									'1_id' 			=> $areaID, 
									'2_id' 			=> $sub_category1ID,
									'3_id' 			=> $sub_category2ID,
									'4_id' 			=> $sub_category3ID,
									'5_id' 			=> $sub_category4ID,
									'name' 			=> $product,
									'date_created'	=> time()
								), 
								array( 
									'%d',
									'%d',
									'%d',
									'%d',
									'%d',
									'%s' 
								) 
							);
						}
					}
				}
			} 
			if($_REQUEST['add_area']){
				$area = $_REQUEST['new_area'];
				if($area != ''){
					$wpdb->insert( 
						'area_search_1', 
						array( 
							'name' => $area
						), 
						array( 
							'%s' 
						) 
					);
				}
			}
			if($_REQUEST['add_sub_category1']){
				$areas = array();
				$new_sub_category1 = $_REQUEST['new_sub_category1'];
				$areas = $_REQUEST['area'];
				if(!empty($areas)){
					foreach($areas as $area){
						$result = $wpdb->get_results("SELECT * from area_search_2 where 1_id = ".$area." and name REGEXP '".$new_sub_category1."' ");
						if(empty($result)){
							$wpdb->insert( 
								'area_search_2', 
								array( 
									'name'	=> $new_sub_category1,
									'1_id'	=> $area
								), 
								array( 
									'%s',
									'%d' 
								) 
							);
						}
					}
				}else{
					$error['add_sub_category1'] = 'Please Select Area to Link';
				}
			}
			if($_REQUEST['add_sub_category2']){
				$areas = array();
				$new_sub_category2 = $_REQUEST['new_sub_category2'];
				$sub_categories = $_REQUEST['sub_category1'];
				if(!empty($sub_categories)){
					foreach($sub_categories as $sub_category){
						$result = $wpdb->get_results("SELECT * from area_search_3 where 2_id = ".$sub_category." and name REGEXP '".$new_sub_category2."' ");
						if(empty($result)){
							$wpdb->insert( 
								'area_search_3', 
								array( 
									'name'	=> $new_sub_category2,
									'2_id'	=> $sub_category
								), 
								array( 
									'%s',
									'%d' 
								) 
							);
						}
					}
				}else{
					$error['add_sub_category2'] = 'Please Select Sub Category 1 to Link';
				}
			}
			if($_REQUEST['add_sub_category3']){
				$areas = array();
				$new_sub_category3 = $_REQUEST['new_sub_category3'];
				$sub_categories = $_REQUEST['sub_category2'];
				if(!empty($sub_categories)){
					foreach($sub_categories as $sub_category){
						$result = $wpdb->get_results("SELECT * from area_search_4 where 3_id = ".$sub_category." and name REGEXP '".$new_sub_category3."' ");
						if(empty($result)){
							$wpdb->insert( 
								'area_search_4', 
								array( 
									'name'	=> $new_sub_category3,
									'3_id'	=> $sub_category
								), 
								array( 
									'%s',
									'%d' 
								) 
							);
						}
					}
				}
				else{
					$error['add_sub_category3'] = 'Please Select Sub Category 2 to Link';
				}
			}
			if($_REQUEST['add_sub_category4']){
				$new_sub_category4 = $_REQUEST['new_sub_category4'];
				$sub_category2 = $_REQUEST['sub_category2'];
				$sub_category3 = $_REQUEST['sub_category3'];
				$result = $wpdb->get_results("SELECT * from area_search_5 where 3_id = ".$sub_category2." and 4_id = ".$sub_category3." and name REGEXP '".$new_sub_category4."' ");
				if(empty($result)){
					$wpdb->insert( 
						'area_search_5', 
						array( 
							'name'	=> $new_sub_category4,
							'3_id'	=> $sub_category2,
							'4_id'	=> $sub_category3
						), 
						array( 
							'%s',
							'%d',
							'%d'
						) 
					);
				}
			} 
			if(!empty($error)){
				foreach($error as $er){
					echo '<p style="color:#ff0000;">'.$er.'</p>';
				}
			}
			?>
			<div class="block_left" <?php if($_GET['action'] == 'edit') echo 'style="display:none"'; ?>>
				<div class="add_soilage">
					<h3>Add New Area</h3>
					<form action="" method="POST">
						<span>
							<label>Add new Area</label>
							<input type="text" name="new_area" value="">
						</span>
						<span>
							<input type="submit" name="add_area" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
				<div class="add_surface">
					<h3>Add New Sub-Category 1</h3>
					<form action="" method="POST">
						<span>
							<label>Add Sub-Category</label>
							<input type="text" name="new_sub_category1" value="">
						</span>
						<span>
							<label>Link with Area</label>
							<select name="area[]" multiple="multiple" tabindex="1" size=6>
								<?php $areas = $wpdb->get_results("SELECT * from area_search_1 order by name ASC"); 
									foreach($areas as $area){
										echo '<option value="'.$area->id.'">'.$area->name.'</option>';
									}
								?>
							</select>
						</span>
						<span>
							<input type="submit" name="add_sub_category1" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
				<div class="add_surface">
					<h3>Add New Sub-Category 2</h3>
					<form action="" method="POST">
						<span>
							<label>Add Sub-Category</label>
							<input type="text" name="new_sub_category2" value="">
						</span>
						<span>
							<label>Link with Sub-Category 1</label>
							<select name="sub_category1[]" multiple="multiple" tabindex="1" size=6>
								<?php $sub_categories = $wpdb->get_results("SELECT * from area_search_2 group by name order by name ASC");  
									foreach($sub_categories as $sub_category){
										echo '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
									} ?>
							</select>
						</span>
						<span>
							<input type="submit" name="add_sub_category2" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
				<div class="add_surface">
					<h3>Add New Sub-Category 3</h3>
					<form action="" method="POST">
						<span>
							<label>Add Sub-Category</label>
							<input type="text" name="new_sub_category3" value="">
						</span>
						<span>
							<label>Link with Sub-Category 2</label>
							<select name="sub_category2[]" multiple="multiple" tabindex="1" size=6>
								<?php $sub_categories = $wpdb->get_results("SELECT * from area_search_3 group by name order by name ASC"); 
									foreach($sub_categories as $sub_category){
										echo '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
									} ?>
							</select>
						</span>
						<span>
							<input type="submit" name="add_sub_category3" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
				<div class="add_surface">
					<h3>Add New Sub-Category 4</h3>
					<form action="" method="POST">
						<span>
							<label>Add Sub-Category</label>
							<input type="text" name="new_sub_category4" value="">
						</span>
						<span>
							<label>Link with Sub-Category 2</label>
							<!--select name="sub_category2[]" multiple="multiple" tabindex="1" size=6-->
							<select name="sub_category2">
								<option value="0">Select Sub-Category 2</option>
								<?php $sub_categories = $wpdb->get_results("SELECT * from area_search_3 group by name order by name ASC"); 
									foreach($sub_categories as $sub_category){
										echo '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
									} ?>
							</select>
						</span>
						<span>
							<label>Link with Sub-Category 3</label>
							<!--select name="sub_category3[]" multiple="multiple" tabindex="1" size=6-->
							<select name="sub_category3">
								<option value="0">Select Sub-Category 3</option>
								<?php $sub_categories = $wpdb->get_results("SELECT * from area_search_4 group by name order by name ASC"); 
									foreach($sub_categories as $sub_category){
										echo '<option value="'.$sub_category->id.'">'.$sub_category->name.'</option>';
									} ?> 
							</select>
						</span>
						<span>
							<input type="submit" name="add_sub_category4" class="button button-primary" value="Add">
						</span>
					</form>
				</div>
			</div>
			<div class="block_right">
				<div class="loaderSmall"></div>
				<?php if($_GET['action'] == 'edit'){
					echo '<h3>Edit products to Area Section</h3>';
				}else{
					echo '<h3>Add products to Area Section</h3>'; 
				} ?>
				<form class="product_linkA" id="product_linkA" action="" method="POST">
					<span>
						<label>Select Area</label>
						<select name="area" class="required">
							<option value="0">Select Area</option>
							<?php $areas = $wpdb->get_results("SELECT * from area_search_1 order by name ASC"); 
								foreach($areas as $area){
									if($_GET['areaid'] == $area->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}
									echo '<option value="'.$area->id.'" '.$selected.'>'.$area->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Select Sub-Category 1</label>
						<select name="sub_category1">
							<option value="0">Select Sub-Category 1</option>
							<?php if($_GET['action'] == 'edit'){
								$categories = $wpdb->get_results("SELECT * from area_search_2 group by name order by name ASC"); 
								$cat = $wpdb->get_results("SELECT name from area_search_2 where id=".$_GET['subcategory1']); 
							}else{
								$categories = $wpdb->get_results("SELECT * from area_search_2 group by name order by name ASC");   
							}
								foreach($categories as $category){
									/*if($_GET['subcategory1'] == $category->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}*/
									if($cat[0]->name == $category->name){
										$cat_id = $_GET['subcategory1'];   
										$selected = 'selected="selected"';
									}else{
										$cat_id = $category->id;
										$selected = '';
									}
									echo '<option value="'.$cat_id.'" '.$selected.'>'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Select Sub-Category 2</label>
						<select name="sub_category2">
							<option value="0">Select Sub-Category 2</option>
							<?php if($_GET['action'] == 'edit'){
								$categories = $wpdb->get_results("SELECT * from area_search_3 group by name order by name ASC"); 
								$cat = $wpdb->get_results("SELECT name from area_search_3 where id=".$_GET['subcategory2']);
							}else{
								$categories = $wpdb->get_results("SELECT * from area_search_3 group by name order by name ASC"); 
							}
								foreach($categories as $category){
									/*if($_GET['subcategory2'] == $category->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}*/
									if($cat[0]->name == $category->name){
										$cat_id = $_GET['subcategory2'];
										$selected = 'selected="selected"';
									}else{
										$cat_id = $category->id;
										$selected = '';
									}
									echo '<option value="'.$cat_id.'" '.$selected.'>'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Select Sub-Category 3</label>
						<select name="sub_category3">
							<option value="0">Select Sub-Category 3</option>
							<?php if($_GET['action'] == 'edit'){
								$categories = $wpdb->get_results("SELECT * from area_search_4 group by name order by name ASC"); 
								$cat = $wpdb->get_results("SELECT name from area_search_4 where id=".$_GET['subcategory3']); 
							}else{
								$categories = $wpdb->get_results("SELECT * from area_search_4 group by name order by name ASC"); 
							}
								foreach($categories as $category){
									/*if($_GET['subcategory3'] == $category->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}*/
									if($cat[0]->name == $category->name){
										$cat_id = $_GET['subcategory3'];
										$selected = 'selected="selected"';
									}else{
										$cat_id = $category->id;
										$selected = '';
									}
									echo '<option value="'.$cat_id.'" '.$selected.'>'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Select Sub-Category 4</label>
						<select name="sub_category4">
							<option value="0">Select Sub-Category 4</option>
							<?php if($_GET['action'] == 'edit'){
								$categories = $wpdb->get_results("SELECT * from area_search_5  group by name order by name ASC"); 
								$cat = $wpdb->get_results("SELECT name from area_search_5 where id=".$_GET['subcategory4']); 
							}else{
								$categories = $wpdb->get_results("SELECT * from area_search_5  group by name order by name ASC"); 
							}
								foreach($categories as $category){
									/*if($_GET['subcategory4'] == $category->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}*/
									if($cat[0]->name == $category->name){
										$cat_id = $_GET['subcategory4'];
										$selected = 'selected="selected"';
									}else{
										$cat_id = $category->id;
										$selected = '';
									}
									echo '<option value="'.$cat_id.'" '.$selected.'>'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Select Products</label>
						<?php if($_GET['action'] == 'edit'){ ?>
							<select name="product">
								<option value="0">Select Product</option>
						<?php }else{ ?>
							<select name="products[]" class="required" multiple="multiple" tabindex="1" size=10>
						<?php } ?>
							<?php $args = array(
									'post_type'			=> 'product',
									'post_status'		=> 'publish',
									'posts_per_page'	=> -1,
									'orderby' 			=> 'title',
									'order' 			=> 'ASC' 
								); 
								$products = get_posts($args);
								foreach($products as $product){
									if(trim(strtolower(urldecode($_GET['name']))) == trim(strtolower($product->post_title))){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}
									echo '<option value="'.$product->post_title.'" '.$selected.'>'.$product->post_title.'</option>';
								}
							?>
						</select>
						<?php if($_GET['action'] != 'edit'){ ?>
							<p>Note: Press CTRL and select multiple products.</p>
						<?php } ?>
					</span>
					<span>
						<input type="submit" name="save_area" class="button button-primary" value="Save">
					</span>
				</form>
			</div>
	<?php } 
	if($_GET['subtab']=='editCategoriesA'){
		if($_REQUEST['edit_area']){
			$area = $_REQUEST['area_to_edit'];
			$rename_area = $_REQUEST['rename_area'];
			if($area != '' && $rename_area != ''){
				$wpdb->update( 
					'area_search_1', 
					array( 
						'name' => $rename_area
					), 
					array('id' => $area),
					array( 
						'%s' 
					) 
				);
			}
		}
		if($_REQUEST['delete_area']){
			$area = $_REQUEST['area_to_edit'];
			if($area != ''){
				$wpdb->delete(
					'area_search_1',
					array( 'id' => $area ),
					array( '%d' )
				);
				$wpdb->delete(
					'area_search_6',
					array( '1_id' => $area ),
					array( '%d' )
				);
			}
		}
		if($_REQUEST['edit_category_1']){
			$category1 = $_REQUEST['category1_to_edit'];
			$rename_category1 = $_REQUEST['rename_category_1'];
			if($category1 != '' && $rename_category1 != ''){
				$wpdb->update( 
					'area_search_2', 
					array( 
						'name' => $rename_category1
					), 
					array('id' => $category1),
					array( 
						'%s' 
					) 
				);
			}
		}
		if($_REQUEST['delete_category_1']){
			$category1 = $_REQUEST['category1_to_edit'];
			if($category1 != ''){
				$wpdb->delete(
					'area_search_2',
					array( 'id' => $category1 ),
					array( '%d' )
				);
				$wpdb->delete(
					'area_search_6',
					array( '2_id' => $category1 ),
					array( '%d' )
				);
			}
		}
		if($_REQUEST['edit_category_2']){
			$category2 = $_REQUEST['category2_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_3 where id=".$category2);
			$rename_category2 = $_REQUEST['rename_category_2'];
			if($category2 != '' && $rename_category2 != ''){
				$wpdb->update( 
					'area_search_3', 
					array( 
						'name' => $rename_category2
					), 
					array('name' => $categories[0]->name),
					array( 
						'%s' 
					) 
				);
			} 
		}
		if($_REQUEST['delete_category_2']){
			$category2 = $_REQUEST['category2_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_3 where id=".$category2);
			if($category2 != ''){
				$wpdb->delete(
					'area_search_3',
					array( 'name' => $categories[0]->name ),
					array( '%s' )
				);
				$wpdb->delete(
					'area_search_6',
					array( '3_id' => $category2 ),
					array( '%d' )
				);
			}
		}
		if($_REQUEST['edit_category_3']){
			$category3 = $_REQUEST['category3_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_4 where id=".$category3);
			$rename_category3 = $_REQUEST['rename_category_3'];
			if($category3 != '' && $rename_category3 != ''){
				$wpdb->update( 
					'area_search_4', 
					array( 
						'name' => $rename_category3
					), 
					array('name' => $categories[0]->name),
					array( 
						'%s' 
					) 
				);
			}
		}
		if($_REQUEST['delete_category_3']){
			$category3 = $_REQUEST['category3_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_4 where id=".$category3);
			if($category3 != ''){
				$wpdb->delete(
					'area_search_4',
					array( 'name' => $categories[0]->name ),
					array( '%s' )
				);
				$wpdb->delete(
					'area_search_6',
					array( '4_id' => $category3 ),
					array( '%d' )
				);
			}
		}
		if($_REQUEST['edit_category_4']){
			$category4 = $_REQUEST['category4_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_5 where id=".$category4);
			$rename_category4 = $_REQUEST['rename_category_4'];
			if($category4 != '' && $rename_category4 != ''){
				$wpdb->update( 
					'area_search_5', 
					array( 
						'name' => $rename_category4
					), 
					array('name' => $categories[0]->name),
					array( 
						'%s' 
					) 
				);
			}
		}
		if($_REQUEST['delete_category_4']){
			$category4 = $_REQUEST['category4_to_edit'];
			$categories = $wpdb->get_results("SELECT name from area_search_4 where id=".$category4);
			if($category4 != ''){
				$wpdb->delete(
					'area_search_5',
					array( 'name' => $categories[0]->name ),
					array( '%s' )
				);
				$wpdb->delete(
					'area_search_5',
					array( '5_id' => $category4 ),
					array( '%d' )
				); 
			}
		}
		
		
		
		?>
		<div class="block_left">
			<span>
				<h3>Edit Area:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Area: </label>
						<select name="area_to_edit">
							<option value="0">Select Area</option>
							<?php $areas = $wpdb->get_results("SELECT * from area_search_1 order by name ASC"); 
								foreach($areas as $area){
									if($_GET['soilageid'] == $area->id){
										$selected = 'selected="selected"';
									}else{
										$selected = '';
									}
									echo '<option value="'.$area->id.'" '.$selected.'>'.$area->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Rename selected Area: </label>
						<input type="text" name="rename_area" value="">
					</span>
					<span>
						<input type="submit" name="edit_area" class="button button-primary" value="Save">
						<input type="submit" name="delete_area" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</span>
			<span>
				<h3>Edit Sub-Category 2:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Sub-Category 2: </label>
						<select name="category2_to_edit">
							<option value="0">Select Sub-Category 2</option>
							<?php $categories = $wpdb->get_results("SELECT * from area_search_3 group by name order by name ASC"); 
								foreach($categories as $category){
									echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Rename selected Category 2: </label>
						<input type="text" name="rename_category_2" value="">
					</span>
					<span>
						<input type="submit" name="edit_category_2" class="button button-primary" value="Save">
						<input type="submit" name="delete_category_2" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</span>
			<span>
				<h3>Edit Sub-Category 4:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Sub-Category 4: </label>
						<select name="category4_to_edit">
							<option value="0">Select Sub-Category 4</option>
							<?php $categories = $wpdb->get_results("SELECT * from area_search_5 order by name ASC"); 
								foreach($categories as $category){
									echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Rename selected Category 4: </label>
						<input type="text" name="rename_category_4" value="">
					</span>
					<span>
						<input type="submit" name="edit_category_4" class="button button-primary" value="Save">
						<input type="submit" name="delete_category_4" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</span>
		</div>
		<div class="block_right">
			<span>
				<h3>Edit Sub-Category 1:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Sub-Category 1: </label>
						<select name="category1_to_edit">
							<option value="0">Select Sub-Category 1</option>
							<?php $categories = $wpdb->get_results("SELECT * from area_search_2 order by name ASC"); 
								foreach($categories as $category){
									echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Rename selected Category 1: </label>
						<input type="text" name="rename_category_1" value="">
					</span>
					<span>
						<input type="submit" name="edit_category_1" class="button button-primary" value="Save">
						<input type="submit" name="delete_category_1" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</span>
			<span>
				<h3>Edit Sub-Category 3:</h3>
				<form action="" method="POST">
					<span>
						<label>Edit Sub-Category 3: </label>
						<select name="category3_to_edit">
							<option value="0">Select Sub-Category 1</option>
							<?php $categories = $wpdb->get_results("SELECT * from area_search_4 order by name ASC"); 
								foreach($categories as $category){
									echo '<option value="'.$category->id.'">'.$category->name.'</option>';
								}
							?>
						</select>
					</span>
					<span>
						<label>Rename selected Category 3: </label>
						<input type="text" name="rename_category_3" value="">
					</span>
					<span>
						<input type="submit" name="edit_category_3" class="button button-primary" value="Save">
						<input type="submit" name="delete_category_3" class="button button-primary delete" value="Delete">
					</span>
				</form>
			</span>
		</div>
	<?php }
	} ?>
<style>
	.block_left, .block_right{float: left; width:50%;}
	.block_left span{margin-bottom:30px; float: left; width: 100%;}
	.block_right span{margin-bottom:30px; float: left; width: 100%;}
	.block_left span .delete, .block_right span .delete{ margin-left: 30px;}
	.main-table-bottom { background:#fff !important;} 
	.block_right {position:relative;}
	.loaderSmall {width:75%; height:95%; position: absolute; text-align:center; line-height:100%; vertical-align:middle; left:0; top:0;  background:url(<?php bloginfo('template_url')?>/images/loading.gif) center center no-repeat rgba(0,0,0,0.7); display:none;}
	.sorting-indicator{ float: left; }
</style>
<?php }

function psw_scripts() {
	wp_register_style( 'psw_css', get_bloginfo('template_url').'/css/flexselect.css', false, '1.0.0');
	wp_enqueue_style( 'psw_css' );
	wp_register_script( 'flexselect', get_bloginfo('template_url').'/js/jquery.flexselect.js', array( 'jquery' ));
	wp_enqueue_script( 'flexselect' );
	wp_register_script( 'liquidmetal', get_bloginfo('template_url').'/js/liquidmetal.js', array( 'jquery' ));
	wp_enqueue_script( 'liquidmetal' );
	wp_register_script( 'validate', get_bloginfo('template_url').'/js/jquery.validate.min.js', array( 'jquery' ));
	wp_enqueue_script( 'validate' );
}

add_action( 'admin_enqueue_scripts', 'psw_scripts' );


//Soilage Ajax
add_action( 'wp_ajax_select_soilage', 'select_soilage_callback' );
add_action( 'wp_ajax_nopriv_select_soilage', 'select_soilage_callback' );

function select_soilage_callback() {
	global $wpdb;
	$results = $wpdb->get_results('select DISTINCT(name) from soil_search_4 where 1_id='.$_POST['soilage_id'].' group by `name` ORDER BY `name` ASC');
	$data = array();
	foreach($results as $result){
		$data[] = trim($result->name);
	}
	$data = array_unique($data);
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}

//Surface Ajax
add_action( 'wp_ajax_select_surface', 'select_surface_callback' );
add_action( 'wp_ajax_nopriv_select_surface', 'select_surface_callback' );

function select_surface_callback() {
	global $wpdb;
	$where = '';
	if($_POST['soilage_id'] != ''){
		$where = '1_id='.$_POST['soilage_id'];
	}
	if($_POST['surface_id'] != ''){
		$where = '2_id='.$_POST['surface_id'];
	}
	if($_POST['surface_id'] != '' && $_POST['soilage_id'] != ''){
		$where = '1_id='.$_POST['soilage_id'].' and 2_id='.$_POST['surface_id'];
	}
	$results = $wpdb->get_results('select DISTINCT(name) from soil_search_4 where '.$where.' group by `name` ORDER BY `name` ASC');
	$data = array();
	foreach($results as $result){
		$data[] = trim($result->name);
	}
	$data = array_unique($data);
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}

//Soil Sub-Category Ajax
add_action( 'wp_ajax_select_soil_category', 'select_soil_category_callback' );
add_action( 'wp_ajax_nopriv_select_soil_category', 'select_soil_category_callback' );

function select_soil_category_callback() {
	global $wpdb;
	$where = '';
	$data = array();
	$result = $wpdb->get_results('select id from soil_search_3 where 1_id='.$_POST['soilage_id'].' and 2_id='.$_POST['surface_id'].' and name="'.$_POST['category_name'].'"');
	if(!empty($result)){
		if($_POST['category_id'] != '' && $_POST['soilage_id'] != '' && $_POST['surface_id'] == ''){
			$where = '1_id='.$_POST['soilage_id'].' and 3_id='.$result[0]->id;
		}
		if($_POST['category_id'] != '' && $_POST['surface_id'] != '' && $_POST['soilage_id'] == ''){
			$where = '2_id='.$_POST['surface_id'].' and 3_id='.$result[0]->id;
		}
		if($_POST['category_id'] != '' && $_POST['surface_id'] != '' && $_POST['soilage_id'] != '' ){
			$where = '1_id='.$_POST['soilage_id'].' and 2_id='.$_POST['surface_id'].' and 3_id='.$result[0]->id;
		}
		$results = $wpdb->get_results('select DISTINCT(name) from soil_search_4 where '.$where.' group by `name` ORDER BY `name` ASC');
		foreach($results as $result){
			$data[] = trim($result->name);
		}
		$data = array_unique($data);
	}
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}


//Area Ajax
add_action( 'wp_ajax_select_area_admin', 'select_area_admin_callback' );
add_action( 'wp_ajax_nopriv_select_admin_area', 'select_area_admin_callback' );

function select_area_admin_callback() {
	global $wpdb;
	$results = $wpdb->get_results('select DISTINCT(name) from area_search_6 where 1_id='.$_POST['area_id'].' group by `name` ORDER BY `name` ASC');
	$data = array();
	foreach($results as $result){
		$data[] = trim($result->name);
	}
	$data = array_unique($data);
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}


//Sub-Category 1 Ajax
add_action( 'wp_ajax_select_subCat1', 'select_subCat1_callback' );
add_action( 'wp_ajax_nopriv_select_subCat1', 'select_subCat1_callback' );

function select_subCat1_callback() {
	global $wpdb;
	$where = '';
	if($_POST['area_id'] != ''){
		$where = '1_id='.$_POST['area_id'];
	}
	if($_POST['subcat1_id'] != ''){
		$where = '2_id='.$_POST['subcat1_id'];
	}
	if($_POST['area_id'] != '' && $_POST['subcat1_id'] != ''){
		$where = '1_id='.$_POST['area_id'].' and 2_id='.$_POST['subcat1_id'];
	}
	$results = $wpdb->get_results('select DISTINCT(name) from area_search_6 where '.$where.' group by `name` ORDER BY `name` ASC');
	$data = array();
	foreach($results as $result){
		$data[] = trim($result->name);
	}
	$data = array_unique($data);
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}

//Sub-Category 2 Ajax
add_action( 'wp_ajax_select_subCat2', 'select_subCat2_callback' );
add_action( 'wp_ajax_nopriv_select_subCat2', 'select_subCat2_callback' );

function select_subCat2_callback() {
	global $wpdb;
	$where = '';
	$data = array();
	$result = $wpdb->get_results('select id from area_search_3 where 1_id='.$_POST['area_id'].' and 2_id='.$_POST['subcat1_id'].' and name="'.$_POST['subcat2_name'].'"');
	if(!empty($result)){
		if($_POST['subcat2_id'] != '' && $_POST['area_id'] != '' && $_POST['subcat1_id'] == ''){
			$where = '1_id='.$_POST['area_id'].' and 3_id='.$result[0]->id;
		}
		if($_POST['subcat2_id'] != '' && $_POST['subcat1_id'] != '' && $_POST['area_id'] == ''){
			$where = '2_id='.$_POST['subcat1_id'].' and 3_id='.$result[0]->id;
		}
		if($_POST['subcat2_id'] != '' && $_POST['subcat1_id'] != '' && $_POST['area_id'] != '' ){
			$where = '1_id='.$_POST['area_id'].' and 2_id='.$_POST['subcat1_id'].' and 3_id='.$result[0]->id;
		}
		$results = $wpdb->get_results('select DISTINCT(name) from area_search_6 where '.$where.' group by `name` ORDER BY `name` ASC');
		foreach($results as $result){
			$data[] = trim($result->name);
		}
		$data = array_unique($data);
	}
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}


//Sub-Category 3 Ajax
add_action( 'wp_ajax_select_subCat3', 'select_subCat3_callback' );
add_action( 'wp_ajax_nopriv_select_subCat3', 'select_subCat3_callback' );

function select_subCat3_callback() {
	global $wpdb;
	$where = '';
	if($_POST['subcat2_id'] != ''){
		$where = '3_id='.$_POST['subcat2_id'];
	}
	if($_POST['subcat3_id'] != ''){
		$where = '4_id='.$_POST['subcat3_id'];
	}
	if($_POST['subcat2_id'] != '' && $_POST['subcat3_id'] != ''){
		$where = '3_id='.$_POST['subcat2_id'].' and 4_id='.$_POST['subcat3_id'];
	}
	$results = $wpdb->get_results('select DISTINCT(name) from area_search_6 where '.$where.' group by `name` ORDER BY `name` ASC');
	$data = array();
	foreach($results as $result){
		$data[] = trim($result->name);
	}
	$data = array_unique($data);
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}

//Sub-Category 4 Ajax
add_action( 'wp_ajax_select_subCat4', 'select_subCat4_callback' );
add_action( 'wp_ajax_nopriv_select_subCat4', 'select_subCat4_callback' );

function select_subCat4_callback() {
	global $wpdb;
	$where = '';
	$data = array();
	$result = $wpdb->get_results('select id from area_search_5 where 3_id='.$_POST['subcat2_id'].' and 4_id='.$_POST['subcat3_id'].' and name="'.$_POST['subcat5_name'].'"');
	if(!empty($result)){
		if($_POST['subcat4_id'] != '' && $_POST['subcat2_id'] != '' && $_POST['subcat3_id'] == ''){
			$where = '3_id='.$_POST['subcat2_id'].' and 5_id='.$result[0]->id;
		}
		if($_POST['subcat4_id'] != '' && $_POST['subcat3_id'] != '' && $_POST['subcat2_id'] == ''){
			$where = '4_id='.$_POST['subcat3_id'].' and 5_id='.$result[0]->id;
		}
		if($_POST['subcat4_id'] != '' && $_POST['subcat2_id'] != '' && $_POST['subcat3_id'] != '' ){
			$where = '3_id='.$_POST['subcat2_id'].' and 4_id='.$_POST['subcat3_id'].' and 5_id='.$result[0]->id;
		}
		$results = $wpdb->get_results('select DISTINCT(name) from area_search_6 where '.$where.' group by `name` ORDER BY `name` ASC');
		foreach($results as $result){
			$data[] = trim($result->name);
		}
		$data = array_unique($data);
	}
	if(!empty($data)){
		echo json_encode(array_values($data));
	}else{
		echo json_encode('none');
	}
	die();
}
