<?php 
add_action('admin_menu', 'register_custom_hot_product_page');

function register_custom_hot_product_page() {
	add_submenu_page( 'edit.php?post_type=product', 'Hot Product', 'Hot Product', 'manage_options', 'hot_product', 'custom_hot_product_page_callback' ); 
}

function custom_hot_product_page_callback() { 
	if(isset($_POST['save'])){
		$hot_product = array(
			'hot_title'		=> $_POST['hot_title'],
			'hot_link'		=> $_POST['hot_link'],
			'hot_image'		=> $_POST['hot_image'],
			'hot_content'	=> $_POST['hot_content']
		);
		update_option( 'agar_hot_product', serialize($hot_product), '', 'yes' );
	}
	$hot_product = unserialize(get_option('agar_hot_product'));
	?>
	<h2>Hot Product</h2>
	<h4>Select the title, content and link of the Hot Product section in front page.</h4>
	<form method="post" action="">
			<div style="float:left; width:96%; margin-bottom:0px; margin-top:10px; padding:15px; background:#fff; border-bottom:1px solid #f0f0f0">
				<span style="float:left; width:15%; font-weight:bold; line-height: 28px;">Title</span>
				<input type="text" name="hot_title" value="<?php echo $hot_product['hot_title']; ?>" size="40" >
			</div>
			<div style="float:left; width:96%; margin-bottom:0px; padding:15px; background:#fff; border-bottom:1px solid #f0f0f0">
				<span style="float:left; width:15%; font-weight:bold; line-height: 28px;">Choose the Link</span>
				<?php query_posts('post_type=product&posts_per_page=-1&post_status=publish');  ?>
				<select name="hot_link" style="width:346px">
					<option value="" ><?php echo 'Select Product'; ?></option>
					<?php while ( have_posts() ) : the_post(); ?>
						<option value="<?php echo get_the_ID(); ?>" <?php if($hot_product['hot_link'] == get_the_ID()) { echo 'selected="selected"'; }?>><?php echo get_the_title(); ?></option>
					<?php endwhile; ?>
				</select>
			</div>
			<div style="float:left; width:96%; margin-bottom:0px; padding:15px; background:#fff; border-bottom:1px solid #f0f0f0">
				<span style="float:left; width:15%; font-weight:bold; line-height: 28px;">Image</span>
				<input type="text" name="hot_image" value="<?php echo $hot_product['hot_image']; ?>" id="hot_image" size="40" style="float:left;" >
				<?php if($hot_product['hot_image']){ ?>
					<span class="image_block">
						<img src="<?php echo $hot_product['hot_image']; ?>" width="200" height="200" style="margin-left:50px;" class="hot_image" />
						<img src="<?php bloginfo('template_url'); ?>/images/close_red.png" class="close" />
					</span>
				<?php } ?>
			</div>
			<div style="float:left; width:96%; margin-bottom:0px; padding:15px; background:#fff; border-bottom:1px solid #f0f0f0";>
				<span style="float:left; width:15%; font-weight:bold; line-height: 28px;">Content</span><br /><br />
					<?php /* <textarea class="ckeditor" name="mail_templateedit" id="mail_templateedit" rows="5" cols=""><?php echo $seldata[0]->mail_template; ?></textarea>  */ ?>
					<span style="float:left; width:60%">
						<?php $settings = array( 'textarea_name' => 'hot_content' );
						wp_editor( $hot_product['hot_content'], 'hot_contenteditor', $settings ); ?>
					</span>
			</div>
            <div style="float:left; width:96%; padding:15px; background:#fff; border-bottom:1px solid #f0f0f0">
			<input type="submit" value="Save Changes" name="save" class="button button-primary" />
            </div>
		</form>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				var file_frame;
				jQuery('#hot_image').live('click', function( event ){
					event.preventDefault();
					if ( file_frame ) {
						file_frame.open();
						return;
					}
					file_frame = wp.media.frames.file_frame = wp.media({
						title: jQuery( this ).data( 'uploader_title' ),
						button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
						},
						multiple: false
					});
					file_frame.on( 'select', function() {
						attachment = file_frame.state().get('selection').first().toJSON();
						jQuery('.image_block').show();
						jQuery('#hot_image').val(attachment.url);
						jQuery('.image_block img.hot_image').attr('src',attachment.url);
					});
					file_frame.open();
				});
				jQuery('.image_block img.close').live('click', function(){ 
					jQuery('#hot_image').val('');
					jQuery('.image_block').hide();
				});
			});
			
		</script>
		<style type="text/css">
			.gallery_images img{margin-right: 10px; margin-bottom: 6px;}
			.image_block{ position: relative; display: inline-block; }
			.image_block img.close{ position: absolute; top:-5px; right:-5px; width: 15px; cursor: pointer; }
		</style>

<?php }
