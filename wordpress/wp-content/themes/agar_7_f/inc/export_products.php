<?php

// Add Custom Taxonomies In woocommerce products

add_action( 'init', 'create_woocommerce_taxonomies', 0 );
function create_woocommerce_taxonomies() {
	$labels = array(
		'name'              => _x( 'Soilage', 'taxonomy general name' ),
		'singular_name'     => _x( 'Soilage', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Soilage' ),
		'all_items'         => __( 'All Soilages' ),
		'parent_item'       => __( 'Parent Soilage' ),
		'parent_item_colon' => __( 'Parent Soilage:' ),
		'edit_item'         => __( 'Edit Soilage' ),
		'update_item'       => __( 'Update Soilage' ),
		'add_new_item'      => __( 'Add New Soilage' ),
		'new_item_name'     => __( 'New Soilage Name' ),
		'menu_name'         => __( 'Soilage' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'soilage' ),
	);
	register_taxonomy( 'soilage', array( 'product' ), $args ); 
		
	$labels = array(
		'name'                       => _x( 'Surface', 'taxonomy general name' ),
		'singular_name'              => _x( 'Surface', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Surface' ),
		'popular_items'              => __( 'Popular Surface' ),
		'all_items'                  => __( 'All Surface' ),
		'parent_item'                => __( 'Parent Surface' ),
		'parent_item_colon'          => __( 'Parent Surface' ),
		'edit_item'                  => __( 'Edit Surface' ),
		'update_item'                => __( 'Update Surface' ),
		'add_new_item'               => __( 'Add New Surface' ),
		'new_item_name'              => __( 'New Surface Name' ),
		'menu_name'                  => __( 'Surfaces' ),
	);
	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'surface' ),
	);
	register_taxonomy( 'surface', array( 'product' ), $args );
}


add_action('admin_menu', 'register_custom_import_page');

function register_custom_import_page() {
	add_submenu_page( 'edit.php?post_type=product', 'Import Products', 'Import Products', 'manage_options', 'import_products', 'custom_import_page_callback' ); 
}

function custom_import_page_callback() { ?>
	
	<h2>Import Products</h2>
	<h4>Upload Excel file to import Woocommerce Products</h4>
	<?php 
	include 'excel/excel_reader2.php';
	include 'excel/SpreadsheetReader.php';
	include 'excel/SpreadsheetReader_XLSX.php';
	include 'excel/SpreadsheetReader_XLS.php'; 
	$upload_dir = wp_upload_dir();
	global $wpdb;
	if (isset($_POST['submit'])){
		$doc_root = rtrim($_SERVER['DOCUMENT_ROOT'],"/");
		$filename = $_FILES['file']['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if($ext == 'xls' || $ext == 'xlsx'){ 
		move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir['basedir']."/product_import/".$filename);
		$excel = new SpreadsheetReader($upload_dir['basedir']."/product_import/".$filename);
		}else{ 
			$errors = "Please upload file with xls or xlsx format";
		} 
		$activesheet = $excel->activeSheet();
		foreach($activesheet[0] as $active){
			$activeArray = get_object_vars($active);
			$activeSheet = $activeArray['@attributes']['activeTab'];
		}
		$Sheets = $excel->Sheets();
		$sheetArray = array();
		foreach ($Sheets as $Index => $Name)
		{ 
			$excel->ChangeSheet($Index); 
			foreach ($excel as $Row)
			{	
				$data['sheet_'.$Index][] = $Row;
			}	
		}
		$headingsArray = $data['sheet_0'][0];
		function add_attribute($attribute, $post_id,$taxonomy,$label){
			global $wpdb;
			if ( ! taxonomy_exists( wc_attribute_taxonomy_name($taxonomy) ) ) { 
				$taxonomy = 'pa_'.$taxonomy;
				$exists_in_db = $wpdb->get_results( "SELECT attribute_id FROM " . $wpdb->prefix . "woocommerce_attribute_taxonomies WHERE attribute_name = '" . $taxonomy . "';" );
				if ( ! $exists_in_db ) {
					$wpdb->insert( $wpdb->prefix . "woocommerce_attribute_taxonomies", array( 'attribute_name' => $taxonomy, 'attribute_type' => 'select', 'attribute_label' => $label ), array( '%s', '%s', '%s' ) );

				}
				register_taxonomy( $taxonomy,
					array( 'product', 'product_attributes' ),
					array(
						'hierarchical' => false,
						'show_ui'      => false,
						'query_var'    => is_admin(),
						'rewrite'      => false,
					)
				);
				//wp_set_object_terms ($post_id, 'variable', 'product_type');
				wp_set_object_terms( $post_id, $attribute, $taxonomy);
				$product_attributes= array($taxonomy=>array(
					'name'=>$taxonomy,
					'value'=>'',
					'is_visible' => '1', 
					'is_variation' => '1',
					'is_taxonomy' => '1'
				));
			}else{ 
				//wp_set_object_terms ($post_id, 'variable', 'product_type');
				$taxonomy = 'pa_'.$taxonomy;
				$pos = strpos($attribute, ',');
				if($pos !== false) {
					$attribute = explode(',', $attribute);
				}
				wp_set_object_terms( $post_id, $attribute, $taxonomy);
				$product_attributes= array($taxonomy=>array(
					'name'=>$taxonomy,
					'value'=>'',
					'is_visible' => '1', 
					'is_variation' => '1',
					'is_taxonomy' => '1'
				));
			}
			$getMetaKey = get_post_meta($post_id, '_product_attributes', true);
			if (!empty($getMetaKey))
				update_post_meta( $post_id, '_product_attributes', array_merge($product_attributes, $getMetaKey));
			else
				update_post_meta($post_id, '_product_attributes', $product_attributes);
			
		}
		function add_terms($category, $post_id,$taxonomy){
			$pos = strpos($category, 'and');
			$pos1 = strpos($category, '&');
			$pos2 = strpos($category, ',');
			if($pos !== false && $taxonomy == 'product_cat') {
				$array = explode('and', $category);
			}elseif($pos1 !== false && $taxonomy == 'product_cat') {
				$array = explode('&', $category);
			}elseif($pos2 !== false) {
				$array = explode(',', $category);
			}else{
				$array = $category;
			}
			$term_arr = array();
			
			if(is_array($array)){ 
				foreach($array as $arr){
					if(trim($arr) != ''){
						
						$cat_name = $arr;
						$cat_slug = trim(strtolower($arr));
						$cat_slug = str_replace(' ','-',$cat_slug);
						if(term_exists( $cat_slug, $taxonomy )){
							$term_id = get_term_by( 'slug', trim($cat_slug), $taxonomy );
							$term_arr[] = $term_id->term_id;
						}else{
							$term = wp_insert_term(
							  trim($cat_name), 
							  $taxonomy,
							  array(
								'description'=> '',
								'slug' => trim($cat_slug),
							  )
							);
							$term_arr[] = $term['term_id'];
						}
					}
				}
			}else{
				$cat_name = $category;
				$cat_slug = trim(strtolower($category));
				$cat_slug = str_replace(' ','-',$cat_slug);
				if(term_exists( $cat_slug, $taxonomy )){
							$term_id = get_term_by( 'slug', trim($cat_slug), $taxonomy );
							$term_arr[] = $term_id->term_id;
						}else{
							$term = wp_insert_term(
							  trim($cat_name),
							  $taxonomy,
							  array(
								'description'=> '',
								'slug' => trim($cat_slug),
							  )
							);
							$term_arr[] = $term['term_id'];
						}
			}
			/*	
				$term_arr = array();
				$cat_name = $category;
				$sub_cat_name = $sub_category;
				$cat_slug = trim(strtolower($category));
				$sub_cat_slug = trim(strtolower($sub_category));
				$cat_slug = str_replace(' ','-',$cat_slug);
				$sub_cat_slug = str_replace(' ','-',$sub_cat_slug);
				if(term_exists( $cat_slug, $taxonomy )){
					$term_id = get_term_by( 'slug', trim($cat_slug), $taxonomy );
					$term_arr[] = $term_id->term_id;
				}else{
					$term = wp_insert_term(
					  trim($cat_name),
					  $taxonomy,
					  array(
						'description'=> '',
						'slug' => trim($cat_slug),
					  )
					);
					if(!isset($sub_category) && $sub_category == ''){
						$term_arr[] = $term['term_id'];
					}
				}
				if(isset($sub_category) && $sub_category != ''){
					if(term_exists( $sub_cat_slug, $taxonomy )){
						$sub_term_id = get_term_by( 'slug', trim($sub_cat_slug), $taxonomy );
						$term_arr[] = $sub_term_id->term_id;
					}else{
						$sub_term = wp_insert_term(
						  trim($sub_cat_name),
						  $taxonomy,
						  array(
							'description'=> '',
							'slug' => trim($sub_cat_slug),
							'parent' => $term_arr[0]
						  )
						);
						$term_arr[] = $sub_term['term_id'];
					}
				}
				* * */
			wp_set_object_terms( $post_id, $term_arr, $taxonomy, true );
			
		}
		$namedDataArray = array();
		$i =0;
		for($row=1; $row<= count($data['sheet_0']); $row++){
			echo $data['sheet_0'][$row][2].'<br>';
			/*
			$r5 = $wpdb->get_results('select id from area_search_5 where name="'.$data['sheet_0'][$row][4].'"');
			$r4 = $wpdb->get_results('select id from area_search_4 where name="'.$data['sheet_0'][$row][3].'"');
			$r3 = $wpdb->get_results('select id from area_search_3 where name="'.$data['sheet_0'][$row][2].'"');
			$r2 = $wpdb->get_results('select id from area_search_2 where name="'.$data['sheet_0'][$row][1].'"');
			$r1 = $wpdb->get_results('select id from area_search_1 where name="'.$data['sheet_0'][$row][0].'"');
			*/
			$pos = strpos($data['sheet_0'][$row][2], ',');
			if($pos !== false) {
				$array = explode(',', $data['sheet_0'][$row][2]);
				foreach($array as $arr){
					$r1 = $wpdb->get_results('select id from soil_search_1 where name="'.$data['sheet_0'][$row][0].'"');
					$pos = strpos($data['sheet_0'][$row][1], ',');
					if($pos !== false) {
						$arra = explode(',',$data['sheet_0'][$row][1]);
						foreach($arra as $ar){
							$r3 = $wpdb->get_results('select id from soil_search_2 where name="'.$ar.'"');
							$wpdb->insert( 
								'soil_search_3', 
								array( 
									'name' => $arr,
									'1_id' => $r1[0]->id,
									'2_id' => $r3[0]->id,
								)
							);
						}
					}else{
						$r3 = $wpdb->get_results('select id from soil_search_2 where name="'.$data['sheet_0'][$row][1].'"');
						$wpdb->insert( 
							'soil_search_3', 
							array( 
								'name' => $arr,
								'1_id' => $r1[0]->id,
								'2_id' => $r3[0]->id,
							)
						);
					}
				}
			}else{
				$r1 = $wpdb->get_results('select id from soil_search_1 where name="'.$data['sheet_0'][$row][0].'"');
				$pos = strpos($data['sheet_0'][$row][1], ',');
				if($pos !== false) {
					$array = explode(',',$data['sheet_0'][$row][1]);
					foreach($array as $ar){
						$r3 = $wpdb->get_results('select id from soil_search_2 where name="'.$ar.'"');
						$wpdb->insert( 
							'soil_search_3', 
							array( 
								'name' => $data['sheet_0'][$row][2],
								'1_id' => $r1[0]->id,
								'2_id' => $r3[0]->id,
							)
						);
					}
				}else{
					$r3 = $wpdb->get_results('select id from soil_search_2 where name="'.$data['sheet_0'][$row][1].'"');
					$wpdb->insert( 
						'soil_search_3', 
						array( 
							'name' => $data['sheet_0'][$row][2],
							'1_id' => $r1[0]->id,
							'2_id' => $r3[0]->id,
						)
					);
				}
			}
		}
		die;
		for($row=1; $row<= count($data['sheet_0']); $row++){
			foreach($headingsArray as $columnKey => $columnHeading) {
				if($columnHeading == 'Product' && $data['sheet_0'][$row][$columnKey] == ''){
					$empty = 'yes';
					break;
				}else{
					$empty = 'no';
				}
				$namedDataArray[$i][trim($columnHeading)] = $data['sheet_0'][$row][$columnKey];
			}
			if($empty == 'yes'){
				unset($namedDataArray[$i]);
				$i--;
				break;
				
			}else{
				$postdata = array(
					'post_author'    => 1,
					'post_content'   => $namedDataArray[$i]['Long Description'],
					'post_title'     => $namedDataArray[$i]['Product'],
					'post_name'      => $namedDataArray[$i]['Product'],
					'post_status'    => 'publish',
					'post_type'      => 'product',
				);
				//$sku = trim(strtolower($namedDataArray[$i]['Product']));
				//$sku = str_replace(' ','_',$sku);
				$sku = $namedDataArray[$i]['Product Codes'];
				$pos15 = strpos($namedDataArray[$i]['Product'], ',');
				if($pos15 !== false) {
					$products = explode(',',$namedDataArray[$i]['Product']);
				foreach($products as $product){
				$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM $wpdb->posts WHERE post_title='%s' and post_type='product' and post_status='publish'  LIMIT 1",ucwords(strtolower(trim($product))) ) );
				if(!isset($post_id) && $post_id == ''){ 
					/*
					$post_id = wp_insert_post( $postdata, true ); 
					update_post_meta($post_id,'_sku',$sku);
					update_post_meta($post_id,'_downloadable','no');
					update_post_meta($post_id,'_virtual','no');
					update_post_meta($post_id,'_regular_price',10);
					update_post_meta($post_id,'_price',10);
					update_post_meta($post_id,'_stock_status','instock');
					update_post_meta($post_id,'_visibility','visible');
					add_terms($namedDataArray[$i]['Category'], $post_id,'product_cat');
					add_terms($namedDataArray[$i]['Soilage'], $post_id,'soilage');
					add_terms($namedDataArray[$i]['Surface'], $post_id,'surface');
					add_attribute($namedDataArray[$i]['pH Level'], $post_id,'ph-level','pH Level');
					if($namedDataArray[$i]['Perfume'] != '' && $namedDataArray[$i]['Perfume'] != 'None')
						add_attribute($namedDataArray[$i]['Perfume'], $post_id,'perfume','Perfume');
					add_attribute($namedDataArray[$i]['Sizes Available'], $post_id,'sizes','Sizes');
					* */
				}else{   
					//update_post_meta($post_id,'_downloadable','no');
					//update_post_meta($post_id,'_virtual','no');
					//update_post_meta($post_id,'_regular_price',10);
					//update_post_meta($post_id,'_price',10);
					//update_post_meta($post_id,'_stock_status','instock');
					//update_post_meta($post_id,'_sku',$sku);
					$my_post = array(
						  'ID'           => $post_id,
						  'post_content' => $namedDataArray[$i]['Description']
					  );
					  //wp_update_post( $my_post );
					 // update_post_meta($post_id,'_sku',$sku);
					//update_post_meta($post_id,'_visibility','visible');
					//add_attribute($namedDataArray[$i]['pH Level'], $post_id,'ph-level','pH Level');
					//add_attribute($namedDataArray[$i]['Product Codes'], $post_id,'product_code','Code');
					//if($namedDataArray[$i]['Perfume'] != '' && $namedDataArray[$i]['Perfume'] != 'None')
						//add_attribute($namedDataArray[$i]['Perfume'], $post_id,'perfume','Perfume');
					//add_attribute($namedDataArray[$i]['Sizes Available'], $post_id,'sizes','Sizes');
					//add_terms($namedDataArray[$i]['Product Group'],$namedDataArray[$i]['Sub Product Group'], $post_id,'product_cat');
					add_terms(ucwords(strtolower($namedDataArray[$i]['Soilage'])), $post_id,'soilage');
					add_terms(ucwords(strtolower($namedDataArray[$i]['Surface'])), $post_id,'surface');
				}
			}
				}else{
					$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM $wpdb->posts WHERE post_title='%s'  LIMIT 1",$namedDataArray[$i]['Product'] ) );
				if(!isset($post_id) && $post_id == ''){ 
					
				}else{   
					
					//add_terms($namedDataArray[$i]['Product Group'],$namedDataArray[$i]['Sub Product Group'], $post_id,'product_cat');
					//add_terms($namedDataArray[$i]['Soilage'], $post_id,'soilage');
					//add_terms($namedDataArray[$i]['Surface'], $post_id,'surface');
				}
				
			}
			$i++;
		}
	} }
	?>
	
	<form class="ImportJobs" name="importjobs" id="importjobs" method="POST" enctype="multipart/form-data">
		<?php if((isset($error)) && (($error!=""))){ echo $error; }	?>
		<input class="uploadfile required" type="file" name="file" id="file">
		<input type="submit" name="submit" value="Import">
	</form>
	<?php 
}
