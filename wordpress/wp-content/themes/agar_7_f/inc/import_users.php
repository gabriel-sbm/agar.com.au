<?php 
$varDirectoryPath = $_SERVER["DOCUMENT_ROOT"].'/agarlive/';
include_once($varDirectoryPath.'wp-load.php');
include_once($varDirectoryPath.'wp-config.php');
include_once($varDirectoryPath.'wp-includes/wp-db.php');
global $wpdb;
$file = fopen("users.csv","r");
echo '<pre>';
while(! feof($file)){
	$a = fgetcsv($file);
	if($a[0] > 1){
		$username= strtolower($a[6]).'_'.strtolower($a[7]);
		$id = username_exists( $user_name );
		$tel = '';
		if ( !$id and email_exists($a[1]) == false ) {
			$args = array(
					'user_login' => $username,
					'user_pass' => $a[4],
					'user_email' => $a[1],
					'role' => 'customer',
					'first_name'=> $a[6],
					'last_name'=> $a[7],
					'user_registered'=> date('Y-m-d H:i:s',strtotime(str_replace('/','-',$a[12]))),
					'description'=> $a[5],
					'status'=>$a[3]
			);
			$user_id = wp_insert_user($args);
			if($user_id){
				$tel = $a[17];
				if($tel != '' && $a[18] != ''){
					$tel .= ','.$a[18];
				}elseif($tel == ''){
					$tel = $a[18];
				}
				update_user_meta( $user_id, 'billing_first_name', $a[6]);
				update_user_meta( $user_id, 'billing_last_name', $a[7]);
				update_user_meta( $user_id, 'billing_company', $a[9]);
				update_user_meta( $user_id, 'billing_address_1', $a[11]);
				update_user_meta( $user_id, 'billing_address_2', $a[13]);
				update_user_meta( $user_id, 'billing_city', $a[10]);
				update_user_meta( $user_id, 'billing_state', $a[14]);
				update_user_meta( $user_id, 'billing_postcode', $a[15]);
				update_user_meta( $user_id, 'billing_country', $a[16]);
				update_user_meta( $user_id, 'billing_phone', $tel);
				update_user_meta( $user_id, 'billing_email', $a[1]);
				update_user_meta( $user_id, 'shipping_first_name', $a[6]);
				update_user_meta( $user_id, 'shipping_last_name', $a[7]);
				update_user_meta( $user_id, 'shipping_company', $a[9]);
				update_user_meta( $user_id, 'shipping_address_1', $a[11]);
				update_user_meta( $user_id, 'shipping_address_2', $a[13]);
				update_user_meta( $user_id, 'shipping_city', $a[10]);
				update_user_meta( $user_id, 'shipping_state', $a[14]);
				update_user_meta( $user_id, 'shipping_postcode', $a[15]);
				update_user_meta( $user_id, 'shipping_country', $a[16]);
				update_user_meta( $user_id, 'found_us_on', $a[19]);
			}
		}else{
			echo $a[0].'<br />';
		}
	}

}
fclose($file);
