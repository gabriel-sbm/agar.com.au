<?php 
add_action( 'admin_menu', 'customer_export_submenu_page' );

function customer_export_submenu_page() {

	add_submenu_page( 'users.php', 'Customers Export', 'Customers Export', 'manage_options', 'customer_export', 'customer_export_submenu_page_callback' );

}

function customer_export_submenu_page_callback(){ ?>
	<script>
		jQuery(document).ready(function($){
			$('.Datepicker').datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd/mm/yy"
			});
		});
	
		function redirect_to(url,rpp){
			window.location.href=url+"&rpp="+rpp;
		}
	</script>
	<div class="wrap"><div id="icon-tools" class="icon32"></div>
		<h2>Customers Export</h2>
	</div>
	<?php global $wpdb;  
	if(isset($_REQUEST["rpp"])){
		$perpage = ($_REQUEST["rpp"]); 
	}else{ 
		$perpage = 100;	 
	}
	if(isset($_REQUEST["p"])){
		$page = ($_REQUEST["p"]); 
	}else{ 
		$page = 1;	 
	}
	if($_REQUEST['search_users']){
		$url = admin_url().'users.php?page=customer_export&startDate='.$_REQUEST['start_date'].'&endDate='.$_REQUEST['end_date']; 
		echo "<script>window.location.href='".$url."'</script>";
	}
	$url = admin_url().'users.php?page=user_search';
	$start=($page-1)*$perpage;
	if($_GET['startDate'] != '' && $_REQUEST['endDate'] != ''){
		$startDate = date('Y-m-d h:i:s',strtotime($_GET['startDate']));
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$users = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered BETWEEN '".$startDate."' and '".$endDate."' order by u1.user_registered DESC Limit $start, $perpage");
	}elseif($_GET['startDate'] != '' && $_REQUEST['endDate'] == ''){
		$startDate = date('Y-m-d h:i:s',strtotime($_GET['startDate']));
		$users = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered >= '".$startDate."' order by u1.user_registered DESC Limit $start, $perpage");
	}elseif($_GET['startDate'] == '' && $_REQUEST['endDate'] != ''){
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$users = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered <= '".$endDate."' order by u1.user_registered DESC Limit $start, $perpage");
	}elseif(strtotime($_GET['startDate']) > strtotime($_REQUEST['endDate'])){
		$users = array();
	}elseif(($_GET['startDate'] == $_REQUEST['endDate']) && $_GET['startDate'] != ''){
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$users = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered = '".$endDate."' order by u1.user_registered DESC Limit $start, $perpage");
	}else{
		$users = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') order by u1.user_registered DESC Limit $start, $perpage");
	}
	if(($_GET['startDate'] != '' && $_REQUEST['endDate'] != '') && (strtotime($_GET['startDate']) < strtotime($_REQUEST['endDate']))){
		$startDate = date('Y-m-d h:i:s',strtotime($_GET['startDate']));
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$Tusers = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered BETWEEN '".$startDate."' and '".$endDate."' order by u1.user_registered DESC");
	}elseif($_GET['startDate'] != '' && $_REQUEST['endDate'] == ''){
		$startDate = date('Y-m-d h:i:s',strtotime($_GET['startDate']));
		$Tusers = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered >= '".$startDate."' order by u1.user_registered DESC");
	}elseif($_GET['startDate'] == '' && $_REQUEST['endDate'] != ''){
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$Tusers = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered <= '".$endDate."' order by u1.user_registered DESC");
	}elseif(strtotime($_GET['startDate']) > strtotime($_REQUEST['endDate'])){
		$Tusers = array();
	}elseif(($_GET['startDate'] == $_REQUEST['endDate']) && $_GET['startDate'] != ''){
		$endDate = date('Y-m-d h:i:s',strtotime($_GET['endDate']));
		$Tusers = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') where u1.user_registered = '".$endDate."' order by u1.user_registered DESC");
	}else{
		$Tusers = $wpdb->get_results("SELECT u1.ID as id, u1.user_email as user_email, u1.user_registered as user_registered, m1.meta_value as first_name, m1.meta_value as last_name from ".$wpdb->users." as u1 JOIN ".$wpdb->usermeta." as m1 ON (m1.user_id = u1.id AND m1.meta_key = 'first_name') JOIN ".$wpdb->usermeta." as m2 ON (m2.user_id = u1.id AND m2.meta_key = 'last_name') order by u1.user_registered DESC");
	} ?>
	<script>
		/*jQuery(document).ready(function($){
			$('input[name="export_users"]').click(function(){
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				var data = {
					'action': 'export_users',
					'data': '<?php echo json_encode($users); ?>'
				};
				jQuery.post(ajaxurl, data, function(response) {
					
				},'json');
			});
		});*/
	</script>
	<div style="display: inline-block; float: left; width: 25%;">
		<form action="<?php echo get_bloginfo('template_url').'/inc/generate_user_csv.php';?>" method="POST">
			<input type="hidden" name="data" value="<?php echo base64_encode(serialize($Tusers)); ?>">
			<input type="submit" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer; width:130px;" value="Export Users" name="export_users">
		</form>
	</div>
	<form method="post" action="" id="soilageProducts">
		<span style="display: inline-block; float: none; width: 54%;">
			<label>Search:</label>
			<input type="text" name="start_date" class="Datepicker" value="" placeholder="From Date:">
			<input type="text" name="end_date" class="Datepicker" value="" placeholder="To Date:">
			<input type="submit" name="search_users" value="Search" style="background-color: #bcbcbc; border: 1px solid #202020; padding: 3px 20px; cursor:pointer;" />
		</span>
		<?php $rpp_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
		if($_GET['rpp'] != ''){
			$parse_url = parse_url($rpp_url);
			$query = explode('&',$parse_url['query']);
			unset($query[count($query)-1]);
			if(count($query) >1){
				$query = implode('&',$query);
			}else{
				$query = $query[0];
			}
			$rpp_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
		} ?>
		<span style="float:right;margin-right:40px;">Record per Page  
			<select name="Records" onchange="redirect_to('<?php echo $rpp_url; ?>',this.value)">
				<?php for($i=1; $i<=8; $i++) {  ?>
					<option value="<?php echo $i*100?>" <?php if($perpage==$i*100){echo 'selected';} ?>><?php echo $i*100 ?></option>
				<?php } ?>
			</select>
		</span>
		<?php if(($_GET['startDate'] != '' && $_REQUEST['endDate'] != '') && (strtotime($_GET['startDate']) < strtotime($_REQUEST['endDate']))){
			echo '<h2>Total Users registered between '.$_GET['startDate'].' and '.$_REQUEST['endDate'].': <b>'.count($Tusers).'</b></h2>';
		}elseif($_GET['startDate'] != '' && $_REQUEST['endDate'] == ''){
			echo '<h2>Total Users registered after '.$_GET['startDate'].': <b>'.count($Tusers).'</b></h2>';
		}elseif($_GET['startDate'] == '' && $_REQUEST['endDate'] != ''){
			echo '<h2>Total Users registered before '.$_REQUEST['endDate'].': <b>'.count($Tusers).'</b></h2>';
		}elseif(strtotime($_GET['startDate']) > strtotime($_REQUEST['endDate'])){
			echo '<h2>Start Date is greater then End Date.</h2>';
		}elseif(($_GET['startDate'] == $_REQUEST['endDate']) && $_GET['startDate'] != ''){
			echo '<h2>Total Users registered on '.$_REQUEST['endDate'].': <b>'.count($Tusers).'</b></h2>';
		}else{
			echo '<h2>Total Users: <b>'.count($Tusers).'</b></h2>';
		}
		$current_url = admin_url().'admin.php?'.$_SERVER['QUERY_STRING'];
		if($_GET['order'] != '' && $_GET['orderby'] != ''){
			$parse_url = parse_url($current_url);
			$query = explode('&',$parse_url['query']);
			unset($query[count($query)-1]);
			unset($query[count($query)-1]);
			if(count($query) >1){
				$query = implode('&',$query);
			}else{
				$query = $query[0];
			}
			$current_url = $parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'?'.$query;
		}
		if(!empty($users)){ 
			if($_GET['order'] == '' || $_GET['order'] == 'desc'){
				$class = 'desc';
			}elseif($_GET['order'] == 'asc'){
				$class = 'asc';
			}
			if($_GET['order'] == '' || $_GET['order'] == 'asc'){
				$Dclass = 'asc';
			}elseif($_GET['order'] == 'desc'){
				$Dclass = 'desc';
			} ?>
			<table style="width:100%; background-color:#fff; overflow-x: hidden; overflow-y: auto; display: inline-block; height: 600px;" class="main-table-bottom" cellspacing="1" cellpadding="10" bgcolor="#fff" >
				<tr>
					<th width="10%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
						<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
							<a href="<?php echo $current_url.'&orderby=ID&order=asc' ?>">
						<?php }elseif($_GET['order'] == 'asc'){ ?>
							<a href="<?php echo $current_url.'&orderby=ID&order=desc' ?>">
						<?php } ?>
							<span style="display:inline-block; float:left;">ID</span>
							<span class="sorting-indicator"></span>
						</a>
					<th width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
						<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
							<a href="<?php echo $current_url.'&orderby=first_name&order=asc' ?>">
						<?php }elseif($_GET['order'] == 'asc'){ ?>
							<a href="<?php echo $current_url.'&orderby=first_name&order=desc' ?>">
						<?php } ?>
							<span style="display:inline-block; float:left;">First Name</span>
							<span class="sorting-indicator"></span>
						</a>
					</th>
					<th width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
						<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
							<a href="<?php echo $current_url.'&orderby=last_name&order=asc' ?>">
						<?php }elseif($_GET['order'] == 'asc'){ ?>
							<a href="<?php echo $current_url.'&orderby=last_name&order=desc' ?>">
						<?php } ?>
							<span style="display:inline-block; float:left;">Last Name</span>
							<span class="sorting-indicator"></span>
						</a>
					</th>
					<th width="30%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $class; ?>">
						<?php if($_GET['order'] == '' || $_GET['order'] == 'desc'){ ?>
							<a href="<?php echo $current_url.'&orderby=email&order=asc' ?>">
						<?php }elseif($_GET['order'] == 'asc'){ ?>
							<a href="<?php echo $current_url.'&orderby=email&order=desc' ?>">
						<?php } ?>
							<span style="display:inline-block; float:left;">Email</span>
							<span class="sorting-indicator"></span>
						</a>
					</th>
					<th width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0" class="<?php echo $Dclass; ?>">
						<?php if($_GET['order'] == '' || $_GET['order'] == 'asc'){ ?>
							<a href="<?php echo $current_url.'&orderby=user_registered&order=desc' ?>">
						<?php }elseif($_GET['order'] == 'desc'){ ?>
							<a href="<?php echo $current_url.'&orderby=user_registered&order=asc' ?>">
						<?php } ?>
							<span style="display:inline-block; float:left;">Registered On</span>
							<span class="sorting-indicator"></span>
						</a>
					</th>
				</tr>
				<?php $k = 0; foreach($users as $user) {  
						$registerdate = $user->user_registered;
						$registerdate = date("d/m/Y", strtotime($registerdate));
						$pos = strpos($registerdate, '1970');
						if ($pos === false) {
							$date =  $registerdate;
						}else{
							$date = 'N/A';
						}
						?>
					<tr>
						<td width="10%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $user->id; ?></td>
						<td width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $user->first_name; ?></td>
						<td width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $user->last_name; ?></td>
						<td width="30%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $user->user_email; ?></td>
						<td width="20%" style="text-align:left; border-bottom:1px solid #f0f0f0; border-right:1px solid #f0f0f0"><?php echo $date; ?></td>
					</tr>
				<?php $k++; } ?>
			</table>
		<?php }else{ 
			echo '<h4>No records found!</h4>';
		} ?>
	</form>
	<?php //$users = $wpdb->get_results("SELECT * from ".$wpdb->users);
	
	$total = count($Tusers);
	if($total > 0){
		$totalPages = ceil($total / $perpage);
		if($totalPages > 1){
			if($page <=1 ){
				echo "<span id='page_prev' style='font-weight:bold;'>Prev</span>";
			}else{
				$j = $page - 1;
				echo "<span><a id='page_prev' href='".$rpp_url."&p=$j&rpp=".$perpage."'> Prev</a>&nbsp</span>";
			}
			for($i=1; $i <= $totalPages; $i++){
				if($i<>$page) {
					echo "<span><a href='".$rpp_url."&p=$i&rpp=".$perpage."' id='page_a_link'> $i</a>&nbsp</span>";
				}else{
					echo "<span id='page_links' style='font-weight:bold;'>&nbsp$i</span>";
				}
			}
			if($page == $totalPages ){
				echo "<span id='page_next' style='font-weight:bold;'>&nbsp Next ></span>";
			}else{
				$j = $page + 1;
				echo "<span><a href='".$rpp_url."&p=$j&rpp=".$perpage."' id='page_next'> Next</a>&nbsp</span>";
			}
		}
	} ?>
	<style>
		.sorting-indicator{ float: left; }
	</style>
<?php }
function users_search_scripts() {
	wp_register_style( 'jquery-ui-css', get_bloginfo('template_url').'/css/jquery-ui.css', false, '1.0.0');
	wp_enqueue_style( 'jquery-ui-css' );
	wp_register_script( 'jquery-ui-js', get_bloginfo('template_url').'/js/jquery-ui.min.js', array( 'jquery' ));
	wp_enqueue_script( 'jquery-ui-js' );
}

add_action( 'admin_enqueue_scripts', 'users_search_scripts' );


function registerdate($columns) {
	$columns['subscribed'] = __('Subscribed', 'subscribed');
    $columns['registerdate'] = __('Registered', 'registerdate');
    return $columns;
}
add_filter('manage_users_columns', 'registerdate');
 
// Display the column content
function registerdate_columns( $value, $column_name, $user_id ) {
	if ( $column_name == 'registerdate' ){
		$user = get_userdata( $user_id );
        $registerdate = $user->user_registered;
        $registerdate = date("Y-m-d", strtotime($registerdate));
        $pos = strpos($registerdate, '1970');
        if ($pos === false) {
			return $registerdate;
		}else{
			return 'N/A';
		}
	}elseif($column_name == 'subscribed'){
		if(get_user_meta($user_id,'subscribed',true == 'yes')){
			return 'Yes';
		}else{
			return 'No';
		}
	}
}
add_action('manage_users_custom_column',  'registerdate_columns', 10, 3);
 
function registerdate_column_sortable($columns) {
          $custom = array(
      // meta column id => sortby value used in query
          'registerdate'    => 'registered',
          );
      return wp_parse_args($custom, $columns);
}
 
add_filter( 'manage_users_sortable_columns', 'registerdate_column_sortable' );
 
function registerdate_column_orderby( $vars ) {
        if ( isset( $vars['orderby'] ) && 'registerdate' == $vars['orderby'] ) {
                $vars = array_merge( $vars, array(
                        'meta_key' => 'registerdate',
                        'orderby' => 'meta_value'
                ) );
        }
 
        return $vars;
}
 
add_filter( 'request', 'registerdate_column_orderby' );

//Export Users
add_action( 'wp_ajax_export_users', 'export_users_callback' );
add_action( 'wp_ajax_nopriv_export_users', 'export_users_callback' );

function export_users_callback() {
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=file.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	$file = fopen('php://output', 'w');
	$main_headers = array('ID','First Name', 'Last Name', 'Email', 'Registered On');
	$users = json_decode(stripslashes($_POST['data']));
	$data = array();
	foreach($users as $user){
		$user_info = get_userdata($user->ID);
		$registerdate = date("d/m/Y", strtotime($user->user_registered));
		$pos = strpos($registerdate, '1970');
		if ($pos === false) {
			$date =  $registerdate;
		}else{
			$date = 'N/A';
		}
		$data[] = array($user->ID,$user_info->first_name,$user_info->last_name,$user->user_email,$date);
	}      
	fputcsv($file, $main_headers);      
	foreach($data as $row) {
		fputcsv($file, $row);
	}
	die();
}
