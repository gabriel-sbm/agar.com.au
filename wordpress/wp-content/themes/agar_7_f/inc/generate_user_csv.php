<?php
$varDirectoryPath = $_SERVER["DOCUMENT_ROOT"];
include_once($varDirectoryPath.'/wp-load.php');
include_once($varDirectoryPath.'/wp-includes/wp-db.php');
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
header("Pragma: no-cache");
header("Expires: 0");
$file = fopen('php://output', 'w');
$main_headers = array('ID','First Name', 'Last Name', 'Email', 'Registered On');
$users = unserialize(stripslashes(base64_decode($_POST['data'])));
$data = array();
foreach($users as $user){
	$user_info = get_userdata($user->ID);
	$registerdate = date("d/m/Y", strtotime($user->user_registered));
	$pos = strpos($registerdate, '1970');
	if ($pos === false) {
		$date =  $registerdate;
	}else{
		$date = 'N/A';
	}
	$data[] = array($user->ID,$user_info->first_name,$user_info->last_name,$user->user_email,$date);
}      
fputcsv($file, $main_headers);      
foreach($data as $row) {
	fputcsv($file, $row);
}
