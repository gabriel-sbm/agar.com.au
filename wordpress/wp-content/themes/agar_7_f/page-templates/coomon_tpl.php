<?php 
/**

Template Name: Common Template

**/
?>
<?php get_header(); ?>

<div class="innerFull AboutNewTemp">
<?php 
global $post;
if ( have_posts() ) : while( have_posts() ) : the_post();
	if ( has_post_thumbnail() ) {
		
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		
	 ?>
    
   <?php /*?> <div class="backgroundImg" style="background-image:url(<?php echo $large_image_url[0] ?>);">
<img src="<?php echo $large_image_url[0] ?>" alt="" />
</div><?php */?>
<?php } ?>
 <div class="full_row">
 <div class="textwidget">
 
<?php if($post->post_name == 'member-area'){
			if ( is_user_logged_in() ) {
				the_content(); 
			}else{
				echo "<p class='logregister'>You have to be logged in to access this page. Click <a href='".get_bloginfo('url')."/my-account'>Login/Register</a>.</p>";
			}
		}else{
			the_content(); 
		}
		?>
</div>
</div>

<?php endwhile;  wp_reset_query();   endif; ?>

</div>
<?php get_footer(); ?>




<?php /*?><ul class="catListing" >
      <?php
			query_posts('cat=19');
			while (have_posts()) : the_post();  ?>
              <li>
              <a href="<?php echo get_permalink(); ?>"> 
              <div class="catimg">  <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail("full");
                } ?>
              </div>
			<h3><?php echo the_title(); ?></h3>
            </a>
           
			<?php the_excerpt(); ?>

				  </li>
      <?php endwhile;  wp_reset_query();
?>
    </ul><?php */?>
	
    
    
<?php /*?><ul class="blogPosts">    
<?php
$args = array( 'posts_per_page' => 20 );
$lastposts = get_posts( $args );
foreach ( $lastposts as $post ) :
  setup_postdata( $post ); ?>
  <li>
  <a href="<?php the_permalink(); ?>"><h2><<?php the_title(); ?></h2></a>
<div class="blogImg">	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail("full");
	} ?>
    </div>
	<?php the_content(); ?>
    </li>
<?php endforeach; 
wp_reset_postdata(); ?>
</ul><?php */?>
