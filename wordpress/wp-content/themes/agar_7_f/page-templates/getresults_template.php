<?php 
/**

Template Name: Product Results Template

**/
get_header(); ?>
<div class="innerFull">
	<div class="full_row">
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<h1 class="page-title"><?php the_title(); ?></h1>
			<?php if($_REQUEST['cat2'] != '' || $_REQUEST['cat2'] != ''){
			$soilage = get_term($_REQUEST['cat2'], 'soilage');
			$surface = get_term($_REQUEST['cat1'], 'surface');  ?>
			<p class="searched">You searched for soil: <?php echo strtolower($soilage->name); ?> surface: <?php echo strtolower($surface->name); ?></p>
			<?php } ?>
	<?php endif;

	$where = '';

	// Soil search logic
	if ($_REQUEST['cat_1'] != '' || $_REQUEST['cat_1'] != '') {
		//$products = get_posts($args); 
		$products = array();
		if($_REQUEST['cat_1'] != 'select' && $_REQUEST['cat_1'] != '' && $_REQUEST['cat_1'] != 0){
		//if($_REQUEST['cat_1'] != ''){
			$where = '1_id = '.$_REQUEST['cat_1'];
			$result = $wpdb->get_results('select * from soil_search_1 where id = '.$_REQUEST['cat_1']);
			$name = strtolower($result[0]->name);
		}
		if($_REQUEST['cat_2'] != 'select' && $_REQUEST['cat_2'] != '' && $_REQUEST['cat_2'] != 0){
		//if($_REQUEST['cat_2'] != ''){
			$where .= ' and 2_id = '.$_REQUEST['cat_2'];
			$result = $wpdb->get_results('select * from soil_search_2 where id = '.$_REQUEST['cat_2']);
			$name .= ' surface: '.strtolower($result[0]->name);
		}
		if($_REQUEST['cat_3'] != 'select' && $_REQUEST['cat_3'] != '' && $_REQUEST['cat_3'] != 0){
		//if($_REQUEST['cat_3'] != ''){
			$where .= ' and 3_id = '.$_REQUEST['cat_3'];
			$result = $wpdb->get_results('select * from soil_search_3 where id = '.$_REQUEST['cat_3']);
			$name .= ' sub-category: '.strtolower($result[0]->name);
		}
		$products = $wpdb->get_results('select * from soil_search_4 where '.$where);

	}

// Area search logic
	else { 
		$products = array();
		if($_REQUEST['search_1'] != 'select' && $_REQUEST['search_1'] != '' && $_REQUEST['search_1'] != 0){
		//if($_REQUEST['search_1'] != ''){
			$where .= '1_id = '.$_REQUEST['search_1'];
			$result = $wpdb->get_results('select * from area_search_1 where id = '.$_REQUEST['search_1']);
			$name = $result[0]->name;
		}
		if($_REQUEST['search_2'] != 'select' && $_REQUEST['search_2'] != '' && $_REQUEST['search_2'] != 0){
		//if($_REQUEST['search_2'] != ''){
			$where .= ' and 2_id = '.$_REQUEST['search_2'];
			$result = $wpdb->get_results('select * from area_search_2 where id = '.$_REQUEST['search_2']);
			$name .= '->'.$result[0]->name;
		}
		if($_REQUEST['search_3'] != 'select' && $_REQUEST['search_3'] != '' && $_REQUEST['search_3'] != 0){
		//if($_REQUEST['search_3'] != ''){
			$where .= ' and 3_id = '.$_REQUEST['search_3'];
			$result = $wpdb->get_results('select * from area_search_3 where id = '.$_REQUEST['search_3']);
			$name .= '->'.$result[0]->name;
		}
		if($_REQUEST['search_4'] != 'select' && $_REQUEST['search_4'] != '' && $_REQUEST['search_4'] != 0){
		//if($_REQUEST['search_4'] != ''){
			$where .= ' and 4_id = '.$_REQUEST['search_4'];
			$result = $wpdb->get_results('select * from area_search_4 where id = '.$_REQUEST['search_4']);
			$name .= '->'.$result[0]->name;
		}
		if($_REQUEST['search_5'] != 'select' && $_REQUEST['search_5'] != '' && $_REQUEST['search_5'] != 0){
		//if($_REQUEST['search_5'] != ''){
			$where .= ' and 5_id = '.$_REQUEST['search_5'];
			$result = $wpdb->get_results('select * from area_search_5 where id = '.$_REQUEST['search_5']);
			$name .= '->'.$result[0]->name;
		}
		$products = $wpdb->get_results('select * from area_search_6 where '.$where);
	}

	if($_REQUEST['cat_1'] == '' || $_REQUEST['cat_1'] == '' && $_REQUEST['search_1'] != ''){ ?>
		<p class="searched">You searched for <?php echo $name; ?></p>
	<?php } else { // soil search "you searched for" output ?> 
		<p class="searched">You searched for soil: <?php echo $name; ?></p>
	<?php } ?>
	<a class="back2start" href="<?php bloginfo('url'); ?>#psw">Back to Start</a>
	<ul class="products"> 
		<?php if(!empty($products)){
				 foreach($products as $product){ 
					$result = $wpdb->get_results('select ID from '.$wpdb->prefix.'posts where post_title = "'.trim($product->name).'" and post_type="product" and post_status="publish"');
					$product = array();
					if(isset($result[0]) && $result[0]->ID != ''){
						$product = get_post($result[0]->ID); ?>
					<li class="product_result">
						<a href="<?php echo get_permalink($product->ID); ?>">
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($product->ID), array(300,300));
						if($image[0]){  ?>
							<img class="woocommerce-placeholder wp-post-image" width="300" height="300" alt="Placeholder" src="<?php echo $image[0]; ?>">
						<?php }else{ ?>
							<img class="woocommerce-placeholder wp-post-image" width="150" height="150" alt="Placeholder" src="<?php bloginfo('url'); ?>/wp-content/plugins/woocommerce/assets/images/placeholder.png">
						<?php } ?>
						</a>
						<div class="product_result_content"> 
							<h4><?php echo get_the_title($product->ID); ?></h4>
							<?php $content =  wp_trim_words($product->post_content, $num_words = 40, $more = null);
							$pos = strpos($content, '?');
							if ($pos !== false) {
								$content = explode('?', $content);
								$content = $content[1];
							} ?>
							<p><?php echo $content; ?></p>
							<a href="<?php echo get_permalink($product->ID); ?>">Click here for more info</a>
						</div>
					</li>
				<?php }
			}
		 }else{ ?>
			<p class="no_products">No products found related to this search.</p>
		<?php } ?> 
	</ul>
	</div>
</div>
</div>
</div>
<?php get_footer();
