<?php 
/**

Template Name: Full Width Standard (was About Page New Template)

**/
?>
<?php get_header(); ?>

<div class="innerFull AboutNewTemp">
<?php if ( have_posts() ) : while( have_posts() ) : the_post();
	if ( has_post_thumbnail() ) {
		
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		
	 ?>
    
   <?php /*?> <div class="backgroundImg" style="background-image:url(<?php echo $large_image_url[0] ?>);">
<img src="<?php echo $large_image_url[0] ?>" alt="" />
</div><?php */?>
<?php } ?>
 <div class="full_row">
 <div class="textwidget">
<?php  if ( is_page('my-account') ){ ?>

<?php if ( is_user_logged_in() ) {?>

<?php } else { ?>
<h1><center>Sign Up</center></h1>
<center>Register your account with agar.com.au and receive access to our exclusive members area, loaded with helpful information on products and how to get the most out of them!</center>
<?php } ?>

<div class="sing"> 
<?php the_content(); ?>
</div>
<?php } else { ?> 
 
<?php the_content(); ?>

<?php } ?>

</div>
</div>

<?php endwhile;  wp_reset_query();   endif; ?>
<?php  if ( is_page('green-products') || is_page('online-training') || is_page('contact') || is_page('e-business-solutions') || is_page('comply-and-protect') || is_page('sign-up')|| is_page('why-choose-agar') || is_page('sds') || is_page('my-account')) {  ?>


 

<?php } else { ?>
<?php /*?><div class="full_row">
 <div class="Divider"></div>
 <h2>AGAR PRINCIPLES</h2>
<ul class="catListing fullWidth" >
      <?php
			query_posts('cat=19');
			while (have_posts()) : the_post();  ?>
              <li>
              <a href="<?php echo get_permalink(); ?>"> 
              <div class="catimg">  <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail("full");
                } ?>
              </div>
			<h3><?php echo the_title(); ?></h3>
            </a>
           
			<?php the_excerpt(); ?>

				  </li>
      <?php endwhile;  wp_reset_query();
?>
    </ul>
 </div><?php */?>
<?php } ?>
</div>
<?php get_footer(); ?>




<?php /*?><ul class="catListing" >
      <?php
			query_posts('cat=19');
			while (have_posts()) : the_post();  ?>
              <li>
              <a href="<?php echo get_permalink(); ?>"> 
              <div class="catimg">  <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail("full");
                } ?>
              </div>
			<h3><?php echo the_title(); ?></h3>
            </a>
           
			<?php the_excerpt(); ?>

				  </li>
      <?php endwhile;  wp_reset_query();
?>
    </ul><?php */?>
	
    
    
<?php /*?><ul class="blogPosts">    
<?php
$args = array( 'posts_per_page' => 20 );
$lastposts = get_posts( $args );
foreach ( $lastposts as $post ) :
  setup_postdata( $post ); ?>
  <li>
  <a href="<?php the_permalink(); ?>"><h2><<?php the_title(); ?></h2></a>
<div class="blogImg">	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail("full");
	} ?>
    </div>
	<?php the_content(); ?>
    </li>
<?php endforeach; 
wp_reset_postdata(); ?>
</ul><?php */?>