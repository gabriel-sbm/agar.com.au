<?php 
/**

Template Name: Product Selection Page Template

**/
?>
<?php get_header(); ?>

<div class="innerFull">
<?php if ( have_posts() ) : while( have_posts() ) : the_post();
	if ( has_post_thumbnail() ) {
		
			$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		 ?>
    
    <div class="backgroundImg" style="background-image:url(<?php echo $large_image_url[0] ?>);">
<img src="<?php echo $large_image_url[0] ?>" alt="" />
</div>
<?php } ?>
 <div class="full_row">
<?php the_content(); ?>
</div>
 <div class="full_row">
 <h1><center><?php echo the_title();?></center></h1>
 
 <div class="productSelections">
	 <?php $args1 = array(
	 'show_option_none'   => 'Select Soilage',
	'orderby'            => 'ID', 
	'order'              => 'ASC',
	'show_count'         => 0,
	'hide_empty'         => 1, 
	'child_of'           => 0,
	'echo'               => 1,
	'selected'           => $_REQUEST['cat1'],
	'hierarchical'       => 0, 
	'name'               => 'cat1',
	'class'              => 'postform',
	'depth'              => 0,
	'tab_index'          => 0,
	'taxonomy'           => 'soilage',
	'hide_if_empty'      => false,
); 
$args2 = array(
	'show_option_none'   => 'Select Surface',
	'orderby'            => 'ID', 
	'order'              => 'ASC',
	'show_count'         => 0,
	'hide_empty'         => 1, 
	'child_of'           => 0,
	'echo'               => 1,
	'selected'           => $_REQUEST['cat2'],
	'hierarchical'       => 0, 
	'name'               => 'cat2',
	'class'              => 'postform',
	'depth'              => 0,
	'tab_index'          => 0,
	'taxonomy'           => 'surface',
	'hide_if_empty'      => false,
); ?>
<form action="<?php the_permalink(); ?>" method="post">
<?php
wp_dropdown_categories($args1);
wp_dropdown_categories($args2); ?>
<input type="submit" name="submit" value="view" />
</form>
 </div>
 
<div class="ProductListing">
	<?php 
	
	if(isset($_REQUEST['submit'])){
		
		 
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'order' => 'DESC', 
			'orderby' => 'date',
			'tax_query' => array( 
				'relation' => 'OR', 
				array(
					'taxonomy' => 'soilage',
					'field' => 'id',
					'terms' => $_REQUEST['cat1']
				),
				array(
					'taxonomy' => 'surface',
					'field' => 'id',
					'terms' => $_REQUEST['cat2']
				)
			),
			'posts_per_page' => 20,
		);
		global $woocommerce;
		query_posts($args);
		if ( have_posts() ) : while( have_posts() ) : the_post(); ?>
		<div class="three_colBor hotProducts">
			<div class="span3">
				<div class="proImg"> 
					<img width="65px" height="115px" alt="My Image Placeholder" src="https://agar.com.au/agar/wp-content/plugins/woocommerce/assets/images/placeholder.png"> 
				</div>
				<h6><?php echo the_title(); ?></h6>
				<?php echo strip_tags(substr(get_the_content(),0,150)).'...'; 
				/*
				global $product;
				echo apply_filters( 'woocommerce_loop_add_to_cart_link',
					sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s">%s</a>',
						esc_url( $product->add_to_cart_url() ),
						esc_attr( $product->id ),
						esc_attr( $product->get_sku() ),
						esc_attr( isset( $quantity ) ? $quantity : 1 ),
						$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
						esc_attr( $product->product_type ),
						esc_html( $product->add_to_cart_text() )
					),
				$product );  */?>
				<a title="Tyre Shine" href="<?php echo get_permalink(); ?>" class="button" id="id-893">MORE INFO</a> 
			</div>
		</div>
		<?php 
		endwhile;  wp_reset_query();   endif;
		
	 } ?>


</div>

 
 </div>


<?php endwhile;  wp_reset_query();   endif; ?>




</div>
<?php get_footer(); ?>




<?php /*?><ul class="catListing" >
      <?php
			query_posts('cat=19');
			while (have_posts()) : the_post();  ?>
              <li>
              <a href="<?php echo get_permalink(); ?>"> 
              <div class="catimg">  <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail("full");
                } ?>
              </div>
			<h3><?php echo the_title(); ?></h3>
            </a>
           
			<?php the_excerpt(); ?>

				  </li>
      <?php endwhile;  wp_reset_query();
?>
    </ul><?php */?>
	
    
    
<?php /*?><ul class="blogPosts">    
<?php
$args = array( 'posts_per_page' => 20 );
$lastposts = get_posts( $args );
foreach ( $lastposts as $post ) :
  setup_postdata( $post ); ?>
  <li>
  <a href="<?php the_permalink(); ?>"><h2><<?php the_title(); ?></h2></a>
<div class="blogImg">	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail("full");
	} ?>
    </div>
	<?php the_content(); ?>
    </li>
<?php endforeach; 
wp_reset_postdata(); ?>
</ul><?php */?>
