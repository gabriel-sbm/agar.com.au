<?php 

/**



Template Name: Blog Page Template



**/

?>

<?php get_header(); ?>



<div class="innerFull">


<?php /*?><?php if ( have_posts() ) : while( have_posts() ) : the_post();

	if ( has_post_thumbnail() ) {

		

		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

		

		 ?>

    

    <div class="backgroundImg" style="background-image:url(<?php echo $large_image_url[0] ?>);">

<img src="<?php echo $large_image_url[0] ?>" alt="" />

</div>

<?php } ?>

 <div class="full_row">

<?php the_content(); ?>

</div>



<?php endwhile;  wp_reset_query();   endif; ?><?php */?>







<div class="full_row">  

	<h1 style="text-align: center">Our blog: tips &amp; tricks for cleaning</h1>


	<div class="blog_filter">

		<?php 

		$args1 = array(

					'type'                     => 'product',

					'child_of'                 => 0,

					'parent'                   => '',

					'orderby'                  => 'name',

					'order'                    => 'ASC',

					'hide_empty'               => 1,

					'hierarchical'             => 1,

					'exclude'                  => '',

					'include'                  => '',

					'number'                   => '',

					'taxonomy'                 => 'category',

					'pad_counts'               => false,

					'hide_if_empty'      	=> false



		);

		$args2 = array(

					'orderby'                  => 'name',

					'order'                    => 'ASC',

					'get'                 	   => 'all',

					'pad_counts'               => false,



		);

		 ?>

		<form name="blog_filter" class="blog_filter" action="" method="post">

			<span class="blog_category">

				<?php $categories = get_categories( $args1 );

				$current_category = get_query_var( 'term' );

				echo '<select id="blog_category" name="blog_category">';

				echo '<option value="0">All Categories</option>';

				foreach($categories as $category){

					if($_REQUEST['blog_category'] == $category->term_id){

						$selected = 'selected=selected';

					}else{

						$selected = '';

					}

					echo '<option value="'.$category->term_id.'" data-slug="'.$category->slug.'" '.$selected.'>'.$category->name.'</option>';

				}

				echo '</select>'; ?>

			</span>

			<span class="blog_tags">

				<?php $tags = get_tags($args2);

				$current_tag = get_query_var( 'tag' );

				echo '<select id="blog_tags" name="blog_tags">';

				echo '<option value="0">All Tags</option>';

				foreach($tags as $tag){

					if($_REQUEST['blog_tags'] == $tag->term_id){

						$selected = 'selected=selected';

					}else{

						$selected = '';

					}

					echo '<option value="'.$tag->term_id.'" data-slug="'.$tag->slug.'" '.$selected.'>'.$tag->name.'</option>';

				}

				echo '</select>'; ?>

			</span>

            

            <span class="blog_title">

            <div class="searchBar">

				<label>

					<span class="screen-reader-text">Search for:</span>

					<input type="text" title="" name="blog_title" value="<?php echo $_REQUEST['blog_title']; ?>" placeholder="Title Search . . ." class="blog_title">

				</label>

				<input type="submit" value="Search" class="search-submit">

			</div>

            </span>

			<span class="blog_order">

				<?php if($_REQUEST['blog_order'] == 'ASC'){

						$asc = 'selected=selected';

						$desc = '';

					}else if($_REQUEST['blog_order'] == 'DESC'){

						$desc = 'selected=selected';

						$asc = '';

					}else{

						$asc = '';

						$desc = '';

					} ?>

				<select id="blog_order" name="blog_order">

					<option value="none">Select Order</option>

					<option value="ASC" <?php echo $asc; ?>>Ascending</option>

					<option value="DESC" <?php echo $desc; ?>>Desending</option>

				</select>

			</span>

		</form>

	</div>

<ul class="blogPosts">  



<?php

if(isset($_REQUEST['blog_title']) && $_REQUEST['blog_title'] != ''){

	$postids = $wpdb->get_col("select ID from $wpdb->posts where post_title LIKE '".$_REQUEST['blog_title']."%' and post_type = 'post' ");

}else{

	$postids = '';

}

$args = array( 

	'post__in'			=> $postids,

	'posts_per_page' 	=> 20,

	'cat'				=> ($_REQUEST['blog_category'] != 0 ? $_REQUEST['blog_category'] : ""),

	'tag_id'			=> ($_REQUEST['blog_tags'] != 0 ? $_REQUEST['blog_tags'] : ""),

	'post_status'		=> 'publish',

	'order'				=> ($_REQUEST['blog_order'] != 'none' ? $_REQUEST['blog_order'] : "")

);



query_posts( $args );

if(have_posts()) :

	while ( have_posts() ) : the_post(); ?>

		<li>

			<?php if ( has_post_thumbnail() ) { ?>

				<div class="blogImg"><?php	the_post_thumbnail("full"); ?></div>

			<?php } ?>

			<div class="blogRtcol">

				<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

				<div class="blog_postdate"><?php echo the_date(); ?></div>

				<?php the_excerpt(); ?> 

				<?php /* <?php the_content(); ?>  */ ?>

			</div>

			<div class="PostBottom">

				<a class="blog_readmore" href="<?php the_permalink(); ?>">Read More</a>

				<div class="totalComents"> <?php echo comments_number(  ); ?>. </div>

			</div>

		</li>

	<?php endwhile; 

endif;

wp_reset_query(); ?>

</ul>

</div>







</div>

<?php get_footer(); ?>

