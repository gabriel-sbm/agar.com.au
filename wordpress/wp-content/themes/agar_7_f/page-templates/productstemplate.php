<?php 
/**

Template Name: Products Template

**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>


	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<?php if(isset($_REQUEST['cat']) && $_REQUEST['cat'] != ''){ 
				$term = get_term_by( 'id', absint( $_REQUEST['cat'] ), 'product_cat' );
				?>
				<h1 class="page-title"><?php echo $term->name; ?></h1>
			<?php }else{ ?>
				<h1 class="page-title">Cleaning Product Categories</h1>

				<?php the_content(); ?>	

			<?php } ?>

		<?php endif; 
		if(isset($_REQUEST['cat']) && $_REQUEST['cat'] != ''){
			$args = array(
				'type'                     => 'product',
				'child_of'                 => '',
				'parent'                   => $_REQUEST['cat'],
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 1,
				'hierarchical'             => 1,
				'taxonomy'                 => 'product_cat',
				'pad_counts'               => false,
				'hide_if_empty'      	=> false

			); 
		}else{
			$args = array(
				'type'                     => 'product',
				'child_of'                 => '',
				'parent'                   => 0,
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 1,
				'hierarchical'             => 1,
				'taxonomy'                 => 'product_cat',
				'pad_counts'               => false,
				'hide_if_empty'      	=> false

			); 
			
		}
			//echo '<div class="select_category">';
			$categories = get_categories( $args );
			function compareByName($a, $b) {
			  return strcmp($a->name, $b->name);
			}
			usort($categories, 'compareByName');
			/*
			$current_category = get_query_var( 'term' );
			echo '<select id="product_category">';
			echo '<option value="0">All Categories</option>';
			foreach($categories as $category){
				if($current_category == $category->slug){
					$selected = 'selected=selected';
				}else{
					$selected = '';
				}
				echo '<option value="'.$category->term_id.'" data-slug="'.$category->slug.'" '.$selected.'>'.$category->name.'</option>';
			}
			echo '</select>';
			echo '</div>';
			* */
		?>

		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>
				<?php /* ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
				<?php */ ?>
				
			<?php foreach($categories as $category){ ?>
					<li class="product_category">
						<?php $children = get_terms( 'product_cat', array(
								'parent'    => $category->term_id,
								'hide_empty' => false
							));
							/*
							if($children){
								$url = get_permalink().'?cat='.$category->term_id;
							}else{
								$url = get_term_link( $category, 'product_cat' );
							}
							* */
							$url = get_term_link( $category, 'product_cat' );
							?>
						<a href="<?php echo $url; ?>">
							<?php $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
							$image = wp_get_attachment_url( $thumbnail_id );  
							if($image){ ?>
								<img class="woocommerce-placeholder wp-post-image" width="150" height="150" alt="Placeholder" src="<?php echo $image; ?>">
							<?php }else{ ?>
								<img class="woocommerce-placeholder wp-post-image" width="150" height="150" alt="Placeholder" src="<?php bloginfo('url'); ?>/wp-content/plugins/woocommerce/assets/images/placeholder.png">
							<?php } ?> 
							<h3><?php echo $category->name; ?></h3>
						</a>
					</li>
			<?php } ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				//do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
