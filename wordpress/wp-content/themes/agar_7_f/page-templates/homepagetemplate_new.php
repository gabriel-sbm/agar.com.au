<?php
/*
*	Template Name: Home Page Template V3
*/

?>
<?php get_header(); ?>

<div id="hp_slider">
	<div id="hp_slider_large">
		<?php echo do_shortcode('[nemus_slider id="4703"]') ?>
	</div>
	<div id="hp_slider_small">
		<?php echo do_shortcode('[nemus_slider id="4704"]') ?>
	</div>

</div>

<?php if (have_posts()) : while (have_posts()) : the_post();
		if (has_post_thumbnail()) { ?>
			<?php the_post_thumbnail('full'); ?>
		<?php } ?>


		<div class="signUpSection">
			<div class="full_row">
				<div class="three_col"><a href="<?php echo site_url() ?>/contact?click_location=homepage_under_banner&tester=happypanda"> <img src="<?php echo bloginfo('template_url'); ?>/images/chat.png" alt="icon: help" />
						<div class="texts">
							<h4>Need help?</h4>
							<p>We can answer your questions!</p>
						</div>
					</a>
				</div>

				<div class="three_col"> <a href="<?php echo site_url() ?>/sds?click_location=homepage_under_banner&tester=happypanda"><img src="<?php echo bloginfo('template_url'); ?>/images/download.png" alt="icon: download" />
						<div class="texts">
							<h4>Download SDS?</h4>
							<p>Click here!</p>
						</div>
					</a>
				</div>

				<div class="three_col"> <a href="<?php echo site_url() ?>/my-account?click_location=homepage_under_banner&tester=happypanda"><img src="<?php echo bloginfo('template_url'); ?>/images/email.png" alt="icon: email" />
						<div class="texts">
							<h4>Sign Up</h4>
							<p>For How-to's, news &amp; more!</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="alldetail">
			<ul>
				<li><a href="<?php echo site_url(); ?>/cleaning-products"><span><img src="<?php echo bloginfo('template_url'); ?>/images/mobileicon1.png" alt="icon: products" /></span>
						<h4>Products</h4>
					</a></li>
				<li><a href="<?php echo site_url(); ?>/distributors"><span><img src="<?php echo bloginfo('template_url'); ?>/images/mobileicon2.png" alt="icon: Distributors" /></span>
						<h4>Find a store</h4>
					</a></li>
				<li><a href="<?php echo site_url() ?>/contact"><span><img src="<?php echo bloginfo('template_url'); ?>/images/chat.png" alt="icon: Contact" /></span>
						<h4>Contact Us</h4>
					</a></li>
			</ul>
			<ul class="online_training">
				<li><a href="http://agar.otrainu.com/login/signup.php"><span><img src="<?php echo bloginfo('template_url'); ?>/images/mobileicon4.png" alt="icon: Online Training" /></span>
						<h4>Online Training </h4>
					</a></li>
				<li><a href="<?php echo site_url() ?>/my-account"><span><img src="<?php echo bloginfo('template_url'); ?>/images/email.png" alt="icon: Sign up" /></span>
						<h4>Sign Up</h4>
					</a></li>
				<li><a href="<?php echo site_url() ?>/sds"><span><img src="<?php echo bloginfo('template_url'); ?>/images/download.png" alt="icon:  download MSDS" /></span>
						<h4>Download MSDS</h4>
					</a></li>
			</ul>
		</div>
		<div class="SearchSection">
			<div class="full_row">
				<div class="four_col">
					<div class="productsearch" id="psw">

						<?php
						global $wpdb;
						/* Ben: I believe below is useless and not being used
							$args1 = array(
								'show_option_none'   => '-Select Soilage-',
								'orderby'            => 'ID', 
								'order'              => 'ASC',
								'show_count'         => 0,
								'hide_empty'         => 1, 
								'child_of'           => 0,
								'echo'               => 1,
								'selected'           => $_REQUEST['cat2'],
								'hierarchical'       => 0, 
								'name'               => 'cat2',
								'class'              => 'postform',
								'depth'              => 0,
								'tab_index'          => 0,
								'taxonomy'           => 'soilage',
								'hide_if_empty'      => false,
							); 
							$args2 = array(
								'show_option_none'   => '-Select Surface-',
								'orderby'            => 'ID', 
								'order'              => 'ASC',
								'show_count'         => 0,
								'hide_empty'         => 1, 
								'child_of'           => 0,
								'echo'               => 1,
								'selected'           => $_REQUEST['cat1'],
								'hierarchical'       => 0, 
								'name'               => 'cat1',
								'class'              => 'postform',
								'depth'              => 0,
								'tab_index'          => 0,
								'taxonomy'           => 'surface',
								'hide_if_empty'      => false,
							); 
							*/

						?>
						<img src="<?php echo bloginfo('template_url') ?>/images/productsec.png" alt="Product Selection tool" />
						<div class="loader"></div>

						<div class="select_first">
							<p>Select an option <br>below, to help <br> you choose the <br> correct product <br> for your task.</p>
							<select class="choose_first select_Box1">
								<option value="none">Select</option>
								<option value="select_area_first">Area to clean</option>
								<option value="select_task_first">Soil to remove</option>
							</select>
							<img class="gresultBtn" alt="Get Results" src="<?php bloginfo('template_url') ?>/images/getresult.png">
						</div>

						<form class="soil_to_remove xxx1" action="<?php echo get_bloginfo('url'); ?>/product-selection-tool-results" method="post">
							<a href="javascript:void(0)" class="backBtnPro" data-hide="">&lt;&lt; Back</a>
							<div class="select_task search_1 selectDiv" data-class="search_1">
								<?php $results = $wpdb->get_results('select * from soil_search_1 ORDER BY `name` ASC'); ?>
								<p><b>Select Soil to remove</b></p>

								<select name="cat_1" class="select_Box2">
									<option value="select">Select</option>
									<?php foreach ($results as $result) {
										if ($result->name != '') { ?>
											<option value="<?php echo $result->id ?>"><?php echo $result->name; ?></option>
									<?php }
									} ?>
								</select>

							</div>
							<div class="select_area search_2 selectDiv" data-class="search_2">
								<p><b>Select surface to clean</b></p>

								<span class="selectSpan">
									<select name="cat_2" class="select_Box3">
										<option value="select">Select</option>
									</select>
								</span>
							</div>
							<div class="soilage_category selectDiv search_3" data-class="search_3">
								<p><b>Select Sub-Category</b></p>
								<span class="selectSpan">
									<select name="cat_3" class="select_Box4">
										<option value="select">Select</option>
									</select>
								</span>
							</div>
							<div class="search_return">
								<a href="javascript:void(0)" class="return_to_start">Return to Start</a>
							</div>
							<input type="image" class="product_search_btn colBtn" alt="Get Results" src="<?php echo bloginfo('template_url') ?>/images/getresult.png">

						</form>
						<form class="area_to_clean" action="<?php echo get_bloginfo('url'); ?>/product-selection-tool-results" method="post">
							<a href="javascript:void(0)" class="backBtnPro" data-hide="">&lt;&lt; Back</a>
							<div class="search_1 selectDiv" data-class="search_1">
								<?php $results = $wpdb->get_results('select * from area_search_1 ORDER BY `name` ASC'); ?>
								<select name="search_1" class="select_Box5">
									<option value="select">Select</option>
									<?php foreach ($results as $result) { ?>
										<option value="<?php echo $result->id ?>"><?php echo $result->name; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="search_2 selectDiv" data-class="search_2">
								<select name="search_2" class="select_Box6">
									<option>Select</option>
								</select>
							</div>
							<div class="search_3 selectDiv" data-class="search_3">
								<select name="search_3" class="select_Box7">
									<option value="select">Select</option>
								</select>
							</div>
							<div class="search_4 selectDiv" data-class="search_4">
								<select name="search_4" class="select_Box8">
									<option value="select">Select</option>
								</select>
							</div>
							<div class="search_5 selectDiv" data-class="search_5">
								<select name="search_5" class="select_Box9">
									<option value="select">Select</option>
								</select>
							</div>
							<div class="search_return">
								<a href="javascript:void(0)" class="return_to_start">Return to Start</a>
							</div>
							<input type="image" class="product_search_btn colBtn" alt="Get Results" src="<?php echo bloginfo('template_url') ?>/images/getresult.png">

						</form>

					</div>
				</div>
				<div class="four_col">
					<div class="productsearch ebus">
						<a href="<?php echo site_url() ?>/e-business-solutions/">
							<img src="<?php echo bloginfo('template_url') ?>/images/ebusiness.png" alt="E-Business Solutions" />
						</a>
						<p><br>Save Time<br>and Order<br>Online<br>24/7</p>
						<a href="<?php echo site_url() ?>/e-business-solutions/" class="colBtn"><img src="<?php echo bloginfo('template_url') ?>/images/signupbtn.png" alt="Sign Up!" /></a>
					</div>
				</div>
				<div class="four_col">

					<div class="productsearch comply">
						<a href="<?php echo site_url() ?>/comply-and-protect/">
							<img src="<?php echo bloginfo('template_url') ?>/images/comply.png" alt="Comply and Protect" />
						</a>
						<p>Tick all the<br>boxes with<br>Agar’s<br>compliance<br>support</p>
						<a href="<?php echo site_url() ?>/comply-and-protect/" class="colBtn"><img src="<?php echo bloginfo('template_url') ?>/images/complbtn.png" alt="Compliance" /></a>
					</div>
				</div>
				<div class="four_col">
					<div class="productsearch envir">
						<a href="<?php echo site_url() ?>/green-products/">
							<img src="<?php echo bloginfo('template_url') ?>/images/green.png" alt="Environmentally Preferred Range" />
						</a>
						<p>Reduce<br>Environmental<br>Impact with<br>GECA approved<br>products</p>
						<a href="<?php echo site_url() ?>/green-products/" class="colBtn"><img src="<?php echo bloginfo('template_url') ?>/images/greenBtn.png" alt="Green Range" /></a>
					</div>
				</div>

				<?php

				$form = '<form role="search" method="get" id="searchform" action="' . esc_url(home_url('/')) . '">
							<div>
								<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __('Quick Product Search...', 'woocommerce') . '" />
								<input type="submit" id="searchsubmit" value="' . esc_attr__('Search', 'woocommerce') . '" />
								<input type="hidden" name="post_type" value="product" />
							</div>
						</form>';

				echo $form;
				?>

			</div>
		</div>

		<div class="postproduct">
			<div class="full_row">
				<div class="three_colBor lattestPost">
					<h3>Latest Blog Posts</h3>
					<ul>
						<?php
						$args = array(
							'post_type' => 'post',
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'caller_get_posts' => 1,
							'orderby' => 'post_date',
							'order' => 'DESC',
							'posts_per_page' => 2
						);
						$my_query = null;
						$my_query = new WP_Query($args);
						if ($my_query->have_posts()) {
							while ($my_query->have_posts()) : $my_query->the_post(); ?>
								<li>
									<div class="postdate">
										<div class="Pdate"><?php the_time('d') ?> </div>
										<div class="pMonth"><?php the_time('M') ?> </div>
									</div>
									<h6><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h6>
									<div class="postcont">
										<p> <?php
											// $content = get_the_content();
											// echo substr($content, 0, 100); 
											$content = wp_trim_words(get_the_content(), 20, '');
											echo $content; ?> </p>

										<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Read more...</a>
									</div>
								</li><?php
									endwhile;
								}
								wp_reset_query();  // Restore global post data stomped by the_post().
										?>
					</ul>
				</div>

				<div id="hotProducts" class="three_colBor hotProducts">
					<h3>Hot Products</h3>

					<?php $hot_product = unserialize(get_option('agar_hot_product')); ?>
					<div class="span3">
						<div class="proImg">
							<?php if ($hot_product['hot_image']) { ?>
								<?php
								// Force URL to use proper image size (200x200), also force to https 
								$hot_product_url = (str_replace(".jpg", "-200x200.jpg", $hot_product['hot_image']));
								$hot_product_url_https = (str_replace("http://", "https://", $hot_product_url));
								?>
								<img src="<?php echo $hot_product_url_https; ?>" width="200px" height="200px" />
							<?php } else {
								echo '<img src="' . woocommerce_placeholder_img_src() . '" alt="My Image Placeholder" width="200px" height="200px" />';
							} ?>
						</div>
						<h6><?php echo $hot_product['hot_title']; ?></h6>
						<p><?php echo $hot_product['hot_content']; ?></p>
						<a id="id-<?php echo $hot_product['hot_link']; ?>" class="button" href="<?php echo get_permalink($hot_product['hot_link']); ?>" title="<?php the_title($hot_product['hot_link']); ?>">MORE INFO</a>
					</div>
				</div>
				<!-- Facebook feed widget  -->
				<div class="three_colBor facebookWidt">
					<div class="fb-page" data-href="https://www.facebook.com/agarcleaningsystems" data-tabs="timeline" data-width="" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/agarcleaningsystems" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/agarcleaningsystems">Agar Cleaning Systems</a></blockquote></div>
				</div>
				<!-- Facebook feed widget  -->
			</div>
		</div>

		<div class="saftySheet">
			<div class="full_row">
				<p>LOOKING FOR SAFETY DATA SHEETS (SDS)?</p>
				<a href="<?php echo site_url(); ?>/sds" class="button">CLICK HERE</a>
			</div>
		</div>


		<!-- Testimonial -->
		<div class="testimonial">
			<div class="full_row">
				<img class="lttestimo" src="<?php echo bloginfo('template_url') ?>/images/testimgleft.png" alt="icon: start quote" />
				<?php //echo do_shortcode('[random_testimonial show_title="1"]') 
				?>

				<?php echo do_shortcode('[testimonials_cycle random="true" count="5" show_title="1" transition="fade" prev_next="1" timer="12000" container="1"]') ?>


				<img class="rttestimg" src="<?php echo bloginfo('template_url') ?>/images/testimgright.png" alt="icon: end quote" />
			</div>
		</div>


		<!-- Homepage Content -->

		<div id="hp-content" class="full_row">
	<?php the_content();
	endwhile;
	wp_reset_query();
endif; ?>
		</div>


		<div class="linkdata">
			<ul>
				<li><a href="<?php echo site_url() ?>/e-business-solutions"><img src="<?php echo bloginfo('template_url') ?>/images/businessicon1.png" alt="E-Business Solutions" /></a></li>
				<li> <a href="<?php echo site_url() ?>/comply-and-protect"><img src="<?php echo bloginfo('template_url') ?>/images/businessicon2.png" alt="Comply and Protect" /></a></li>
				<li><a href="<?php echo site_url() ?>/green-products"><img src="<?php echo bloginfo('template_url') ?>/images/businessicon3.png" alt="Environmentally Preferred Range" /></a></li>
			</ul>
		</div>
		<?php get_footer(); ?>