<?php 
/**

Template Name: About Page Template

**/
?>
<?php get_header(); ?>

<div class="innerFull">
<?php if ( have_posts() ) : while( have_posts() ) : the_post();
	if ( has_post_thumbnail() ) {
		
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		
		 ?>
    
    <div class="backgroundImg" style="background-image:url(<?php echo $large_image_url[0] ?>);">
<img src="<?php echo $large_image_url[0] ?>" alt="" />
</div>
<?php } ?>
 <div class="full_row">
<?php the_content(); ?>
</div>

<?php endwhile;  wp_reset_query();   endif; ?>




</div>
<?php get_footer(); ?>




<?php /*?><ul class="catListing" >
      <?php
			query_posts('cat=19');
			while (have_posts()) : the_post();  ?>
              <li>
              <a href="<?php echo get_permalink(); ?>"> 
              <div class="catimg">  <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail("full");
                } ?>
              </div>
			<h3><?php echo the_title(); ?></h3>
            </a>
           
			<?php the_excerpt(); ?>

				  </li>
      <?php endwhile;  wp_reset_query();
?>
    </ul><?php */?>
	
    
    
<?php /*?><ul class="blogPosts">    
<?php
$args = array( 'posts_per_page' => 20 );
$lastposts = get_posts( $args );
foreach ( $lastposts as $post ) :
  setup_postdata( $post ); ?>
  <li>
  <a href="<?php the_permalink(); ?>"><h2><<?php the_title(); ?></h2></a>
<div class="blogImg">	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail("full");
	} ?>
    </div>
	<?php the_content(); ?>
    </li>
<?php endforeach; 
wp_reset_postdata(); ?>
</ul><?php */?>