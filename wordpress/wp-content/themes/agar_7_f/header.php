<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!--meta name="viewport" content="width=device-width" /-->

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!--[if lt IE 9]>

	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>

	<![endif]-->

<?php wp_head(); ?>
<link href='https://fonts.googleapis.com/css?family=Kalam:700' rel='stylesheet' type='text/css' />

<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/service.ddlist.jquery.css' />

<script type="text/javascript" src="<?php bloginfo('template_url');  ?>/js/service.ddlist.jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');  ?>/js/custom.js"></script>
<?php /*?> <link rel='stylesheet' media='screen and (min-width: 768px) and (max-width: 1024px)' href='<?php bloginfo('template_url'); ?>/css/tablet.css' />

<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 767px)' href='<?php bloginfo('template_url'); ?>/css/iphone.css' />

<link rel='stylesheet' media='screen and (min-width: 0px) and (max-width: 639px)' href='<?php bloginfo('template_url'); ?>/css/mobile.css' /><?php */?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NDNKNKN');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- FB SDK -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0"></script>
<!-- FB SDK -->
<div id="page" class="hfeed site">
<?php if ( get_header_image() ) : ?>
<div id="site-header"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="header image"> </a> </div>
<?php endif; ?>
<header id="masthead" class="site-header" role="banner">
<div class="header-main">
<div class="full_row">
  <div class="rightHead">
    <div class="full"> <a id="members-area" href="<?php echo site_url();?>/member-area">MEMBERS AREA</a>
      <?php 
        if ( is_user_logged_in() ) {
          echo '<p class="marrt"><a href="' . site_url() . '/wp-login.php?action=logout&redirect_to=http://agar.com.au/my-account">Logout</a></p>';
        }
        else {
          echo '<p class="marrt"><a href="' . site_url() . '/my-account">Register now!</a></p>
          <p><a href="' . site_url() . '/my-account">Login</a></p>';
        }
        
      ?>
    </div>
  </div>
  <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url') ?>/images/logo.png" alt="Agar, the Chemistry of Cleaning" /></a>

	<div id="header-phone-top"><a href="tel:1800301302">Call 1800 301 302</a></div>
	
  <div class="serchPhone">
    <?php /* <div class="socialLogo"> <a href="https://www.facebook.com/agarcleaningsystems" target="_blank" title="Facebook" class="fb"></a> <a href="https://www.linkedin.com/company/agar-cleaning-systems-pty-ltd" target="_blank" title="Linkedin" class="linkedin"></a> <a href="http://plus.google.com/+agarchemicals" target="_blank" title="Google+" class="gplus"></a> </div> */ ?>
		<div id="header-title">PERFORMANCE - HYGIENE - SAFETY</div>
    <div id="header-title-new">
      <div id="header-title-top">Commercial &amp; Industrial</div>
      <div id="header-title-middle">Cleaning Products &amp; Systems</div>
      <div id="header-title-bottom">Performance - Hygiene - Safety</div>
    </div>
    <div class="searchBar">
      <?php get_search_form(); ?>
    </div>
    <div id="header-phone">Call 1800 301 302</div>
    <div class="memberarea"> <a class="memberareadata" href="<?php echo site_url();?>/member-area">MEMBERS AREA</a> </div>
  </div>
  </div>
  <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
  <div class="full">
    <button class="menu-toggle">
    <?php _e( 'Menu', 'twentyfourteen' ); ?>
    </button>
    <a class="screen-reader-text skip-link" href="#content">
    <?php _e( 'Skip to content', 'twentyfourteen' ); ?>
    </a>
    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
    </div>
  </nav>

</header>
<!-- #masthead -->

<div id="main" class="site-main">
