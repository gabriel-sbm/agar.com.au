<?php
/**
 * Twenty Fourteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see twentyfourteen_content_width()
 *
 * @since Twenty Fourteen 1.0
 */
 include('inc/export_products.php');
 include('inc/hot_product.php');
 include('inc/psw_manage.php');
 include('inc/users_search.php');
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

/**
 * Twenty Fourteen only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfourteen_setup' ) ) :
/**
 * Twenty Fourteen setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_setup() {

	/*
	 * Make Twenty Fourteen available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Fourteen, use a find and
	 * replace to change 'twentyfourteen' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'twentyfourteen', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( array( 'css/editor-style.css', twentyfourteen_font_url(), 'genericons/genericons.css' ) );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'twentyfourteen-full-width', 1038, 576, true );
	add_image_size( 'homepage_featured_product', 200, 200, true );
	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Top primary menu', 'twentyfourteen' ),
		'secondary' => __( 'Secondary menu in left sidebar', 'twentyfourteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'twentyfourteen_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

	// Add support for featured content.
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'twentyfourteen_get_featured_posts',
		'max_posts' => 6,
	) );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // twentyfourteen_setup
add_action( 'after_setup_theme', 'twentyfourteen_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'twentyfourteen_content_width' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return array An array of WP_Post objects.
 */
function twentyfourteen_get_featured_posts() {
	/**
	 * Filter the featured posts to return in Twenty Fourteen.
	 *
	 * @since Twenty Fourteen 1.0
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'twentyfourteen_get_featured_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
function twentyfourteen_has_featured_posts() {
	return ! is_paged() && (bool) twentyfourteen_get_featured_posts();
}

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_widgets_init() {
	require get_template_directory() . '/inc/widgets.php';
	register_widget( 'Twenty_Fourteen_Ephemera_Widget' );

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'twentyfourteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that appears on the left.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Content Sidebar', 'twentyfourteen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Additional sidebar that appears on the right.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'twentyfourteen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="footer-heading widget-title">',
		'after_title'   => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Sign Up', 'twentyfourteen' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Appears in the footer section of the site.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
}
add_action( 'widgets_init', 'twentyfourteen_widgets_init' );

/**
 * Register Lato Google font for Twenty Fourteen.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return string
 */
function twentyfourteen_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'twentyfourteen' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_scripts() {
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.0.3' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfourteen-style', get_stylesheet_uri(), array( 'genericons' ) );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfourteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfourteen-style', 'genericons' ), '20131205' );
	wp_style_add_data( 'twentyfourteen-ie', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfourteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		wp_enqueue_script( 'twentyfourteen-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
		wp_localize_script( 'twentyfourteen-slider', 'featuredSliderDefaults', array(
			'prevText' => __( 'Previous', 'twentyfourteen' ),
			'nextText' => __( 'Next', 'twentyfourteen' )
		) );
	}

	wp_enqueue_script( 'twentyfourteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20140616', true );
}
add_action( 'wp_enqueue_scripts', 'twentyfourteen_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_admin_fonts() {
	wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'twentyfourteen_admin_fonts' );

if ( ! function_exists( 'twentyfourteen_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default Twenty Fourteen attachment size.
	 *
	 * @since Twenty Fourteen 1.0
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'twentyfourteen_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

if ( ! function_exists( 'twentyfourteen_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );

	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );

		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>

	<div class="contributor">
		<div class="contributor-info">
			<div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
			<div class="contributor-summary">
				<h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
				<p class="contributor-bio">
					<?php echo get_the_author_meta( 'description', $contributor_id ); ?>
				</p>
				<a class="button contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
					<?php printf( _n( '%d Article', '%d Articles', $post_count, 'twentyfourteen' ), $post_count ); ?>
				</a>
			</div><!-- .contributor-summary -->
		</div><!-- .contributor-info -->
	</div><!-- .contributor -->

	<?php
	endforeach;
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentyfourteen_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}

	return $classes;
}
add_filter( 'body_class', 'twentyfourteen_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function twentyfourteen_post_classes( $classes ) {
	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'twentyfourteen_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function twentyfourteen_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );

// Implement Custom Header features.
require get_template_directory() . '/inc/custom-header.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Add Theme Customizer functionality.
require get_template_directory() . '/inc/customizer.php';

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	require get_template_directory() . '/inc/featured-content.php';
}


//add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );
 
/**
* woo_custom_product_searchform
*
* @access public
* @since 1.0
* @return void
*/
function woo_custom_product_searchform( $form ) {
$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/' ) ) . '">
<div>
<label class="screen-reader-text" for="s">' . __( 'Search for:', 'woocommerce' ) . '</label>
<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( 'My Search form', 'woocommerce' ) . '" />
<input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search', 'woocommerce' ) .'" />
<input type="hidden" name="post_type" value="product" />
</div>
</form>';
return $form;
}



function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}


add_action( 'wp_ajax_select_area', 'select_area_callback' );
add_action( 'wp_ajax_nopriv_select_area', 'select_area_callback' );

function select_area_callback() {
	$cat_id = intval( $_POST['cat_id'] );
	$args = array(
		'posts_per_page'   => 100,
		'offset'           => 0,
		'orderby'          => 'post_date',
		'order'            => 'DESC',
		'post_type'        => 'product',
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'tax_query' => array( 
			array(
				'taxonomy' => 'surface',
				'field' => 'id',
				'terms' => $cat_id
			)
		)
	);
	$posts_array = get_posts( $args );
	$post_ids = array();
	$soilage_ids = array();
	foreach($posts_array as $array){
		$post_ids[] = $array->ID;
	}
	$soilage = wp_get_object_terms( $post_ids, 'soilage');
	foreach($soilage as $array){
		$soilage_ids[] = $array->term_id;
	}
	echo json_encode($soilage_ids);

	die(); 
}


add_action( 'wp_ajax_select_task', 'select_task_callback' );
add_action( 'wp_ajax_nopriv_select_task', 'select_task_callback' );

function select_task_callback() {
	$cat_id = intval( $_POST['cat_id'] );
	$args = array(
		'posts_per_page'   => 100,
		'offset'           => 0,
		'orderby'          => 'post_date',
		'order'            => 'DESC',
		'post_type'        => 'product',
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'tax_query' => array( 
			array(
				'taxonomy' => 'soilage',
				'field' => 'id',
				'terms' => $cat_id
			)
		)
	);
	$posts_array = get_posts( $args );
	$post_ids = array();
	$surface_ids = array();
	foreach($posts_array as $array){
		$post_ids[] = $array->ID;
	}
	$surface = wp_get_object_terms( $post_ids, 'surface');
	foreach($surface as $array){
		$surface_ids[] = $array->term_id;
	}
	echo json_encode($surface_ids);

	die(); 
}


add_action( 'wp_ajax_get_products', 'get_products_callback' );
add_action( 'wp_ajax_nopriv_get_products', 'get_products_callback' );

function get_products_callback() {
	if($_REQUEST['surface'] == -1){
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'order' => 'DESC', 
			'orderby' => 'date',
			'tax_query' => array(  
				array(
					'taxonomy' => 'soilage',
					'field' => 'id',
					'terms' => trim($_REQUEST['soilage'])
				)
			),
			'posts_per_page' => 200,
		);
	}else if($_REQUEST['soilage'] == -1){
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'order' => 'DESC', 
			'orderby' => 'date',
			'tax_query' => array(  
				array(
					'taxonomy' => 'surface',
					'field' => 'id',
					'terms' => trim($_REQUEST['surface'])
				)
			),
			'posts_per_page' => 200,
		);
	}else{
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'order' => 'DESC', 
			'orderby' => 'date',
			'tax_query' => array( 
				'relation' => 'AND', 
				array(
					'taxonomy' => 'soilage',
					'field' => 'id',
					'terms' => trim($_REQUEST['soilage'])
				),
				array(
					'taxonomy' => 'surface',
					'field' => 'id',
					'terms' => trim($_REQUEST['surface'])
				)
			),
			'posts_per_page' => 200,
		);
	}
	$products = get_posts($args);
	$data = '';
	//$data .= '<select id="prod" name="prod">';
	//$data .= '<option value="-1" >- Select Product -</option>';
	foreach($products as $product){
		//$data .= '<option value="'.$product->ID.'" data-url="'.$product->guid.'" >'.$product->post_title.'</option>';
		$data .= '<a href="'.$product->guid.'"><li id="'.$product->ID.'" >'.$product->post_title.'</li></a>';
	}
	//$data .= '</select>';
	echo $data;

	die();
}
add_shortcode( 'product_categories_dropdown', 'woo_product_categories_dropdown' );
function woo_product_categories_dropdown( $atts ) {
	extract(shortcode_atts(array(
		'count' => '0',
		'hierarchical' => '0',
		'orderby' => ''
	), $atts));
	ob_start();
	$c = $count;
	$h = $hierarchical;
	$o = ( isset( $orderby ) && $orderby != '' ) ? $orderby : 'order';
	woocommerce_product_dropdown_categories( $c, $h, 0, $o );
	?>
	<script type='text/javascript'>
		var product_cat_dropdown = document.getElementById("dropdown_product_cat");
		function onProductCatChange() {
			if ( product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value !=='' ) {
				location.href = "<?php echo home_url(); ?>/?product_cat="+product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value;
			}
		}
		product_cat_dropdown.onchange = onProductCatChange;
	</script>
	<?php
	return ob_get_clean();
} 


function woo_add_custom_admin_product_tab() {
?>
<li class="custom_tab"><a href="#custom_tab_data"><?php _e('Upload PDFs', 'woocommerce'); ?></a></li>
<?php
} 
add_action( 'woocommerce_product_write_panel_tabs', 'woo_add_custom_admin_product_tab' );

function woo_add_custom_admin_product() {
	global $post, $woocommerce;
  		$custom_tab_data = get_post_custom($post->ID);
	?>
	<div id="custom_tab_data" class="panel woocommerce_options_panel">
		<div class="options_group">
			<h3><?php _e( 'Upload Pdfs', 'woocommerce' ); ?></h3>
		</div>
		<div class="options_group custom_tab_options">   
			<p><label>SDS Pdfs:</label><input class="button tagadd" id="uplaod_pdf1" type="button" value="Browse" ></p>
		</div>	
		<div class="options_group custom_tab_options sds_pdf_links">
			<h3><?php _e( 'Available SDS Pdfs', 'woocommerce' ); ?></h3>
		<?php $sds_data = get_post_meta($post->ID, 'sds_product_pdfs', true);
			  $sds_pdfs = unserialize($sds_data);
			  if($sds_pdfs){
				  if(!is_array($sds_pdfs)){
					  $sds_pdfs = array($sds_pdfs);
				  }
				  foreach($sds_pdfs as $pdf){ 
					  $name = explode('/', $pdf);
					  $name = $name[count($name)-1];
					  ?>
						<li>
							<a href="<?php echo $pdf; ?>" target="_blank"><?php echo $name ?></a> 
							<span><a href="javascript:void(0)" data-sds-pdf="<?php echo $pdf; ?>" class="delete_sds_pdf" style="float:right;">Delete</a></span>
						</li>
				<?php } 
				} ?>
		</div>
		<div class="options_group custom_tab_options">   
			<p><label>PDS Pdfs:</label><input class="button tagadd" id="uplaod_pdf2" type="button" value="Browse" ></p>
		</div>
		<div class="options_group custom_tab_options pds_pdf_links">
			<h3><?php _e( 'Available PDS Pdfs', 'woocommerce' ); ?></h3>
		<?php $pds_data = get_post_meta($post->ID, 'pds_product_pdfs', true);
			  $pds_pdfs = unserialize($pds_data);
			  if($pds_pdfs){
			  if(!is_array($pds_pdfs)){
				  $pds_pdfs = array($pds_pdfs);
			  }
			  foreach($pds_pdfs as $pdf){ 
				  $name = explode('/', $pdf);
				  $name = $name[count($name)-1];
				  ?>
					<li>
						<a href="<?php echo $pdf; ?>" target="_blank"><?php echo $name ?></a> 
						<span><a href="javascript:void(0)" data-pds-pdf="<?php echo $pdf; ?>" class="delete_pds_pdf" style="float:right;">Delete</a></span>
					</li>
			<?php } 
			} ?>
		</div>
		<div class="upload_files_data_sds">
			<?php if($sds_pdfs){
					if(!is_array($sds_pdfs)){
						$sds_pdfs = array($sds_pdfs);
					}
					foreach($sds_pdfs as $pdf){ ?>
					<input type="hidden" name="sds_pdfs[]" value="<?php echo $pdf; ?>">
			<?php 	}
				} ?>
		</div>
		<div class="upload_files_data_pds">
			<?php if($pds_pdfs){
					if(!is_array($pds_pdfs)){
						$pds_pdfs = array($pds_pdfs);
					}
					foreach($pds_pdfs as $pdf){ ?>
					<input type="hidden" name="pds_pdfs[]" value="<?php echo $pdf; ?>">
			<?php 	}
				} ?>
		</div>
	</div>
	<?php
}
add_action('woocommerce_product_write_panels', 'woo_add_custom_admin_product');

function company_js() { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var file_frame;
			var file_frame1;
			jQuery('#uplaod_pdf1').live('click', function( event ){
				event.preventDefault();
				if ( file_frame ) {
					file_frame.open();
					return;
				}
				file_frame = wp.media.frames.file_frame = wp.media({
					title: jQuery( this ).data( 'uploader_title' ),
					button: {
					text: jQuery( this ).data( 'uploader_button_text' ),
					},
					multiple: true
				});
				file_frame.on( 'select', function() {
					attachment = file_frame.state().get('selection').toJSON();
					var data = JSON.stringify(attachment);
					var data = JSON.parse(data);
					jQuery.each(data, function(idx, obj) { 
						jQuery('.upload_files_data_sds').append('<input type="hidden" name="sds_pdfs[]" value="'+obj.url+'">');
						jQuery('.sds_pdf_links').append('<li><a href="'+obj.url+'" target="_blank">'+obj.url+'</a></li>');
					});
				});
				file_frame.open();
			});
			jQuery('#uplaod_pdf2').live('click', function( event ){
				event.preventDefault();
				if ( file_frame1 ) {
					file_frame1.open();
					return;
				}
				file_frame1 = wp.media.frames.file_frame1 = wp.media({
					title: jQuery( this ).data( 'uploader_title' ),
					button: {
					text: jQuery( this ).data( 'uploader_button_text' ),
					},
					multiple: true
				});
				file_frame1.on( 'select', function() {
					attachment = file_frame1.state().get('selection').toJSON();
					var data = JSON.stringify(attachment);
					var data = JSON.parse(data);
					jQuery.each(data, function(idx, obj) {
						jQuery('.upload_files_data_pds').append('<input type="hidden" name="pds_pdfs[]" value="'+obj.url+'">');
						jQuery('.pds_pdf_links').append('<li><a href="'+obj.url+'" target="_blank">'+obj.url+'</a></li>');
					});
				});
				file_frame1.open();
			});
			jQuery('.delete_sds_pdf').live('click', function(){
				var url = $(this).attr('data-sds-pdf');
				$(this).parents('li').css('display','none');
				jQuery('.upload_files_data_sds input').each(function() {
					if(jQuery(this).val() == url){
						jQuery(this).remove();
					}
				});
			});
			jQuery('.delete_pds_pdf').live('click', function(){
				var url = $(this).attr('data-pds-pdf');
				$(this).parents('li').css('display','none');
				jQuery('.upload_files_data_pds input').each(function() {
					if(jQuery(this).val() == url){
						jQuery(this).remove();
					}
				});
			});
		});
	</script>
<?php }
add_action('admin_footer','company_js');

function process_product_meta_custom_tab( $post_id ) {
	global $woocommerce; 
	global $post; 		
	$data1 = $_POST['sds_pdfs'];
	$data2 = $_POST['pds_pdfs'];
  		update_post_meta( $post_id, 'sds_product_pdfs', serialize($data1));
  		update_post_meta( $post_id, 'pds_product_pdfs', serialize($data2));
}
add_action('save_post', 'process_product_meta_custom_tab');

//search 2
add_action( 'wp_ajax_search_2', 'search_2_callback' );
add_action( 'wp_ajax_nopriv_search_2', 'search_2_callback' );

function search_2_callback() {
	$name = trim( $_POST['name'] );
	$id = intval( $_POST['id'] );
	global $wpdb;
	//$results = $wpdb->get_results('select * from area_search_2 where 1_id='.$id.' ORDER BY `name` ASC');
	$results = $wpdb->get_results('select * from (select as2.name as name, as2.id as id from area_search_2 as2 INNER JOIN area_search_6 as6 on (as2.id = as6.2_id) where as2.1_id='.$id.') temp group by name ORDER BY name ASC');
	$data = '<select name="search_2" class="select_Box6">
		<option value="0">Select</option>';
	foreach($results as $result){
		$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
	}
	$data .='</select>';
	echo $data;
	die();
}


//search 3
add_action( 'wp_ajax_search_3', 'search_3_callback' );
add_action( 'wp_ajax_nopriv_search_3', 'search_3_callback' );

function search_3_callback() {
	$name = trim( $_POST['name'] );
	$id2 = intval( $_POST['2_id'] );
	$id1 = intval( $_POST['1_id'] );
	global $wpdb;
	//$results = $wpdb->get_results('select * from area_search_3 where 2_id='.$id2.' and 1_id='.$id1.' ORDER BY `name` ASC');
	$results = $wpdb->get_results('select * from (select as3.name as name, as3.id as id from area_search_3 as3 INNER JOIN area_search_6 as6 on (as3.id = as6.3_id) where as3.2_id='.$id2.' and as3.1_id='.$id1.') temp group by name ORDER BY `name` ASC');
	if(count($results) > 0){
	$data = '<select name="search_3" class="select_Box7">
		<option value="0">Select</option>';
	foreach($results as $result){
		$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
	}
	$data .='</select>';
	echo $data;
	}else{
		echo 'none';
	}
	die();
}

//search 4
add_action( 'wp_ajax_search_4', 'search_4_callback' );
add_action( 'wp_ajax_nopriv_search_4', 'search_4_callback' );

function search_4_callback() {
	$name = trim( $_POST['name'] );
	$id = intval( $_POST['id'] );
	global $wpdb;
	//$results = $wpdb->get_results('select * from area_search_4 where 3_id='.$id.' ORDER BY `name` ASC');
	$results = $wpdb->get_results('select * from (select as4.name as name, as4.id as id from area_search_4 as4 INNER JOIN area_search_6 as6 on (as4.id = as6.4_id) where as4.3_id='.$id.') temp group by name ORDER BY `name` ASC');
	if(count($results) > 0){
		$data = '<select name="search_4" class="select_Box8">
			<option value="0">Select</option>';
		foreach($results as $result){
			$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
		}
		$data .='</select>';
		echo $data;
	}else{
		echo 'none';
	}
	die();
}

//search 5
add_action( 'wp_ajax_search_5', 'search_5_callback' );
add_action( 'wp_ajax_nopriv_search_5', 'search_5_callback' );

function search_5_callback() {
	global $wpdb;
	if(isset($_POST['skip']) && $_POST['skip'] != ''){
		$id_3 = $_POST['3_id'];
		$id_4 = $_POST['4_id'];
		//$results = $wpdb->get_results('select * from area_search_5 where 3_id='.$id_3.' and 4_id='.$id_4.' ORDER BY `name` ASC');
		$results = $wpdb->get_results('select * from (select as5.name as name, as5.id as id from area_search_5 as5 INNER JOIN area_search_6 as6 on (as5.id = as6.5_id) where as5.3_id='.$id_3.' and as5.4_id='.$id_4.') temp group by name ORDER BY `name` ASC');
		if(count($results)>0){
			$data = '<select name="search_5" class="select_Box9">
				<option value="0">Select</option>';
			foreach($results as $result){
				$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
			}
			$data .='</select>';
		}else{
			echo 'none';
		}
		echo $data;
		die;
		
	}else{
		$name = intval( $_POST['name'] );
		$id = intval( $_POST['id'] );
		
		//$results = $wpdb->get_results('select * from area_search_5 where 4_id='.$id.' ORDER BY `name` ASC');
		$results = $wpdb->get_results('select * from (select as5.name as name, as5.id as id from area_search_5 as5 INNER JOIN area_search_6 as6 on (as5.id = as6.5_id) where as5.4_id='.$id.') temp group by name ORDER BY `name` ASC');
		if(!empty($results)){
			$data = '<select name="search_5" class="select_Box9">
				<option value="0">Select</option>';
			foreach($results as $result){
				$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
			}
			$data .='</select>';
			echo $data;
		}else{
			echo 'none';
		}
	}
	die();
}

//search 1
add_action( 'wp_ajax_soil_search_2', 'soil_search_2_callback' );
add_action( 'wp_ajax_nopriv_soil_search_2', 'soil_search_2_callback' );

function soil_search_2_callback() {
	global $wpdb;
	$name = trim( $_POST['name'] );
	$id = intval( $_POST['id'] );
	//$results = $wpdb->get_results('select * from soil_search_2 where FIND_IN_SET("'.$id.'",1_id) ORDER BY `name` ASC');
	//$results = $wpdb->get_results('select * from ( select ss2.name as name, ss2.id as id from soil_search_2 ss2 INNER JOIN soil_search_4 ss4 on (ss2.id = ss4.2_id) where ss2.1_id ='.$id.' ) temp group by name ORDER BY `name` ASC');
	$rests = $wpdb->get_results('select id as id, name as name, 1_id as iid from soil_search_2 where FIND_IN_SET("'.$id.'",1_id) ORDER BY `name` ASC');
	$array = array();
	$i = 0;
	foreach($rests as $rest){
		$datas = explode(',',$rest->iid);
		foreach($datas as $data){
			$results = $wpdb->get_results('select * from ( select ss2.name as name, ss2.id as id from soil_search_2 ss2 INNER JOIN soil_search_4 ss4 on (ss2.id = ss4.2_id) where ss4.1_id ='.$data.' ) temp group by name ORDER BY `name` ASC');
			if($results){
				foreach($results as $result){
					$array[$i]['id'] =  $rest->id;
					$array[$i]['name'] =  trim($rest->name);
				}
				$i++;
			}
		}
	}
	//print_r($array);
	$array = array_map("unserialize", array_unique(array_map("serialize", $array)));
	/*
	function sortByOrder($a, $b) {
		return $a['name'] - $b['name'];
	}
	usort($array, 'sortByOrder');*/
	function subval_sort($a,$subkey) {
		foreach($a as $k=>$v) {
			$b[$k] = strtolower($v[$subkey]);
		}
		asort($b);
		foreach($b as $key=>$val) {
			$c[] = $a[$key];
		}
		return $c;
	}
	$array = subval_sort($array,'name');
	if(count($array)>0){
		$data = '<select name="cat_2" class="select_Box3">
			<option value="0">Select</option>';
		foreach($array as $arr){
			$data .= '<option value='.$arr['id'].'>'.$arr['name'].'</option>';
		}
		$data .='</select>';
		echo $data;
	}else{
		echo 'none';
	}
	die();
}


//search 2
add_action( 'wp_ajax_soil_search_3', 'soil_search_3_callback' );
add_action( 'wp_ajax_nopriv_soil_search_3', 'soil_search_3_callback' );

function soil_search_3_callback() {
	$name = trim( $_POST['name'] );
	$id_1 = $_POST['1_id'];
	$id_2 = $_POST['2_id'];
	global $wpdb;
	//$results = $wpdb->get_results('select * from soil_search_3 where 1_id='.$id_1.' and 2_id='.$id_2.' ORDER BY `name` ASC');
	$results = $wpdb->get_results('select * from (select ss3.name as name, ss3.id as id from soil_search_3 ss3 INNER JOIN soil_search_4 ss4 on (ss3.id = ss4.3_id) where ss3.1_id='.$id_1.' and ss3.2_id='.$id_2.') temp group by name ORDER BY `name` ASC');
	if(count($results)>0){
		$data = '<select name="cat_3" class="select_Box4">
			<option value="0">Select</option>';
		foreach($results as $result){
			$data .= '<option value='.$result->id.'>'.$result->name.'</option>';
		}
		$data .='</select>';
		echo $data;
	}else{
		echo 'none';
	}
	die();
}


  
  
  // Add to admin_init function
add_filter('manage_edit-gallery_columns', 'add_new_gallery_columns');



function add_new_gallery_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
     
    $new_columns['id'] = __('ID');
    $new_columns['title'] = _x('Gallery Name', 'column name');
    $new_columns['images'] = __('Images');
    $new_columns['author'] = __('Author');
     
    $new_columns['categories'] = __('Categories');
    $new_columns['tags'] = __('Tags');
 
    $new_columns['date'] = _x('Date', 'column name');
 
    return $new_columns;
}

  
 function modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['found_us_on'] = 'where did you find us on?';

	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');
 
 
//Subcribe to Agar while registration
add_action( 'user_register', 'registration_save', 10, 1 );

function registration_save( $user_id ) {
    if ( isset( $_POST['subscribe'] ) )
        update_user_meta($user_id, 'subscribed', $_POST['subscribe']);
}
  

function removewoo_updates_info( $value ) {
   unset( $value->response['woocommerce/woocommerce.php'] );
   unset( $value->response['easy-testimonials/easy-testimonials.php'] );
   return $value;
}
add_filter( 'site_transient_update_plugins', 'removewoo_updates_info' );



// == Amendments for the styling and extra field for store locator (wpsl) ==
// Add Range text field

add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Additional Information', 'wpsl' )] = array(
        'phone' => array(
            'label' => __( 'Tel', 'wpsl' )
        ),
        'fax' => array(
            'label' => __( 'Fax', 'wpsl' )
        ),
        'email' => array(
            'label' => __( 'Email', 'wpsl' )
        ),
        'url' => array(
            'label' => __( 'Website (e.g. agar.com.au)', 'wpsl' )
        ),
        'range' => array(
            'label' => __( 'Range Carried', 'wpsl' )
        )
    );

    return $meta_fields;
}

add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_range'] = array( 
        'name' => 'range',
        'type' => 'text'
    );

    return $store_fields;
}

//  Style store locator display

add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl_settings;

    	$listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
	    
	    // Hide thumbnail para
	    // $listing_template .= "\t\t\t" . '<p><%= thumb %></p>' . "\r\n";
	    $listing_template .= "\t\t\t" . '<h3 class="wpsl-store-name">' . wpsl_store_header_template( 'listing' ) . "</h3>\r\n";

      $listing_template .= "\t\t\t" . '<% if ( range ) { %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<div class="wpsl-range"><%= range %></div>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";

	    $listing_template .= "\t\t\t" . '<div class="wpsl-store-address">' . "\r\n";
	    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address %></span>' . "\r\n";
	    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
	    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
	    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
	    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
	    // $listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";

	    
	    // Check if the 'appointment_url' contains data before including it.
	    // $listing_template .= "\t\t\t" . '<% if ( appointment_url ) { %>' . "\r\n";
	    // $listing_template .= "\t\t\t" . '<p><a href="<%= appointment_url %>">' . __( 'Make an Appointment', 'wpsl' ) . '</a></p>' . "\r\n";
	    // $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
	    
	    $listing_template .= "\t\t" . '<div class="wpsl-distdir">' . "\r\n";

	    // Check if we need to show the distance.
	    if ( !$wpsl_settings['hide_distance'] ) {
	        $listing_template .= "\t\t" . '<%= distance %>' . esc_html( $wpsl_settings['distance_unit'] ) . ' away.' . "\r\n";
	    }
	    $listing_template .= "\t\t" . '<%= createDirectionUrl() %>' . "</div>\r\n"; 

	    $listing_template .= "\t\t" . '</div>' . "\r\n";

      $listing_template .= "\t\t\t" . '<div class="wpsl-contact-details">' . "\r\n";
      $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<span class="wpsl-phone">Ph: <a href="tel:<%= phone %>"><%= formatPhoneNumber( phone ) %></a></span>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<span class="wpsl-fax">Fax: <%= fax %></span>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<span class="wpsl-email">Email: <a href="mailto:<%= email %>"><%= email %></a></span>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% if ( url ) { %>' . "\r\n";
      $listing_template .= "\t\t\t" . '<span class="wpsl-url"><a href="<%= url %>"><%= url %></a></span>' . "\r\n";
      $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
      $listing_template .= "\t\t\t" . '</div>' . "\r\n";
	    
	    $listing_template .= "\t" . '</li>' . "\r\n"; 

	return $listing_template;

}

/* BEN - fixes broken lostpassword link! */
add_filter( 'lostpassword_url',  'wdm_lostpassword_url', 10, 0 );
function wdm_lostpassword_url() {
    return site_url('/wp-login.php?action=lostpassword');
}

/* Added to create success message for the header bar (homepage) contact form. (Using contact form 7) */

add_action( 'wp_footer', 'mycustom_wp_footer' );
 
function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    alert('Thank you, your enquiry has been sent');
}, false );
</script>
<?php
}

/* BEN 7-3-2018 - allow HTML in category descriptions */

foreach ( array( 'pre_term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}
 
foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}

/* BEN: 28-3-2018 Remove emojii crud */

function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}