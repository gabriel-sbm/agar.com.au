<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">
        <?php dynamic_sidebar('sidebar-4'); ?>
<div class="full_row">
<div class="footer_mobile">
   <ul>
   <li><a href="<?php echo site_url() ?>/contact">Contact Us</a></li>
<li><a <?php $hot_product = unserialize(get_option('agar_hot_product')); ?> href="<?php echo get_permalink($hot_product['hot_link']);?>">Hot Products</a></li>


<li><a href="<?php echo site_url() ?>/my-account">Sign Up</a></li>
<li><a href="#psw">Search Products</a></li>
<li><a href="<?php echo site_url(); ?>/blog1">Blog</a></li>
</ul>
<ul>
<li><a href="<?php echo site_url(); ?>/products">Products </a></li>
<li><a href="<?php echo site_url() ?>/sds">Download MSDS</a></li>
<li><a href="<?php echo site_url();?>/product-category/green-cleaning-products/heavy-duty-detergents">Green Cleaning </a></li>
<li><a href="<?php echo site_url();?>/member-area">Members Area</a></li>
<li><a href="http://agar.otrainu.com/login/signup.php">online Training</a></li>
</ul>
<ul class="termuse">
<li><a  href="#">Terms of Use </a></li>
</ul>
   </div>
			<?php get_sidebar( 'footer' ); ?>
            
 </div>
   
			<div class="site-info">
            <div class="full_row">
<p>Copyright-All rights reserved </p>		
</div>		
			</div><!-- .site-info -->
           
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
<!--script src="http://code.jquery.com/jquery-migrate-1.2.0.js"></script-->
<script type="text/javascript"> 
	
	jQuery(document).ready(function($){
		$('.socialLogo').parent('.execphpwidget').parents('.widget').addClass('SocialOt');
		
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
		/*
		$(".select_area ul.options li").live('click', function() {
			var cat_id = $(this).attr('data-raw-value');
			if($("#cat2 option:selected").attr('value') == -1){
				var data = {
					'action': 'select_area',
					'cat_id': cat_id
				};
				$.post(ajaxurl, data, function(response) {
					var array = JSON.stringify(response);
					$(".select_task ul.options li").each(function() {
						if(array.indexOf($(this).attr('data-raw-value'), array) > -1) {
							$(this).show();
						}else{
							$(this).hide();
						}
					});
				});
				//get_products(cat_id, $("#cat2 option:selected").attr('value'));
			}else{
				//get_products(cat_id, $("#cat2 option:selected").attr('value'));
			}
		});
		
		$(".select_task ul.options li").live('click', function() {
			var cat_id = $(this).attr('data-raw-value');
			if($("#cat1 option:selected").attr('value') == -1){
				var data = {
					'action': 'select_task',
					'cat_id': cat_id
				};
				$.post(ajaxurl, data, function(response) {
					var array = JSON.stringify(response);
					$(".select_area ul.options li").each(function() {
						if(array.indexOf($(this).attr('data-raw-value'), array) > -1) {
							$(this).show();
						}else{
							$(this).hide();
						}
					});
				});
				//get_products($("#cat1 option:selected").attr('value'), cat_id);
			}else{
				//get_products($("#cat1 option:selected").attr('value'), cat_id);
			}
		});
		*/
		$(".select_product ul.options li").live('click', function() {
			var url = $("#prod option:selected").attr('data-url');
			var id = $(this).attr('data-raw-value');
			$('a.colBtn').attr('href',url);
		});
		
		$(".select_category ul.options li").live('click', function() {
			if($(this).attr('data-raw-value') == 0){
				location.href = "<?php echo home_url(); ?>/products";
			}else{
				location.href = "<?php echo home_url(); ?>/product-category/"+$("#product_category option:selected").attr('data-slug');
			}
		});
		
		$(".blog_category ul.options li, .blog_tags ul.options li, .blog_order ul.options li").live('click', function() {
			$("form.blog_filter").submit(); 
		});
		$(".single-product .user_not_logged_dwn").live('click', function() {
			alert("Please login to download the file");
		});
		if($('body').find('.select_Box1').length>0){
			$('.select_Box1').ddlist({
				onSelected: function (index, value, text) {
					//var first = $(this).find('a label').text(); 
					if(value == 'select_task_first'){
						$('form.soil_to_remove').show();
						$('form.area_to_clean').hide();
						$('.select_task').show();
						$('.select_first').hide();
					}
					if(value == 'select_area_first'){
						$('form.soil_to_remove').hide();
						$('form.area_to_clean').show();
						//$('.select_task').show();
						//$('.select_area').show();
						//$('.select_area').insertBefore($('.select_task'));
						$('.select_first').hide();
					}
				}
			});
		}
		if($('body').find('.select_Box5').length>0){
			$('.select_Box5').ddlist({
				onSelected: function (index, value, text) {
					//var name = $(this).text();
					if(value != 'select' && value != 0){
						$('.productsearch .loader').show();
						var data = {
							'action': 'search_2',
							'name': text,
							'id': value
						};
						$.post(ajaxurl, data, function(response) {
							$('form.area_to_clean .search_2').html(response);
							if($('body').find('.select_Box6').length>0){
								$('.select_Box6').ddlist({
									onSelected: function (index, value, text) {
										if(value != 'select' && value != 0){
											$('.productsearch .loader').show();
											var data = {
												'action': 'search_3',
												'name': text,
												'2_id': value,
												'1_id': $('form.area_to_clean .search_1 select option:selected').val()
											};
											$.post(ajaxurl, data, function(response) {
												if(response == 'none'){
													$('.search_3').hide();
													$('.search_4').hide();
													$('.search_5').hide();
													$('form.area_to_clean input[type="image"]').show();
												}else{
													$('.search_3').show();
													$('.search_4').hide();
													$('.search_5').hide();
													$('form.area_to_clean .search_3').html(response);
													if($('body').find('.select_Box7').length>0){
														$('.select_Box7').ddlist({
															onSelected: function (index, value, text) {
																$('.select_Box7 option').each(function(){
																	if($(this).attr('value') == value){
																		$(this).attr('selected','selected');
																	}
																});
																if(value != 'select' && value != 0){
																	$('.productsearch .loader').show();
																	var data = {
																		'action': 'search_4',
																		'name': text,
																		'id': value
																	};
																	$.post(ajaxurl, data, function(response) {
																		if(response == 'none'){
																			$('.search_4').hide();
																			var data = {
																				'action': 'search_5',
																				'skip': 'yes',
																				'4_id': 0,
																				'3_id': $('.area_to_clean .search_3 select option:selected').val()
																			};
																			$.post(ajaxurl, data, function(response) {
																				if(response == 'none'){
																					$('.search_5').hide();
																					$('form.area_to_clean input[type="image"]').show();
																				}else{
																					$('.search_5').show();
																					$('.search_5').html(response);
																					if($('body').find('.select_Box9').length>0){
																						$('.select_Box9').ddlist({
																							onSelected: function (index, value, text) {
																								if(value != 'select' && value != 0){
																									$('form.area_to_clean input[type="image"]').show();
																								}
																							}
																						});
																					}
																				}
																				hide_first_select('.search_5');
																			});
																		}else{
																			$('.search_4').show();
																			$('.search_5').hide();
																			$('.search_4').html(response);
																			if($('body').find('.select_Box8').length>0){
																				$('.select_Box8').ddlist({
																					onSelected: function (index, value, text) {
																						if(value != 'select' && value != 0){
																							$('.productsearch .loader').show();
																							var data = {
																								'action': 'search_5',
																								'name': text,
																								'id': value
																							};
																							$.post(ajaxurl, data, function(response) { 
																								if(response == 'none'){
																									$('form.area_to_clean input[type="image"]').show();
																									$('.productsearch .loader').hide();
																								}else{
																									$('.search_5').show();
																									$('.search_5').html(response);
																									$('.select_Box9').ddlist({
																										onSelected: function (index, value, text) {
																											if(value != 'select' && value != 0){
																												$('form.area_to_clean input[type="image"]').show();
																											}
																										}
																									});
																									$('.productsearch .loader').hide();
																								}
																								hide_first_select('.search_5');
																							});
																						}
																					}
																				});
																			}
																		}
																		$('.productsearch .loader').hide();
																		hide_first_select('.search_4');
																	});
																}
															}
														});
													}
												}
												$('.productsearch .loader').hide();
												hide_first_select('.search_3');
											});
										}
									}
								});
							}
							hide_first_select('.search_2');
							$('form.area_to_clean .search_2').show();
							$('.search_3').hide();
							$('.search_4').hide();
							$('.search_5').hide();
							$('form.area_to_clean a.return_to_start').show();
							$('.productsearch .loader').hide();
						});
					}
				}
			});
		}
		$("a.backBtnPro").live('click',function(){
			//$('.productsearch .select_first').show();
			//$('.productsearch form.area_to_clean').hide();
			//$('.productsearch form.soil_to_remove').hide();
			var last_class = '';
			var class_hide = '';
			var prev_class = '';
			var formClass = $(this).parent('form').attr('class');
			$(this).parent('form').find('div.selectDiv').each(function(){
				if($(this).css('display')=='block'){
					class_hide = $(this).attr('data-class');
				}
			});
			//var classes = last_class.split(' ');
			//class_hide = classes[0];
			p_class = class_hide.split('_');
			if(p_class[1]>1){
				$(this).parent('form').find('div.'+class_hide).hide();
				//$('.'+class_hide).hide();
				var Cint = p_class[1]-1;
				prev_class = p_class[0]+'_'+Cint;
				find_prev(formClass,prev_class,p_class[0],Cint);
				//changeselected($('.'+prev_class+' select'));
			}else{
				$(this).parent('form').find('a.return_to_start').trigger('click');
			}
			$(this).parent('form').find('input[type="image"]').hide();
		});
		$("a.return_to_start").live('click',function(){
			$('.search_1').show();
			$('.search_2').hide();
			$('.search_3').hide();
			$('.search_4').hide();
			$('.search_5').hide();
			$('form.area_to_clean').hide();
			$('form.soil_to_remove').hide();
			changeselected($('.select_Box1'));
			changeselected($('.select_Box2'));
			changeselected($('.select_Box3'));
			changeselected($('.select_Box4'));
			changeselected($('.select_Box5'));
			changeselected($('.select_Box6'));
			changeselected($('.select_Box7'));
			changeselected($('.select_Box8'));
			changeselected($('.select_Box9'));
			$('form.area_to_clean input[type="image"]').hide();
			$(this).hide();
			$('.select_first').show();
		});
		if($('body').find('.select_Box2').length>0){
			$('.select_Box2').ddlist({  
				onSelected: function (index, value, text) {
					if(value != 'select' && value != 0){
						$('.productsearch .loader').show();
						var data = {
							'action': 'soil_search_2',
							'name': text,
							'id': value
						};
						$.post(ajaxurl, data, function(response) {
							if(response != 'none'){
								$('.select_area span.selectSpan').html(response); 
								$('.select_area').show();
								$('.soilage_category').hide();
								$('.productsearch .loader').hide();
								$('form.soil_to_remove a.return_to_start').show();
								if($('body').find('.select_Box3').length>0){
									$('.select_Box3').ddlist({
										onSelected: function (index, value, text) {
											if(value != 'select' && value != 0){
												$('.productsearch .loader').show();
												var data = {
													'action': 'soil_search_3',
													'1_id': $('.select_task select option:selected').val(),
													'2_id': value
												};
												$.post(ajaxurl, data, function(response) {
													if(response != 'none'){
														$('.soilage_category span.selectSpan').html(response);
														if($('body').find('.select_Box4').length>0){
															$('.select_Box4').ddlist({
																onSelected: function (index, value, text) {
																	if(value != 'select' && value != 0){
																		$('form.soil_to_remove input[type="image"]').show();
																	}
																}	
															});
														}
														$('.soilage_category').show();
														$('.productsearch .loader').hide();
														$('form.soil_to_remove a.return_to_start').show();
													}else{
														$('.soilage_category').hide();
														$('.productsearch .loader').hide();
														$('form.soil_to_remove a.return_to_start').show();
														$('form.soil_to_remove input[type="image"]').show();
													}
													hide_first_select('.soilage_category');
												});
											}
										}
									});
								}
							}else{
								$('.soilage_category').hide();
								$('.select_area').hide();
								$('.productsearch .loader').hide();
								$('form.soil_to_remove a.return_to_start').show();
								$('form.soil_to_remove input[type="image"]').show();
							}
							hide_first_select('.select_area');
						});
					}
				}
			});
		}
		
	});
	jQuery(window).load(function($){
		hide_first_select('.select_first');
		hide_first_select('.area_to_clean .search_1');
		hide_first_select('.select_task');
	});
	function hide_first_select(selector){
		jQuery(selector+' ul li').each(function(){
			if(jQuery(this).find('a label').text() == 'Select'){
				jQuery(this).hide();
			}
		});
	}
	function get_products(surface,soilage){
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
		var data = {
					'action': 'get_products',
					'soilage': soilage,
					'surface': surface
				};
				jQuery.post(ajaxurl, data, function(response) {
					jQuery('.select_product .product ul').html(response); 
					//jQuery('.select_product select').fancySelect();
				});
	}
	function blog_filter(){
		$("form.blog_filter").submit(); 
	}
	
	function changeselected(obj){
		obj.find("option").removeAttr("selected");
		obj.find("option").eq(0).attr('selected', true);
		obj.trigger("change");
		obj.next().find("a").eq(0).find("label").html(obj.find("option").eq(0).html());
		obj.next().find("ul li a").removeClass("ddListOptionIsSelected");
		obj.next().find("ul li").eq(0).find("a").addClass("ddListOptionIsSelected");
	}
	function find_prev(formClass,Class,fclass,cint){
		var CLss = fclass+'_'+cint;
		if(jQuery('form.'+formClass+' .'+CLss).css('display') == 'none' ){
			find_prev(formClass,CLss,fclass,cint-1);
		}else{
			changeselected(jQuery('form.'+formClass+' .'+CLss+' select'));
		}
	}

</script>

</body>
</html>
