<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://registration_magic.com
 * @since      1.0.0
 *
 * @package    Registraion_Magic
 * @subpackage Registraion_Magic/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Registraion_Magic
 * @subpackage Registraion_Magic/public
 * @author     CMSHelplive
 */
class RM_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $registraion_magic    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The controller of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $controller    The main controller of this plugin.
     */
    private $controller;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    private static $editor_counter = 1;
    
    // Helps to avoid success message for same form twice
    private static $success_form= false;
    
    public function __construct($plugin_name, $version, $controller) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->controller = $controller;
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_version() {
        return $this->version;
    }

    public function get_controller() {
        return $this->controller;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     * 
     */
    public function enqueue_styles() {
       
        $settings = new RM_Options;
        $theme = $settings->get_value_of('theme');
        $layout = $settings->get_value_of('form_layout');
        wp_enqueue_style('style_rm_rating', RM_BASE_URL . 'public/js/rating3/rateit.css', array(), $this->version, 'all');
               
        switch ($theme) {
            case 'classic' :
                if ($layout == 'label_top')
                    wp_enqueue_style('rm_theme_classic_label_top', plugin_dir_url(__FILE__) . 'css/theme_rm_classic_label_top.css', array(), $this->version, 'all');
                elseif ($layout == 'two_columns')
                    wp_enqueue_style('rm_theme_classic_two_columns', plugin_dir_url(__FILE__) . 'css/theme_rm_classic_two_columns.css', array(), $this->version, 'all');
                else
                    wp_enqueue_style('rm_theme_classic', plugin_dir_url(__FILE__) . 'css/theme_rm_classic.css', array(), $this->version, 'all');
                break;
                
            default :
                if ($layout == 'label_top')
                    wp_enqueue_style('rm_theme_matchmytheme_label_top', plugin_dir_url(__FILE__) . 'css/theme_rm_matchmytheme_label_top.css', array(), $this->version, 'all');
                elseif ($layout == 'two_columns')
                    wp_enqueue_style('rm_theme_matchmytheme_two_columns', plugin_dir_url(__FILE__) . 'css/theme_rm_matchmytheme_two_columns.css', array(), $this->version, 'all');
                else
                    wp_enqueue_style('rm_theme_matchmytheme', plugin_dir_url(__FILE__) . 'css/theme_rm_matchmytheme.css', array(), $this->version, 'all');
                break;
        }
        //wp_enqueue_style('rm-jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css', false, $this->version, 'all');
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/style_rm_front_end.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        $gopt= new RM_Options();
        $magic_pop= $gopt->get_value_of('display_floating_action_btn');
        wp_enqueue_script('rm_front', plugin_dir_url(__FILE__) . 'js/script_rm_front.js', array('jquery', 'jquery-ui-core', 'jquery-ui-sortable', 'jquery-ui-tabs', 'jquery-ui-datepicker','jquery-effects-core','jquery-effects-slide'), $this->version, false);
        wp_localize_script( 'rm_front', 'rm_ajax', array("url"=>admin_url('admin-ajax.php'),"gmap_api"=>$gopt->get_value_of("google_map_key")) );
        wp_register_script('rm_front_form_script', RM_BASE_URL."public/js/rm_front_form.js",array('rm_front'), $this->version, false);
        //Register jQ validate scripts but don't actually enqueue it. Enqueue it from within the shortcode callback.
        wp_register_script('rm_jquery_validate', RM_BASE_URL."public/js/jquery.validate.min.js");
        wp_register_script('rm_jquery_validate_add', RM_BASE_URL."public/js/additional-methods.min.js");
        wp_register_script('rm_jquery_conditionalize', RM_BASE_URL."public/js/conditionize.jquery.js");
        
        if(isset($_GET['action']) && $_GET['action']=='registrationmagic_embedform'){
            wp_enqueue_script('google_charts', 'https://www.gstatic.com/charts/loader.js');
            wp_enqueue_script("rm_chart_widget",RM_BASE_URL."public/js/google_chart_widget.js");
            $service= new RM_Services();
            $gmap_api_key= $service->get_setting('google_map_key');
            if(!empty($gmap_api_key)){
                wp_enqueue_script ('google_map_key', 'https://maps.googleapis.com/maps/api/js?key='.$gmap_api_key.'&libraries=places');
                wp_enqueue_script("rm_map_widget_script",RM_BASE_URL."public/js/map_widget.js");
            }
            wp_enqueue_script("rm_price_widget_script",RM_BASE_URL."public/js/price_widget.js");
            wp_enqueue_script("rm_pwd_strength",RM_BASE_URL."public/js/password_min.js");
            wp_enqueue_script("rm_mobile_data_script", RM_BASE_URL . "public/js/mobile_field/data.js");
            wp_enqueue_script("rm_mobile_script", RM_BASE_URL . "public/js/mobile_field/intlTelInput.js");
            wp_enqueue_style("rm_mobile_style", RM_BASE_URL . "public/css/mobile_field/intlTelInput.css");
            wp_localize_script('rm_mobile_script','rm_country_list', RM_Utilities::get_countries() );
            wp_enqueue_script("rm_mask_script", RM_BASE_URL . "public/js/jquery.mask.min.js");
            wp_enqueue_script('rm_jquery_conditionalize');
        }
        
        
    }

    public function run_controller($attributes = null, $content = null, $shortcode = null) {
        return $this->controller->run();
    }
    
    public function rm_front_submissions() {
        if(!empty($_GET['resend']) && !empty($_GET['rm_user'])){
            $re_verification_link= RM_Utilities::get_acc_verification_link($_GET['rm_user']);
            echo 'Click here to resend the verification link.'.$re_verification_link;
            return;
        }
        /* User Verification */
        if(!empty($_GET['rm_hash']) && !empty($_GET['rm_user'])){
            
            $user_id= absint($_GET['rm_user']);
            if(empty($user_id))
                return;
            /*
            if(is_user_logged_in())
                return;
            */
            
            $hash= sanitize_text_field($_GET['rm_hash']);
            $user_hash= get_user_meta($user_id, 'rm_activation_hash', true);
            $gopts= new RM_Options();
           
            if(get_user_meta($user_id, 'rm_user_status', true)===0){
                echo $gopts->get_value_of('acc_act_notice');
                echo do_shortcode('[RM_Login]');
                return;
            }
            
            // check for payment status
            $submission_id= get_user_meta($user_id, 'RM_UMETA_SUB_ID', true); // Get submission ID from where it is 
            if(!empty($submission_id)){
                $submission_model= new RM_Submissions;
                $submission_model->load_from_db($submission_id);
                $status= $submission_model->get_payment_status();
                if(!empty($status) && !in_array(strtolower($status),array('completed','succeeded'))){
                  // Payment not completed.
                  echo RM_UI_Strings::get('LABEL_ACC_NOT_ACTIVATED_PENDING_PAYMENT');
                  return;
                }
            }
           
            if($hash==$user_hash){
                $act_message= $gopts->get_value_of('acc_act_notice');
                $act_expiry= $gopts->get_value_of('acc_act_link_expiry');
                if($act_expiry>0){
                    $user_info = get_userdata($user_id);
                    $reg_date= get_user_meta($user_id, 'rm_activation_time', true);
                    $reg_timestamp= strtotime($reg_date);
                    $current_time= current_time('timestamp');
                    $time_diff= $current_time-$reg_timestamp;
                    $seconds_diff= $time_diff/60;
                    $hour_diff= $seconds_diff/60;
                    if($act_expiry>=$hour_diff){
                        update_user_meta( $user_id , 'rm_user_status', 0 );
                        echo $act_message;
                        echo do_shortcode('[RM_Login]');
                    } else {
                        $act_expiry_message= $gopts->get_value_of('acc_act_link_exp_notice');
                        $re_verification_link= RM_Utilities::get_acc_verification_link($user_id);
                        $act_expiry_message= str_ireplace('{{send verification email}}', $re_verification_link, $act_expiry_message);
                        echo $act_expiry_message;
                    }
                } else if($act_expiry==0) {
                    update_user_meta( $user_id , 'rm_user_status', 0 );
                    echo $act_message;
                    echo do_shortcode('[RM_Login]');
                }
                //delete_user_meta( $user_id, $meta_key, $meta_value )
            } else{
                if(isset($_GET['rm_user'])){
                    $user_id= absint($_GET['rm_user']);
                    if($user_id==0)
                        return;
                    $user= get_userdata($user_id);
                    if(empty($user))
                        return;
                     $invalid_msg= $gopts->get_value_of('acc_invalid_act_code');
                     echo $invalid_msg;
                     echo '<form method="get"><input type="hidden" name="rm_user" value="'.$user_id.'"><input type="text" name="rm_hash" placeholder="Activation Code"><br><input type="submit" value="Submit"></form>';
                }
               
            }
            return;
        }
        /* Shows form preview */
        if(!empty($_GET['form_prev']) && !empty($_GET['form_id']) && is_super_admin())
        {  
            $form_id= $_GET['form_id'];
            $form_factory= new RM_Form_Factory();
            $form= $form_factory->create_form($form_id);
            $form->set_preview(true);
            echo '<script>jQuery(document).ready(function(){jQuery(".entry-header").remove();}); </script>';
            echo '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">';
            echo '<div class="rm_embedeed_form">' . $form->render() . '</div>';
            return;
        }
        
        if (RM_Utilities::fatal_errors()) {
            ob_start();
            include_once RM_ADMIN_DIR . 'views/template_rm_cant_continue.php';
            $html = ob_get_clean();
            return $html;
        }
        
        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');

        $request = new RM_Request($xml_loader);
        if(isset($_POST['rm_slug'])){
            $request->setReqSlug($_POST['rm_slug'], true);
        }
        else{
            $request->setReqSlug('rm_front_submissions', true);
        }
        
        $params = array('request' => $request, 'xml_loader' => $xml_loader);
        $this->controller = new RM_Main_Controller($params);
        return $this->controller->run();
    }

    public function rm_login($name) {
        if (RM_Utilities::fatal_errors()) {
            ob_start();
            include_once RM_ADMIN_DIR . 'views/template_rm_cant_continue.php';
            $html = ob_get_clean();
            return $html;
        }

        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');

        $request = new RM_Request($xml_loader);
        $request->setReqSlug('rm_login_form', true);

        $params = array('request' => $request, 'xml_loader' => $xml_loader);
        $this->controller = new RM_Main_Controller($params);
        return $this->controller->run();
    }

    public function rm_user_form_render($attribute) {
        $this->disable_cache();
        if (RM_Utilities::fatal_errors()) {
            ob_start();
            include_once RM_ADMIN_DIR . 'views/template_rm_cant_continue.php';
            $html = ob_get_clean();
            return $html;
        }
        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');
        $form_id= $attribute['id'];
        $request = new RM_Request($xml_loader);
        
        if(!self::$success_form && isset($request->req['rm_success']) && $request->req['rm_success']=="1" && !empty($form_id) && isset($request->req['rm_form_id']) && $form_id==$request->req['rm_form_id']){
            self::$success_form= true;
            $form = new RM_Forms();
            $form->load_from_db($form_id);
            $form_options= $form->form_options;
            $html = "<div class='rm-post-sub-msg'>";
            $html .= $form_options->form_success_message != "" ? $form_options->form_success_message : $form->form_name . " Submitted ";
            $html .= '</div>';
            return $html;
        }
        $request->setReqSlug('rm_user_form_process', true);
        $params = array('request' => $request, 'xml_loader' => $xml_loader, 'form_id' => isset($attribute['id']) ? $attribute['id'] : null,'force_enable_multiform'=>true);
       
        if(isset($attribute['prefill_form']))
            $request->setReqSlug('rm_user_form_edit_sub', true);
        
        
        $this->controller = new RM_Main_Controller($params);
        return $this->controller->run();
    }
    
    // Set flag to notify third party caching plugins to not to cache this page.
    // Honoring this flag is up to the cache provider plugin.
    protected function disable_cache()
    {        
        if(!defined('DONOTCACHEPAGE'))
            define( 'DONOTCACHEPAGE', true );
    }

    public function register_otp_widget() {
        register_widget('RM_OTP_Widget');
    }
    
    public function register_form_widget()
    {
        register_widget('RM_Form_Widget');
    }

    /* function add_field_invites()
      {
      $screen = get_current_screen();

      if($screen->base=='registrations_page_rm_form_add')
      {   if(self::$editor_counter==3) {
      $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');

      $request = new RM_Request($xml_loader);
      $request->setReqSlug('rm_editor_actions_add_email', true);

      $params = array('request' => $request, 'xml_loader' => $xml_loader);
      $this->controller = new RM_Main_Controller($params);
      $this->controller->run();
      }
      self::$editor_counter= self::$editor_counter +1;
      }

      } */

    function execute_login() {
        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');

        $request = new RM_Request($xml_loader);
        $request->setReqSlug('rm_login_form', true);

        $params = array('request' => $request, 'xml_loader' => $xml_loader);
        $this->controller = new RM_Main_Controller($params);
        return $this->controller->run();
    }

    public function cron() {
        RM_DBManager::delete_front_user(1, 'h');
    }

    public function render_embed() {
        //Set X-Frame-Options to allow
        @header('X-Frame-Options: GOFORIT');
        $id = $_GET['form_id'];
        ?>
        <pre class="rm-pre-wrapper-for-script-tags"><script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        </script></pre>
        <?php
        do_action('wp_head');
        define('RM_AJAX_REQ', true);
        echo '<div class="rm_embedeed_form">' . $this->do_shortcode("[RM_Form id='$id']") . '</div>';
        die;
    }

    public function do_shortcode($content, $ignore_html = false) {
        if (has_shortcode($content,'RM_Form') || has_shortcode($content,'CRF_Login') || has_shortcode($content,'CRF_Form') || has_shortcode($content,'CRF_Submissions') || has_shortcode($content,'RM_Users') || has_shortcode($content,'RM_Front_Submissions')){
            return do_shortcode($content, $ignore_html);
        }
        return $content;
    }

    public function floating_action() {
        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');

        $request = new RM_Request($xml_loader);
        $request->setReqSlug('rm_front_fab', true);

        $params = array('request' => $request, 'xml_loader' => $xml_loader);
        $this->controller = new RM_Main_Controller($params);
        return $this->controller->run();
        
    }
    
     public function rm_user_list($attribute){ 
        if (RM_Utilities::fatal_errors()) {
            ob_start();
            include_once RM_ADMIN_DIR . 'views/template_rm_cant_continue.php';
            $html = ob_get_clean();
            return $html;
        }
        $xml_loader = RM_XML_Loader::getInstance(plugin_dir_path(__FILE__) . 'rm_config.xml');
 
        $request = new RM_Request($xml_loader);
        $request->setReqSlug('rm_front_user_list', true);
        
        $params = array('request' => $request, 'xml_loader' => $xml_loader,'attribute'=>$attribute);
        
        $this->controller = new RM_Main_Controller($params);
        $this->controller->run();
    }
    
    public function rm_mark_email_read() { 
        //Safety check that it is indeed invoked by WP ajax call
        if (defined('DOING_AJAX') && DOING_AJAX) {
            if(isset($_POST['action'], $_POST['rm_email_id']) && $_POST['action'] == 'rm_mark_email_read') {
                $email_id = $_POST['rm_email_id'];
                $front_service = new RM_Front_Service;        
                $front_service->mark_email_read($email_id);
            }
            wp_die();
        }
    }
    
    public function register_stat_ids() { 
        $result = array();
        if(isset($_POST['form_ids'])) {            
            
            $form_ids = $_POST['form_ids'];
            
            if(is_array($form_ids) && count($form_ids) > 0) {
                $front_form_service = new RM_Front_Form_Service;            
                foreach($form_ids as $form_uid) {
                    $form_id = explode("_", $form_uid);
                    if(count($form_id) == 3) {
                        $form_id = intval($form_id[1]);                                                
                        $result[$form_uid] = $front_form_service->create_stat_entry(array('form_id' => $form_id));
                    }                
                }
            }
        }
        echo json_encode($result);
        wp_die();
    }
    
    public function unique_field_value_check()
    { 
       if(empty($_POST['value']) || empty($_POST['field_name']))
       {
            echo json_encode(array('status'=> 'valid'));
       }
       
       $service= new RM_Front_Form_Service();  
       $field= explode('_', $_POST['field_name']);
       
       
       if($service->is_unique_field_value($field[1], $_POST['value']))
       {
            echo json_encode(array('status'=> 'valid')); 
            wp_die();
       }
       $field_model= new RM_Fields();
       $field_model->load_from_db($field[1]); 
       
       $msg= ucwords($field_model->field_label).' '.RM_UI_Strings::get("ERROR_UNIQUE");
       if($field_model->field_options->field_is_unique==1)
           $msg= $field_model->field_options->un_err_msg;
       echo json_encode(array('status'=> 'invalid','msg'=> $msg)); 
       wp_die();
    }

    public function request_non_cached_copy() {
        global $post;
        
        if( isset($_GET['rmcb']) || isset($request->req['rm_pproc']))
            return;
        
        if($post instanceof WP_Post && has_shortcode($post->post_content, 'RM_Form')) {
            $red_url = add_query_arg('rmcb', time());
            wp_redirect($red_url);
            exit();
        }
    }
    
    public function load_states(){
        if(empty($_POST['country']))
            die('Unknown country');
            
        $country= strtolower($_POST['country']);
       
        $states= array();
        if($country=="us"){
            $states= RM_Utilities::get_usa_states();
        } else if($country=="canada"){
             $states= RM_Utilities::get_canadian_provinces();
        }
        echo json_encode($states);
        
        die;
    }
    
    public function send_activation_link(){
        $user_id= absint($_POST['user_id']);
        $response= array('success'=>true);
        
        if(empty($user_id)){
            $response['success']= false;
            $response['msg']= 'No such user exists';
            echo json_encode($response);
            exit;
        }
        $user_info = get_userdata($user_id); 
        if(empty($user_info)){
            $response['success']= false;
            $response['msg']= 'No such user exists';
            echo json_encode($response);
            exit;
        }
        
        $activation_nonce= sanitize_text_field($_POST['activation_nonce']);
        if(wp_verify_nonce( $activation_nonce, 'rm_send_verification_nonce' )){
            RM_Email_Service::send_activation_link($user_id);
            $response['msg']= 'Verification link has been sent on your registered email account. Please check.';
        }
        else{
             $response['msg']= 'Incorrect security token. Please try after some time.';
        }
        echo json_encode($response);
        exit;
    }

}
